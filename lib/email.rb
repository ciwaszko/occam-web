class Occam
  class Email
    def self.send_message(account, message)
      require 'mail'

      mail = Mail.new do
        from 'me@deadreckoning.no-ip.org'
        to   'wilkie05@gmail.com'
        subject 'Blah'
        body 'ok test test'
      end

      mail.delivery_method :sendmail

      mail.deliver
    end

    def self.send_authorization(account)
    end

    def self.send_password_change_token(account)
    end

    def self.send_workset_done(account, workset)
    end
  end
end
