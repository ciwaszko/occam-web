require 'bundler'
Bundler::require

require_relative '../config/environment'

# Application root
class Occam < Sinatra::Base
  # Use root directory as root
  set :app_file => '.'

  # Use HTML5
  set :haml, :format => :html5
  set :markdown, :layout_engine => :haml,
                 :fenced_code_blocks => true,
                 :smartypants => true

  # Use sessions
  # TODO: Secret
  use Rack::Session::Cookie, :key => 'rack.session',
                             :path => '/',
                             :secret => 'foobar'

  use Rack::MethodOverride

  if ENV["RACK_ENV"] != "production"
    class PrettyJsonResponse
      def initialize(app)
        @app = app
      end

      def call(env)
        status, headers, response = @app.call(env)
        if headers["Content-Type"] =~ /^application\/json/
          obj = JSON.parse(response.first)
          pretty_str = JSON.pretty_unparse(obj)
          response = [pretty_str]
          headers["Content-Length"] = Rack::Utils.bytesize(pretty_str).to_s
        end
        [status, headers, response]
      end
    end
    use PrettyJsonResponse
  end

  # Helpers
  helpers Sinatra::ContentFor

  def self.load_tasks
    Dir[File.join(File.dirname(__FILE__), "tasks", '*.rb')].each do |file|
      require file
    end
  end

  helpers do
    def partial(page, options={})
      if page.to_s.include? "/"
        page = page.to_s.sub /[\/]([^\/]+)$/, "/_\\1"
      else
        page = "_#{page}"
      end
      haml page.to_sym, options.merge!(:layout => false)
    end
  end
end

# Load application source
%w(config helpers controllers models).each do |dir|
  Dir[File.join(File.dirname(__FILE__), "..", dir, '*.rb')].each {|file| require_relative file}
end

require_relative '../lib/redcarpet'
require_relative '../lib/memcache'
require_relative '../lib/email'

# Create default paths
require 'fileutils'

occam_path = File.join(File.dirname(__FILE__), "..", 'occam')

unless File.exists?(occam_path)
  FileUtils.mkdir_p occam_path
end

["objects", "jobs"].each do |dir|
  path = File.realdirpath(File.join(occam_path, dir))
  unless File.exists?(path)
    FileUtils.mkdir_p path
  end
end

# Create and Use Default OCCAM install path
unless Occam::System.first
  system_paths = {}
  ["objects", "jobs"].each do |dir|
    path = File.realdirpath(File.join(occam_path, dir))
    system_paths["#{dir}_path"] = path
  end

  Occam::System.create system_paths
end

#ActiveRecord::Base.logger = nil
