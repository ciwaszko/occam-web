class Occam
  require 'grit'
  require 'json'
  require 'tmpdir'

  module Git
    # Clones the given git repository and executes the given
    # block. The block is passed the name of the temporary
    # path of the repo contents. This directory is destroyed
    # after the call.
    def self.import_scripts(path, &blk)
      # Create a temporary directory.
      # Clone the repo given into that directory.
      # This directory will automatically be destroyed.
      Dir.mktmpdir do |dir|
        repo = Grit::Git.new(dir)
        repo.clone({}, path, dir)

        yield(dir)
      end
    end

    # Read the OCCAM Object description JSON from the given
    # git repository. Returns a Hash of the description data
    # found there.
    def self.description(path)
      Occam::Git.import_scripts(path) do |dir|
        begin
          description = JSON.parse(IO.read("#{dir}/object.json"))
        rescue
          description = {}
        end

        description
      end
    end
  end
end
