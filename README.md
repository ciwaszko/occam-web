# OCCAM Web Interface

This project serves as the front-end website for the OCCAM system.

## Project Layout

```
:::text

lib               - application code
\- application.rb - main app and helpers

helpers           - assorted functions to aid views

models            - data models and representation
\- account.rb     - user accounts and authorization
\- experiment.rb  - experiment information
\- simulator.rb   - simulator information
\- job.rb         - contains the jobs for the workers

controllers       - url routing
\- accounts.rb    - routes for accounts
\- sessions.rb    - logging on/off routes
\- static.rb      - contains routes for simple text content
\- simulators.rb  - routes for simulators
\- experiments.rb - routes for experiments
\- search.rb      - relates to searching functionality

views             - html markup
\- accounts       - pages for accounts
\- sessions       - pages for logging on

public            - external assets
|- css            - stylesheets
\- js             - javascript files

spec              - tests
\- controllers    - tests for routes and renders
```

## Usage

Install ruby, git, and build tools (gcc 4.8+) separately using your system
package manager. Then install ruby's library manager:

```
:::text
gem install bundler
```

Clone the occam repo:

```
:::text
git clone https://bitbucket.org/occam/occam-web
```

Install postgres and have it running. Build the database:

```
:::text
RACK_ENV=production rake db:schema:load
```

To run a production server (on port 3000):

```
:::text
thin start -e production -R config.ru
```

You can specify a port (such as port 80) using `-p` but it is recommended
to use a more powerful web server and multiple instances of our application
layer.

To start multiple servers, in this case 2, add `-s`:

```
:::text
thin start -e production -R config.ru -s 2
```

This will run them in the background on ports 3000 and 3001. Set up your
web server to route to those ports. We recommend lighttpd.

## Development

Install git and ruby separately.
With ruby installed, install bundler if you haven't already:

```
:::text
gem install bundler
```

Clone this repo:

```
:::text
git clone https://bitbucket.org/occam/occam-web
```

Inside the project directory, install dependencies:

```
:::text
bundle install
```

Now, create the database (if necessary)

```
:::text
rake db:schema:load
```

Fake some data, if you would like:

```
:::text
ruby fake_data.rb
```

Run a development server (by default, this project uses 'thin'):

```
:::text
rackup
```

This will spawn a local webserver listening on port 9292 at localhost:

```
:::text
http://localhost:9292/
```

## Testing

To run the back-end tests, inside the project directory, invoke:

```
:::text
rake test
```

For convenience, several shortcuts are available to speed up testing turn around. To run a particular model/controller test, or even a particular file (in this case, model/controller Foo):

```
:::text
rake test:model[foo]
rake test:controller[foo]
rake test:file[foo.rb]
```

## Open Source

This project makes use of several open source technologies:

* [haml](http://haml.info/) - markup preprocessor for html.
* [minitest](https://github.com/seattlerb/minitest) - minimal testing framework.
* [mocha](http://gofreerange.com/mocha/docs/) - mocking/stubbing framework for testing.
* [postgres](http://www.postgresql.org/) - robust SQL database implementation for production.
* [Ruby](https://www.ruby-lang.org/en/) - an expressive scripting language.
* [Sinatra](http://www.sinatrarb.com/) - a minimalistic web framework for Ruby.
* [sqlite](http://www.sqlite.org/) - public domain SQL implementation for development.
* [thin](http://code.macournoyer.com/thin/) - a minimal webserver we use for development.
* [chronic_duration](https://github.com/hpoydar/chronic_duration) - a library to pretty print time durations.
