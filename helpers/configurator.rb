class Occam
  module ConfiguratorHelpers
    # This method produces html that lists all input options for a given schema.
    # When values is given, they are used to provide the value of each option.
    # Otherwise, the default values given in the schema are used.
    # When dropdowns is true, an enumerated type will render a dropdown to show
    # all possible options. When false, just the current value or default will
    # be shown.
    def render_config(schema, values=nil, dropdowns=true, key=nil)
      if schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if values && values.has_key?(key)
            value = values[key]
          elsif schema.has_key?("default")
            value = schema["default"]
          end

          type = schema["type"]
          if type.is_a?(Array)
            type = "array"
          end

          if schema["type"].is_a?(Array) && (dropdowns || values.nil?)
            "<p class='#{type}'>#{schema["label"]}</p>" +
            "<div class='select'><select>" + schema["type"].map { |sub_type|
              selected = (value == sub_type)
              "<option#{selected ? " selected='selected'" : ""}>#{sub_type}</option>"
            }.join("") + "</select></div>" +
            "<div class='description'>#{schema["description"]}</div>"
          else
            "<p class='#{type}'>#{schema["label"]}</p><p>#{value}</p>" +
            "<div class='description'>#{schema["description"]}</div>"
          end
        else
          # Output group
          sub_values = values
          if values && values.has_key?(key)
            sub_values = values[key]
          end
          "<ul class='configuration-group'>" + schema["__ordering"].map { |k|
            v = schema[k]
            if v.is_a? Hash
              if v.has_key?("type") && !(v["type"].is_a?(Hash))
                # An input value
                "<li><div class='dots'></div>#{render_config(v, sub_values, dropdowns, k)}</li>"
              else
                # A group
                "<li><h2><span class='expand shown'>&#9662;</span>#{k}</h2>#{render_config(v, sub_values, dropdowns, k)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end

    def render_binary_form(object, id)
      return "" if object.nil? or object.binaries.empty?

      base64_key = Base64.urlsafe_encode64("__binaries")
      base64_key.chomp!('=')
      base64_key.chomp!('=')
      base64_key.chomp!('=')

      base64_id = Base64.urlsafe_encode64(id.to_s)
      base64_id.chomp!('=')
      base64_id.chomp!('=')
      base64_id.chomp!('=')

      hash = "data[#{base64_id}][#{base64_key}]"

      "<ul class='configuration-group' data-nesting='binary'>" +
        "<li><div class='dots'></div><label>Binary</label><span class='expand'>[+]</span>" +
          "<div class='select'><select name='#{hash}'>" + object.binaries.map{|binary| "<option>#{binary.name}</option>"}.join("") + "</select></div>" +
          "<div class='description'><p>The binary to execute.</p></div>" +
        "</li>" +
      "</ul>"
    end

    def render_ranged_form(hash, schema, recipes={}, object, id)
      render_form(hash, schema, "data", id.to_s, false, true, recipes, '', object)
    end

    def render_form(hash, schema, nesting="data", key=nil, blank=false, range=false, recipes={}, header='', object=nil)
      require 'base64'

      if key
        base64_key = Base64.urlsafe_encode64(key)
        base64_key.chomp!('=')
        base64_key.chomp!('=')
        base64_key.chomp!('=')
      end

      if schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if schema.has_key?("type") && (schema["type"] == "long" || schema["type"] == "port")
            schema["type"] = "int"
          end

          if schema.has_key?("type") && (schema["type"] == "array" || schema["type"] == "hex")
            schema["type"] = "string"
          end

          if schema.has_key?("type") && (schema["type"] == "file")
            schema["type"] = "string"
          end

          if blank
            value = ""
          elsif schema.has_key?("default")
            value = schema["default"]
          end

          if hash && hash.has_key?(key)
            value = hash[key]
          end

          description_div = ""
          if schema.has_key? "description"
            description_div = "<div class='description'>#{render(:markdown, schema["description"])}</div>"
          end

          validations = ""

          if schema["type"] == "int"
            if range
              validations += "data-parsley-type_range='integer' "
            else
              validations += "type='number' "
            end
          elsif schema["type"] == "float"
            if range
              validations += "data-parsley-type_range='number' "
            else
              validations += "data-parsley-type='number' "
            end
          end

          enables = ""
          if schema.has_key? "enables"
            if schema["enables"].has_key? "key"
              enables_key = schema["enables"]["key"]
              base64_enables_key = Base64.urlsafe_encode64(enables_key)
              base64_enables_key.chomp!('=')
              base64_enables_key.chomp!('=')
              base64_enables_key.chomp!('=')

              enables += "data-enables-key='#{base64_enables_key}' "
            end
            if schema["enables"].has_key? "is"
              enables += "data-enables-is='#{schema["enables"]["is"]}' "
            end
          end

          if schema.has_key? "validations"
            schema["validations"].each do |validation|
              if validation.has_key? "test"
                if range
                  validations += "data-parsley-test_range='#{validation["test"]}' "
                else
                  validations += "data-parsley-test='#{validation["test"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-test_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-test-message='#{validation["message"]}' "
                  end
                end
              end
              if validation.has_key? "min"
                if range
                  validations += "data-parsley-min_range='#{validation["min"]}' "
                else
                  validations += "min='#{validation["min"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-min_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-min-message='#{validation["message"]}' "
                  end
                end
              end
              if validation.has_key? "max"
                if range
                  validations += "data-parsley-max_range='#{validation["max"]}' "
                else
                  validations += "max='#{validation["max"]}' "
                end

                if validation.has_key? "message"
                  if range
                    validations += "data-parsley-max_range-message='#{validation["message"]}' "
                  else
                    validations += "data-parsley-max-message='#{validation["message"]}' "
                  end
                end
              end
            end
            validations += "data-parsley-trigger='focusout' "
          end

          case schema["type"]
          when "int", "float", "string"
            "<label class='#{schema["type"]}'>#{schema["label"]}</label><span class='expand'>[+]</span><input #{validations}#{enables}name='#{nesting}[#{base64_key}]' value='#{value}'>" +
            description_div
          when "boolean"
            if blank
              "<label>#{schema["label"]}</label><span class='expand'>[+]</span>" +
              "<div class='select'><select #{validations}#{enables}name='#{nesting}[#{base64_key}]'>" + ["true", "false", "___any___"].map { |type|
                if blank
                  selected = ("___any___" == type)
                else
                  selected = (value == type)
                end
                if type == "___any___"
                  "<option#{selected ? " selected='selected'" : ""} value='___any___'>Any</option>"
                else
                  "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
                end
              }.join("") + "</select></div>" +
              description_div
            else
              "<label class='boolean'>#{schema["label"]}</label><span class='expand'>[+]</span><input #{validations}#{enables}type='checkbox' name='#{nesting}[#{base64_key}]'#{value ? " checked" : ""}>" +
              description_div
            end
          when Array
            if blank
              schema["type"].push("___any___")
            end
            "<label>#{schema["label"]}</label><span class='expand'>[+]</span>" +
            "<div class='select'><select #{validations}#{enables}name='#{nesting}[#{base64_key}]'>" + schema["type"].map { |type|
              if blank
                selected = ("___any___" == type)
              else
                selected = (value == type)
              end
              if type == "___any___"
                "<option#{selected ? " selected='selected'" : ""} value='___any___'>Any</option>"
              else
                "<option#{selected ? " selected='selected'" : ""}>#{type}</option>"
              end
            }.join("") + "</select></div>" +
            description_div
          else
          end
        else
          # Output group
          "<ul class='configuration-group' data-nesting='#{nesting}[#{base64_key}]' data-key='#{base64_key}'>" + schema["__ordering"].map { |k|
            v = schema[k]
            if v.is_a? Hash
              new_hash = hash
              if hash && hash.has_key?(key)
                new_hash = hash[key]
              end

              if header.length > 0
                new_header = "#{header}/#{k}"
              else
                new_header = k || ""
              end

              if v.has_key?("type") && !(v["type"].is_a?(Hash))
                # An input value
                "<li><div class='dots'></div>#{render_form(new_hash, v, "#{nesting}[#{base64_key}]", k, blank, range, recipes, new_header)}</li>"
              else
                # A group
                # Rake any recipes for this group
                dropdown = ""
                if recipes.has_key? new_header
                  options = recipes[new_header].map do |recipe|
                    "<option data-template='/objects/#{object.id}/recipes/#{recipe.id}'>#{recipe.name}</option>"
                  end.join('')
                  dropdown = "<div class='recipe'><select>#{options}</select></div>"
                end
                "<li>#{dropdown}<h2><span class='expand shown'>&#9662;</span>#{k}</h2>#{render_form(new_hash, v, "#{nesting}[#{base64_key}]", k, blank, range, recipes, new_header)}</li>"
              end
            end
          }.join("") + "</ul>"
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
        return schema.to_s
      else
        # Shouldn't happen
        return schema.to_s
      end
    end
  end

  helpers ConfiguratorHelpers
end
