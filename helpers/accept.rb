class Occam
  set(:accept) do |*types|
    condition do
      all_types = ['text/html'].concat(types)
      types.include? request.preferred_type(all_types)
    end
  end
end
