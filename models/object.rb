class Occam
  class Object < ActiveRecord::Base
    require 'json'
    require_relative '../lib/git'

    # Fields

    # links     - List of all other object data
    serialize :links, Array

    # published - DateTime when object was originally published
    # updated   - DateTime when object was updated
    def update_timestamps
      now = Time.now.utc
      self[:published] ||= now if !persisted?
      self[:updated]     = now
    end
    before_save :update_timestamps

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true
    #validates :name, :uniqueness => {
    #  :case_sensitive => false
    #}

    # object_type - Object's type (simulator, benchmark, etc)
    def type
      self.object_type
    end

    def type=(value)
      self.object_type = value
    end

    # self_metadata - serialization of 'self' section
    def self_metadata=(value)
      if value.is_a? Array
        value = value.to_json
      end

      self[:self_metadata] = value
    end

    def self_metadata
      JSON.parse(self[:self_metadata])
    end

    # install_metadata - serialization of 'install' section
    def install_metadata=(value)
      if value.is_a? Hash
        value = value.to_json
      end

      self[:install_metadata] = value
    end

    def install_metadata
      JSON.parse(self[:install_metadata])
    end

    # build_metadata   - serialization of 'build' section
    def build_metadata=(value)
      if value.is_a? Hash
        value = value.to_json
      end

      self[:build_metadata] = value
    end

    def build_metadata
      JSON.parse(self[:build_metadata])
    end

    # init_metadata     - serialization of 'init' section
    def init_metadata=(value)
      if value.is_a? Hash
        value = value.to_json
      end

      self[:init_metadata] = value
    end

    def init_metadata
      JSON.parse(self[:init_metadata])
    end

    # run_metadata     - serialization of 'run' section
    def run_metadata=(value)
      if value.is_a? Hash
        value = value.to_json
      end

      self[:run_metadata] = value
    end

    def run_metadata
      JSON.parse(self[:run_metadata])
    end

    # Return a list of all known object types
    def self.types
      Occam::Object.select(:object_type).distinct.map(&:type)
    end

    # binaries
    has_many :binaries, :foreign_key => :occam_object_id

    # dependencies (through Dependency)
    has_many :dependencies, :foreign_key => :dependant_id

    # inputs
    has_many :inputs,
             :class_name  => "Occam::ObjectInput",
             :foreign_key => :occam_object_id

    # Yield all unique input types
    def input_types
      self.inputs.select(:object_type).distinct
    end

    def indirect_input_types
      types = []
      self.inputs.each do |input|
        types.concat(input.indirect_objects.select(:object_type).distinct)
      end
      types
    end

    # Yield all inputs, optionally all with the given type.
    def inputs(options={})
      ret = super
      if options["by_type"]
        ret = ret.where(:object_type => options["by_type"])
      end
      if options["by_indirect_type"]
      end
      ret
    end

    # outputs
    has_many :outputs,
             :class_name  => "Occam::ObjectOutput",
             :foreign_key => :occam_object_id

    # Yield all unique output types
    def output_types
      self.outputs.select(:object_type).distinct.map(&:object_type)
    end

    # Returns the first inactive dependency (or nil if none)
    def first_inactive_dependency
      self.dependencies.each do |d|
        if !d.depends_on.active
          return d.depends_on
        end
      end

      nil
    end

    # Returns true if all dependencies are active
    def dependencies_active?
      self.first_inactive_dependency.nil?
    end

    # tags      - The semicolon separated list of tags.

    # active    - Whether or not this object has been moderated
    def active=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:active] = value
    end

    def active
      self[:active] == 1
    end

    # built     - Whether or not the object has been built
    def built=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:built] = value
    end

    def built
      self[:built] == 1
    end

    # Returns a query of all inactive Objects. These are Objects that
    # await administrator approval.
    def self.all_inactive(type = nil)
      ret = Object.where(:active => 0)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    # Returns a query of all active and available Objects. This means
    # they have been moderated (if necessary) by an administrator and they
    # have been successfully built.
    def self.all_active
      # There is no point in not having 'built' specified or placed in a
      # second function. You cannot have active: false and built: true
      # for instance.
      Object.where(:active => 1, :built => 1)
    end

    def self.names(type = nil)
      ret = self.all_active.select(:name, :id)
      if not type.nil?
        ret = ret.where(:object_type => type)
      end
      ret
    end

    def self.all_tags(type, with = "")
      # TODO: obviously, normalize tags
      self.all_active.where(:object_type => type).to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(type, tag)
      Object.all_active.where(:object_type => type).where('tags LIKE ?', "%;#{tag};%")
    end

    def self.search(term)
      Object.where('name LIKE ?', "%#{term}%").select([:name, :description, :id])
    end

    def input_schema(format = :hash)
      if self.input_schema_document_id
        document_id = BSON::ObjectId.from_string(self.input_schema_document_id)
        document = InputSchema.first(:id => document_id)
      end

      if document
        case format
        when :hash
          document.serializable_hash
        when :json
          document.to_json
        end
      end
    end

    def output_schema(format = :hash)
      if self.output_schema_document_id
        document_id = BSON::ObjectId.from_string(self.output_schema_document_id)
        document = OutputSchema.first(:id => document_id)
      end

      if document
        case format
        when :hash
          document.serializable_hash
        when :json
          document.to_json
        end
      end
    end

    def self.search_adv(name, tags, description, website, organization, license, authors)
      query = Object.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      query = query.where('description LIKE ?', "%#{description}")
      query = query.where('website LIKE ?', "%#{website}")
      query = query.where('organization LIKE ?', "%#{organization}")
      query = query.where('license LIKE ?', "%#{license}")
      query = query.where('authors LIKE ?', "%#{authors}")
      query = query.select([:name, :description, :id])
    end

    # Install the object to the system (delayed via worker)
    def install
      # Cannot install if not active!
      return nil if !self.active

      job = self.install_job

      if job.nil?
        # Install task depends on dependencies being available on the system
        deps_jobs = self.dependencies.map do |dep|
          if !dep.depends_on.active
            # Cannot install if dependencies aren't active
            return nil
          end

          dep_job = dep.depends_on.build_job
          if dep_job.nil?
            # We need to build this dependency, so build it
            dep.depends_on.install
            dep_job = dep.depends_on.build

            # If we cannot queue a build of this dependency, abort
            return nil if dep_job.nil?
          end

          dep_job
        end.select do |build_job|
          build_job.status != :finished
        end

        job = Job.create(:object           => self,
                         :kind             => "install",
                         :codependant      => false,
                         :has_dependencies => deps_jobs.any?,
                         :dependencies     => deps_jobs,
                         :status           => "queued")
      end

      job
    end

    # Returns the Job object that is responsible for installing this object
    def install_job
      Job.where(:occam_object_id => self.id, :kind => "install").first
    end

    # Build the object (delayed via worker)
    def build
      # Cannot build if not active!
      return nil if !self.active

      install_job = self.install_job
      job         = self.build_job

      if install_job.nil?
        # Cannot build unless we have a task in the system to install
        return nil
      end

      # TODO: Race condition on depends_on??
      if job.nil?
        job = Job.create(:object           => self,
                         :kind             => "build",
                         :status           => "queued",
                         :codependant      => false,
                         :has_dependencies => !install_job.nil?,
                         :dependencies     => install_job && [install_job])
      end

      job
    end

    # Returns the Job object that is responsible for building this object
    def build_job
      Job.where(:occam_object_id => self.id, :kind => "build").first
    end

    def recipes
      Recipe.where(:occam_object_id => self.id)
    end

    # Updates the object and creates a new version of the built object
    def update
      # TODO: Do not overwrite everything
      `rm -rf #{self.local_path}`

      self.recipes.map(&:destroy)

      if self.install_job
        self.install_job.destroy
      end

      if self.build_job
        self.build_job.destroy
      end

      ActiveRecord::Base.transaction do
        self.install
        self.build
      end
    end

    # Pull object record information from the given url in script_path
    def import(url=nil, json=nil)
      if json.nil?
        url ||= self.script_path
        json = Occam::Git.description(url)
      end

      if url
        self.script_path = url
      else
        self.script_path = json["script_path"]             if json.has_key? "script_path"
      end

      self.name         = json["name"]                     if json.has_key? "name"

      if json.has_key? "install"
        self.binary_path  = json["install"]["tar"] if json["install"].has_key? "tar"
        self.binary_path  = json["install"]["svn"] if json["install"].has_key? "svn"
        self.binary_path  = json["install"]["git"] if json["install"].has_key? "git"

        self.package_type = "tar" if json["install"].has_key? "tar"
        self.package_type = "svn" if json["install"].has_key? "svn"
        self.package_type = "git" if json["install"].has_key? "git"
      end

      self.license      = json["license"]                  if json.has_key? "license"
      self.group        = json["group"]                    if json.has_key? "group"
      self.organization = json["organization"]             if json.has_key? "organization"
      self.description  = json["description"]              if json.has_key? "description"
      self.website      = json["website"]                  if json.has_key? "website"
      self.authors      = ";#{json["authors"].join(';')};" if json.has_key? "authors"
      self.tags         = ";#{json["tags"].join(';')};"    if json.has_key? "tags"

      self.object_type  = json["type"]                     if json.has_key? "type"

      # Figure out the slug path to install the object to
      if self.name
        system = System.retrieve

        root_path = system.objects_path
        self.local_path ||= "#{root_path}/#{self.id}"
      end

      # Add mirrors section for install
      json["install"]       ||= {}
      json["install"]["to"] ||= "package"

      to = json["install"]["to"]

      json["install"]["mirrors"] ||= []
      json["install"]["mirrors"] << {
        "#{self.package_type}" => "/#{self.package_type}/#{self.id}"
      }

      json["self"] ||= []

      if json["self"].empty?
        json["self"] << {
          "git" => self.script_path
        }
      end

      json["run"] ||= {}

      if json["run"].has_key? "runs-binaries"
        self.runs_binaries = json["run"]["runs-binaries"] ? 1 : 0
      else
        self.runs_binaries = 0
      end

      self.run_metadata     = json["run"]     || {}
      self.init_metadata    = json["init"]    || {}
      self.self_metadata    = json["self"]    || []
      self.build_metadata   = json["build"]   || {}
      self.install_metadata = json["install"] || {}

      # Rake inputs
      if json.has_key? "inputs"
        json["inputs"].each do |input|
          input["group"] ||= ""
          if !input.has_key? "fifo"
            input["fifo"] = false
          end
          self.inputs << Occam::ObjectInput.new(:object_type  => input["type"],
                                                :object_group => input["group"],
                                                :fifo         => input["fifo"] ? 1 : 0)
        end
      end

      # Rake outputs
      if json.has_key? "outputs"
        json["outputs"].each do |output|
          output["group"] ||= ""
          if !output.has_key? "fifo"
            output["fifo"] = false
          end
          self.outputs << Occam::ObjectOutput.new(:object_type  => output["type"],
                                                  :object_group => output["group"],
                                                  :fifo         => output["fifo"] ? 1 : 0)
        end
      end

      # Rake dependencies
      Occam::Object.dependencies(json).each do |dependency|
        self.dependencies << dependency
      end

      if json.has_key? "binaries"
        self.binaries = json["binaries"].map do |name|
          Occam::Binary.new :name => name
        end
      end

      self
    end

    def initialize(options = {}, *args)
      system = System.retrieve

      if system.moderate_objects
        options[:active] = false
      else
        options[:active] = true
      end

      # Coerse to integer
      if options[:active].is_a? TrueClass
        options[:active] = 1
      elsif options[:active].is_a? FalseClass
        options[:active] = 0
      end

      if options[:built].is_a? TrueClass
        options[:built] = 1
      elsif options[:built].is_a? FalseClass
        options[:built] = 0
      end

      # Create object binary records
      options[:binaries] ||= []
      if options[:binaries].is_a? String
        options[:binaries] = options[:binaries].split(';')
      end

      options[:binaries].map! do |name|
        Binary.new :name => name
      end

      # Ensure a leading and ending ';' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      # Ensure a leading and ending ';' in the author CSV
      options[:authors] ||= ""
      if !options[:authors].start_with?(";")
        options[:authors] = ";#{options[:authors]}"
      end
      if !options[:authors].end_with?(";")
        options[:authors] = "#{options[:authors]};"
      end

      # Figure out the slug path to install the object to
      if options[:name]
        slug = options[:name].to_url
        root_path  = system.objects_path
        options[:local_path] ||= "#{root_path}/#{slug}"
      end

      super options, *args
    end

    # Override to_a to split tag strings to arrays
    def to_hash
      hash = self.serializable_hash

      hash["tags"] = hash["tags"].split(';').drop(1)
      hash["authors"] = hash["authors"].split(';').drop(1)

      hash["type"] = hash["object_type"]
      hash.delete "object_type"

      # Provide urls for schemata
      hash.delete "input_schema_document_id"
      hash.delete "output_schema_document_id"

      hash["input_schema"]  = "/objects/#{self.id}/input_schema.json"
      hash["output_schema"] = "/objects/#{self.id}/output_schema.json"

      # Delete normalized fields (their info is within 'install')
      hash.delete "binary_path"
      hash.delete "package_type"
      hash.delete "script_path"

      # Set up 'self', 'install', 'build', and 'run' blobs
      hash["install"] = hash["install_metadata"]
      hash["build"]   = hash["build_metadata"]
      hash["self"]    = hash["self_metadata"]
      hash["run"]     = hash["run_metadata"]
      hash["init"]    = hash["init_metadata"]
      hash.delete "install_metadata"
      hash.delete "build_metadata"
      hash.delete "self_metadata"
      hash.delete "run_metadata"
      hash.delete "init_metadata"

      # Add ourselves as a mirror
      hash["self"] << {
        "git"  => "/objects/#{self.id}",
        "html" => "/objects/#{self.id}"
      }

      # Remove nil fields
      hash.delete_if {|k, v| v.nil?}

      # Delete local info
      hash.delete "local_path"

      hash.delete "id"

      hash
    end

    def to_json(*args)
      self.to_hash.to_json
    end

    def self.download_json(url, content_type = 'application/json', limit = 10)
      # Just try and pull json

      uri = URI(url)
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Accept'] = content_type
      request.content_type = content_type

      http = Net::HTTP.new(uri.hostname, uri.port)
      if uri.scheme == 'https'
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER
      end

      response = http.request(request)

      # Did we get a response? (allow redirects)
      if response.is_a?(Net::HTTPRedirection) && limit > 0
        location = response['location']
        Occam::Object.download_json(location, content_type, limit - 1)
      elsif response.is_a?(Net::HTTPSuccess) && response.content_type == "application/json"
        JSON.parse(response.body)
      else # Otherwise, assume git:
        Occam::Git.description(url)
      end
    end

    def self.all_unknown_dependencies(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"

      unknowns = Occam::Object.unknown_dependencies(json)

      deps = []

      unknowns.each do |unknown|
        url = unknown["git"]
        json = Occam::Object.download_json(url)
        obj  = Occam::Object.import(url, json)
        data = Occam::Object.all_unknown_dependencies(url, json)
        deps.concat data
        deps.append obj
      end

      deps
    end

    def self.import_all(url, json=nil)
      json ||= Occam::Object.download_json(url)
      unknowns = Occam::Object.unknown_dependencies(json)

      unknowns.each do |unknown|
        Occam::Object.import_all(unknown["git"])
      end

      obj = Occam::Object.import(url, json)
      obj.save

      # TODO: only for installable/buildable objects
      obj.install
      obj.build

      obj
    end

    # Pulls an object from a OCCAM json description file.
    def self.import(url, json=nil)
      json ||= Occam::Object.download_json(url)
      json["type"] ||= "simulator"

      # Pull out existing object or create a new one
      object = Object.where(:object_type => json["type"]).where(:script_path => url).first || Object.new
      object.import(url, json)
    end

    def self.unknown_dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          object_type = dependency["type"] || "simulator"

          if dependency.has_key? "git"
            object = Object.where(:object_type => object_type).where(:script_path => dependency["git"]).first
          end

          if object.nil? && dependency.has_key?("name")
            object = Object.where(:object_type => object_type).where(:name => dependency["name"]).first
          end

          if object.nil?
            ret << dependency
          end
        end
      end

      ret
    end

    # Given a json object description, return all realized dependencies.
    def self.dependencies(json)
      ret = []

      if json.has_key? "dependencies"
        json["dependencies"].each do |dependency|
          # TODO: Other code syndication options
          object_type = dependency["type"] || "simulator"
          if dependency.has_key? "git"
            object = Object.where(:object_type => object_type).where(:script_path => dependency["git"]).first
          end

          if object.nil? && dependency.has_key?("name")
            object = Object.where(:object_type => object_type).where(:name => dependency["name"]).first
          end

          unless object.nil?
            ret << Dependency.new(:depends_on => object)
          end
        end
      end

      ret
    end
  end
end
