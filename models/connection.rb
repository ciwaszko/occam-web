class Occam
  # A Connection is an entity that describes how two nodes are connected
  # in a Workflow.
  class Connection < ActiveRecord::Base
    # Fields

    # run_order

    # creates_object_of_type

    # co_runnable
    def co_runnable
      self[:co_runnable] == 1
    end

    def co_runnable=(value)
      # Coerse to integer
      if value.is_a? TrueClass
        value = 1
      elsif value.is_a? FalseClass
        value = 0
      end

      self[:co_runnable] = value
    end

    # Associations

    # workflow_id
    belongs_to :workflow

    # connection_id
    belongs_to :connection

    # connection_id (foreign)
    has_many :connections

    # occam_object_id
    belongs_to :object,
               :foreign_key => :occam_object_id
    validates  :object, :presence => true

    # object_output_id
    belongs_to :object_output

    # object_input_id
    belongs_to :object_input
  end
end
