class Occam
  class ObjectOutput < ActiveRecord::Base
    # Fields

    # object_group
    # object_type
    # fifo

    # Associations

    belongs_to :object,
               :foreign_key => :occam_object_id

    def objects
      q = Occam::Object.where(:object_type => self.object_type)
      if not self.object_group.blank?
        q = q.where(:group => self.object_group)
      end
      q
    end
  end
end
