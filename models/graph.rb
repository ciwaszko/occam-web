class Occam
  class Graph
    include MongoMapper::Document
    set_collection_name "graphs"

    # Integer
    key :workset_id

    # String
    key :type

    # Object
    key :data
  end
end
