class Occam
  class ObjectInput < ActiveRecord::Base
    # Fields

    # object_group
    # object_type

    # Associations

    belongs_to :object,
               :foreign_key => :occam_object_id

    def objects
      q = Occam::Object.where(:object_type => self.object_type)
      if not self.object_group.blank?
        q = q.where(:group => self.object_group)
      end
      q
    end

    def indirect_objects
      sub_query = Occam::ObjectOutput.where(:object_type => self.object_type)
      if not self.object_group.blank?
        sub_query = sub_query.where(:object_group => self.object_group)
      end
      Occam::Object.where(:id => sub_query.select(:occam_object_id))
    end
  end
end
