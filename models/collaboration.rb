class Occam
  # The Collaboration table is a join table from worksets and accounts
  class Collaboration < ActiveRecord::Base
    # Fields
    belongs_to :workset
    belongs_to :account
  end
end
