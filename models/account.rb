class Occam
  class Account < ActiveRecord::Base
    require 'bcrypt'
    require 'digest'
    require 'open-uri'

    DEFAULT_GRAVATAR = "wilkie+occam@xomb.org"

    # Fields

    # id              - Unique identifier.

    # active          - Whether or not this account has been moderated

    def self.all_inactive
      Account.where(:active => 0)
    end

    def self.all_active
      Account.where(:active => 1)
    end

    # username        - The name used to log in.
    validates :username, :presence => true
    validates :username, :length => {
      :in => 4..30
    }
    validates :username, :uniqueness => {
      :case_sensitive => false
    }

    # email           - The email for this account.

    # hashed_password - The hash of the password.
    validates :hashed_password, :presence => {
      :message => "Password cannot be blank"
    }
    validates :hashed_password, :length => {
      :in => 60..60
    }

    # roles

    # Returns true if the user has the particular role
    def has_role?(role)
      self.roles.to_s.include? ";#{role};"
    end

    # Adds the given role to this user
    def add_role(role)
      roles = self.get_roles
      roles.append(role).uniq!
      self.roles = ";#{roles.join(';')};"
    end

    # Removes the given role if it exists
    def delete_role(role)
      roles = self.get_roles
      roles.delete(role)
      self.roles = ";#{roles.join(';')};"
    end

    def get_roles
      self.roles.split(';')[1..-1]
    end

    # Accounts can collaborate on worksets
    has_many :collaborations
    has_many :worksets, :through => :collaborations

    # Create a hash of the password.
    def self.hash_password(password)
      BCrypt::Password.create(password, :cost => Occam::BCRYPT_ROUNDS)
    end

    # Determine if the given password matches the account.
    def authenticated?(password)
      (self.active == 1) && BCrypt::Password.new(hashed_password) == password
    end

    # Determine the canonical display name
    def name
      if !self.display_name.blank?
        self.display_name
      else
        self.username
      end
    end

    # Determine the avatar image url
    def avatar_url(size)
      if !self.email.blank?
        "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(self.email.strip.downcase)}?size=#{size}&d=#{URI::encode("https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(DEFAULT_GRAVATAR)}?size=#{size}")}"
      else
        if size >= 96
          "/images/person_large.png"
        else
          "/images/person.png"
        end
      end
    end

    def initialize(options = {}, *args)
      if System.first.moderate_accounts
        options[:active] = 0
      else
        options[:active] = 1
      end

      options.delete :hashed_password

      # Hash the password before entering it in our database
      if !(options[:password].nil? || options[:password] == "")
        options[:hashed_password] = Account.hash_password(options[:password])
      end

      options.delete :password

      options[:roles] = [options[:roles]] unless options[:roles].is_a? Array

      # First account is always an administrator
      if Account.count == 0
        options[:roles].append(:administrator)
      end

      options[:roles] = ";#{options[:roles].join(';')};"

      super options, *args
    end
  end
end
