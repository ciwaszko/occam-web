class Occam
  class InputSchema
    include MongoMapper::Document
    set_collection_name "input_schemas"

    def to_json(*args)
      hash = self.serializable_hash

      # Consolidate __ordering keys
      def consolidate(data)
        new_hash = {}
        data["__ordering"].each do |k|
          if data[k].is_a? Hash
            new_hash[k] = consolidate(data[k])
          else
            new_hash[k] = data[k]
          end
        end
        new_hash
      end

      consolidate(hash).to_json(*args)
    end
  end
end
