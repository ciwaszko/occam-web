class Occam
  class System < ActiveRecord::Base
    # Fields

    # id                - Unique identifier.

    # jobs_path         - System path to job output
    # objects_path      - System path to object repos
    # moderate_objects  - Whether or not objects must be authorized by
    #                     an admin before they are installed/built.
    # moderate_accounts - Whether or not accounts must be activated by
    #                     an admin before they can log in.
    # curate_git        - Whether or not git read access is given and
    #                     git repos used by objects are mirrored.
    # curate_hg         - Whether or not mercurial read access is given and
    #                     mercurial repos used by objects are mirrored.
    # curate_svn        - Whether or not svn read access is given and
    #                     svn repos used by objects are mirrored.
    # curate_files      - Whether or not file object read access is given and
    #                     any external files used by objects are mirrored.

    # Retrieves the canonical system configuration
    def self.retrieve
      System.first
    end
  end
end
