class Occam
  # Records the dependency between one Object and another
  class Dependency < ActiveRecord::Base
    # Fields
    #
    # id
    #
    # depends_on
    # dependant

    belongs_to :depends_on, :class_name => "Occam::Object"
    belongs_to :dependant,  :class_name => "Occam::Object"
  end
end
