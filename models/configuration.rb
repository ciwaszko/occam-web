class Occam
  module ConfigurationEval
    module Math
      def self.floor(x)
        x.floor
      end

      def self.ceiling(x)
        x.ceil
      end

      def self.log(x)
        ::Math.log(x)
      end

      def self.log2(x)
        ::Math.log2(x)
      end
    end
  end

  class Configuration
    require 'base64'

    include MongoMapper::Document
    set_collection_name "configurations"

    class Tokens
      attr_reader :tokens

      def initialize
        @tokens = []
      end

      def push(token, data=nil)
        @tokens.push(token)
        if token == "identifier" || token == "number"
          @tokens.push(data)
        end
      end

      def tokenize(str)
        last_character = '';
        state = 0
        token = ""

        str.each_char do |c|
          if state == 0
            # Reading identifier
            if c.match(/[*+-\/()%=<>!]/)
              # Switch states
              state = 1;

              # Push token
              if token.match(/^\d+$/)
                self.push("number", token)
              elsif (token == "x" || token == "log2" || token == "floor")
                self.push("identifier", token)
              elsif token.length > 0
                return false
              end

              token = ""
              last_character = ""
            end
          elsif state == 1
            # Reading symbol
            if c == '*' && last_character == '*' && token.length == 1
              # **
            elsif (c == '=' && (last_character == '<' ||
                                last_character == '>' ||
                                last_character == '=' ||
                                last_character == '!') && token.length == 1)
              # ==, <=, >=, !=
            elsif c.match(/\d|\w/)
              # Switch states
              state = 0

              # Push token
              self.push(token)
              token = ""
              last_character = ""
            elsif (c.match(/[*+-\/()%=<>!]/))
              # Push token
              self.push(token)
              token = ""
              last_character = ""
            end
          end

          if (c == ' ')
            self.push(token)
            token = ""
            last_character = ""
          else
            token = token + c
            last_character = c
          end
        end

        self.push(token);

        self.tokens;
      end
    end

    def self.search(data)
      self.where(data).fields(:id)
    end

    def self.validate_test(value, requirement)
      tokens = Tokens.new.tokenize(requirement);

      output = "class Occam; module ConfigurationEval; def self.func(x); "
      lastToken = ""
      leftParens = 0

      tokens.each do |token|
        if lastToken == "identifier"
          if token == "log2"
            output += "::Occam::ConfigurationEval::Math.log2"
          elsif token == "floor"
            output += "::Occam::ConfigurationEval::Math.floor"
          elsif token == "ceiling"
            output += "::Occam::ConfigurationEval::Math.ceil"
          elsif token == "log"
            output += "::Occam::ConfigurationEval::Math.log"
          elsif token == "x"
            output += "x"
          end
        elsif lastToken == "number"
          output += "(" + token + ")"
        elsif (token == "identifier" || token == "number")
        elsif token == "=="
          output += "==="
        else
          if token == "("
            leftParens += 1
          elsif token == ")"
            if leftParens < 1
              return false
            end
            leftParens -= 1
          end
          output = output + token
        end

        lastToken = token
      end

      eval(output + "; end; end; end");
      Occam::ConfigurationEval.func(value)
    end

    # Returns a set of hashes for each permutation of the given ranges.
    def self.form_range(values, schema=nil)
      set = []
      values.each do |range|
        keys   = range[0].split('.')
        values = range[1]

        object_id = keys.first.to_i
        object = ::Occam::Object.find_by(:id => object_id)
        keys = keys[1..-1]

        key_set = []
        schema = object.input_schema

        # Find definition in schema
        definition = schema
        hash = Hash.new
        current = hash
        last = nil
        last_key = nil
        p keys
        # Go down the configuration tree and find the key
        keys.each do |k|
          current[k] = Hash.new
          last = current
          current = current[k]

          definition = definition[k]
          p definition
          last_key = k
        end

        start = values[0].to_i
        final = values[1].to_i

        # Find first possible value
        until start > final
          passed = true
          if definition.has_key? "validations"
            definition["validations"].each do |validation|
              if validation.has_key? "test"
                if !self.validate_test(start, validation["test"])
                  passed = false
                  break
                end
              end
            end
          end

          if passed
            break
          else
            start += 1
          end
        end

        start.upto(final) do |i|
          passed = true

          if i != start
            if definition.has_key? "validations"
              definition["validations"].each do |validation|
                if validation.has_key? "test"
                  if !self.validate_test(i, validation["test"])
                    passed = false
                    break
                  end
                end
              end
            end
          end

          if passed
            last[last_key] = i
            key_set << {
              "hash" => {object.id.to_s => hash.dup},
              "value" => i,
              "name" => range[0]
            }
          end
        end

        set << key_set
      end

      set
    end

    # Reports all of the ranged values which we will replace when we generate
    # new configurations.
    #
    # Returns an array of tuples of the form [full_key, range]
    #   ie ['1.foo.bar', [2,3]]
    #   which means range from 2 to 3 in key foo.bar in object id 1
    def self.rake_ranges(hash, schema=nil, nesting='', key=nil, blank=false, values=[])
      hash ||= {}

      new_key = nesting
      if key && nesting
        new_key = "#{(nesting.length > 0) ? "#{nesting}." : ''}#{key}"
      end

      if schema.nil? && key.nil?
        # We are looking at a blank object config... go through every object
        hash.each do |k, v|
          object_id = k.to_i
          object = ::Occam::Object.find_by(:id => object_id)
          if object.nil?
            hash.delete k
          else
            Configuration.rake_ranges(hash[k], object.input_schema, "#{object_id}", nil, blank, values)
          end
        end
      elsif schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if schema.has_key?("type") && (schema["type"] == "long" || schema["type"] == "port")
            schema["type"] = "int"
          end

          if schema.has_key?("type") && (schema["type"] == "array" || schema["type"] == "hex")
            schema["type"] = "string"
          end

          if schema.has_key?("type") && (schema["type"] == "file")
            schema["type"] = "string"
          end

          if schema.has_key?("default")
            value = schema["default"]
          end

          if hash && hash.has_key?(key)
            value = hash[key]

            if value.index(' to ')
              values << [new_key, value.split(' to ')]
            end
          end
        else
          # Go through group
          schema["__ordering"].each do |k|
            v = schema[k]
            if v.is_a? Hash
              if hash && !key.nil? && (!hash.has_key?(key) || hash[key] == "") && blank
                next
              end

              if hash && !key.nil?
                if hash.has_key?(key)
                  Configuration.rake_ranges(hash[key], v, new_key, k, blank, values)
                end
              elsif key.nil?
                Configuration.rake_ranges(hash, v, new_key, k, blank, values)
              end
            end
          end
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
      else
        # Shouldn't happen
      end

      values
    end

    # For forms, we encode the keys as urlsafe_base64 so that they can serve
    # as ids. Our validation library optimistically requires them. We will need
    # to decode them before we can store them.
    def self.decode_base64(hash)
      hash ||= {}

      if hash.is_a? Hash
        hash.keys.each do |k|
          original_key = k
          v = hash[k]

          # Add padding back
          if (k.length % 4) > 0
            k = k + ("=" * (4 - (k.length % 4)))
          end

          k = Base64.urlsafe_decode64(k)
          hash[k] = v
          hash.delete original_key

          if v.is_a? Hash
            self.decode_base64(v)
          end
        end
      end

      hash
    end

    # This will do server side validations and add default parameters and delete
    # unknown parameters for the configuration and coerce types.
    def self.type_check(hash, schema=nil, key=nil, blank=false)
      hash ||= {}

      if schema.nil? && key.nil?
        # We are looking at a blank object config... go through every object
        hash.each do |k, v|
          connection_id = k.to_i
          connection = ::Occam::Connection.find_by(:id => connection_id)
          if connection.nil?
            hash.delete k
          else
            object = connection.object
            Configuration.type_check(hash[k], object.input_schema, nil, blank)
          end
        end
      elsif schema.is_a? Hash
        if schema.has_key?("type") && !(schema["type"].is_a?(Hash))
          # Output form input
          value = ""
          if schema.has_key?("type") && (schema["type"] == "long" || schema["type"] == "port")
            schema["type"] = "int"
          end

          if schema.has_key?("type") && (schema["type"] == "array" || schema["type"] == "hex")
            schema["type"] = "string"
          end

          if schema.has_key?("type") && (schema["type"] == "file")
            schema["type"] = "string"
          end

          if schema.has_key?("default")
            value = schema["default"]
          end

          if hash && hash.has_key?(key)
            value = hash[key]
          end

          case schema["type"]
          when "int"
            value = value.to_i
          when "float"
            value = value.to_f
          when "boolean"
            value = (value == "on" ? true : false)
          else
            value = value.to_s
          end

          if blank && (!hash.has_key?(key) || hash[key] == "" || (schema["type"].is_a?(Array) && hash[key] == "___any___"))
            hash.delete key
          else
            hash[key] = value
          end
        else
          # Go through group
          schema["__ordering"].each do |k|
            v = schema[k]
            if v.is_a? Hash
              if hash && !key.nil? && (!hash.has_key?(key) || hash[key] == "") && blank
                next
              end

              if hash && !key.nil?
                if !hash.has_key?(key)
                  hash[key] = {}
                end
                new_hash = hash[key]
              elsif key.nil?
                new_hash = hash
              end

              ret = Configuration.type_check(new_hash, v, k, blank)

              if key.nil?
                hash = new_hash
              else
                if blank && ret.empty?
                  hash.delete key
                else
                  hash[key] = ret
                end
              end
            end
          end
        end
      elsif hash.is_a? Array
        # TODO: add a "+" button to allow values of this type to be
        # appended. This is a 'more than 1' type of configuration
      else
        # Shouldn't happen
      end

      hash
    end
  end
end
