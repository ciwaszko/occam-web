class Occam
  class Experiment < ActiveRecord::Base
    require 'json'
    require 'pp'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true

    # tags      - The semicolon separated list of tags.

    # workflow  - The workflow to use to generate results.
    belongs_to :workflow

    # configuration - The configuration JSON.
    def configuration
      document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      document = Occam::Configuration.first(:id => document_id)

      if document
        document.serializable_hash
      end
    end

    def configuration=(value)
      if self.configuration_document_id
        document_id = BSON::ObjectId.from_string(self.configuration_document_id)
        document = Occam::Configuration.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = Occam::Configuration.create(value)
        self.configuration_document_id = document.id.to_s
        self.save
      end
      document
    end

    # account   - The account that created this experiment.
    belongs_to :account

    # recipe - The Recipe this experiment was spawned from (if any).
    belongs_to :recipe

    # workset - The Workset this experiment belongs to.
    belongs_to :workset

    # group - The Group this experiment belongs to.
    belongs_to :group

    # forked_from - The experiment this one was based off of (if any).
    belongs_to :forked_from, :class_name => "Experiment"

    # private - Boolean: whether or not the experiment is locked to creators/collaborators

    def can_view?(account)
      if self.private
      else
        true
      end
    end

    def results?
      !!self.results_document_id
    end

    def results
      if self.results_document_id
        document_id = BSON::ObjectId.from_string(self.results_document_id)
        document = Result.first(:id => document_id)
      end

      if document
        document.serializable_hash
      end
    end

    def results=
    end

    # Cancel the experiment by unqueuing all attached jobs
    def cancel
      job = self.job

      # Kill the process and remove from our worker collection
      job.destroy
    end

    # Run the experiment by spawning run jobs
    def run
      job = self.job
      if job.nil?
        # Add something to our worker collection

        # Build Input

        # For each object, if it has a 'run' option, create a job with any input leading to it


        # Crawl the workflow from the end result (the data) to any tasks without
        # dependencies. When we reach a node with no dependencies, we spawn the job.
        # As we crawl back down the tree, we spawn nodes and mark their dependencies
        # as the children.

        def spawn_jobs(connection=nil)
          inputs = []
          runnable = []
          corunnable = []
          ret_runnable = []

          if connection.nil?
            connections = self.workset.workflow.tail_connections
            object = nil
          else
            connections = connection.connections
            object = connection.object
          end

          hash = {
            "object" => object,
            "connection" => connection
          }

          connections.each do |sub_connection|
            sub_inputs, sub_runnable, sub_corunnable = spawn_jobs(sub_connection)
            inputs.concat(sub_inputs)
            runnable.concat(sub_runnable)
            corunnable.concat(sub_corunnable)
          end

          # Decide what to run
          #
          # Right now, we have an "object" attached to our "connection"
          #
          # We already went through and gathered the inputs for this object
          #
          # We have "inputs", which is an array of all configurations for
          #   connected objects which don't run by themselves.
          #
          # We have "runnable", which is an array of all objects which are
          #   marked to be run. We can override them running with the
          #   following rules:
          #     If we run binaries, we can spawn a job to run this object with
          #       each binary connection.
          #     If we don't, we should spawn jobs for each runnable connection
          #       and depend on those.
          #
          # Return the runnable objects and inputs.

          running = false
          if object and object.run_metadata != {}
            running = true
            if object.runs_binaries == 1
              # We run binaries.
              # Are there binaries in our connections?
              # We will know there are if the runnable array isn't empty.
              if !runnable.empty?
                # Well, then, don't run these by themselves, run those WITH us!
                runnable.each do |runnable_input|
                  # They become an input
                  inputs << runnable_input[0]
                  # Their input becomes our input
                  inputs.concat(runnable_input[1])
                end

                runnable   = []
                corunnable = []

                if connection.co_runnable
                  corunnable = [[hash, inputs]]
                  inputs = []
                else
                  runnable = [[hash, inputs]]
                  inputs = [hash]
                end
              else
                # Ok, add ourselves to runnable
                if connection.co_runnable
                  ret_corunnable << [hash, inputs]
                else
                  ret_runnable << [hash, inputs]
                end
                inputs = []
              end
            else
              # Ok, add ourselves to runnable
              if connection.co_runnable == 1
                ret_corunnable << [hash, inputs]
              else
                ret_runnable << [hash, inputs]
              end
              inputs = []
            end
          end

          if object and !running
            # This object doesn't run, it just becomes an input
            inputs << hash
          end

          # Spawn work for all runnables
          runnable.each do |hash, inputs|
            object = hash["object"]
            this_connection = hash["connection"]

            # Get binary
            binary = nil
            if self.configuration[this_connection.id.to_s].has_key? "__binaries"
              binary = self.configuration[this_connection.id.to_s]["__binaries"]
              self.configuration[this_connection.id.to_s].delete "__binaries"
            end

            # Create inputs
            input = {
              "type" => "run",

              "build" => {
                "using" => "ubuntu-base-2014-06",
              },

              "input" => {
                object.object_type => [
                  {
                    "type"          => object.object_type,
                    "id"            => object.id.to_s,
                    "name"          => object.name,
                    "corunning"     => false,
                    "url"           => object.script_path,
                    "configuration" => self.configuration[this_connection.id.to_s]
                  }
                ]
              }
            }

            # Add it as corunning in the input section if we are corunning things
            unless corunnable.empty?
              input["input"][object.object_type].first["corunning"] = true
            end

            if binary
              input["input"][object.object_type].first["binary"] = binary
            end

            input["run"] = {
              "id"        => object.id.to_s,
              "corunning" => false,
              "type"      => object.object_type,
              "index"     => 0,
              "name"      => object.name,
              "url"       => object.script_path,
            }.merge(object.run_metadata)

            inputs.each do |hash|
              input_object = hash["object"]
              input_connection = hash["connection"]

              input["input"][input_object.object_type] ||= []
              input["run"]["input"] ||= {}
              input["run"]["input"][input_object.object_type] ||= []
              input["run"]["input"][input_object.object_type].append(input["input"][input_object.object_type].length)

              input_hash = {
                "id"            => input_object.id.to_s,
                "corunning"     => false,
                "type"          => input_object.object_type,
                "name"          => input_object.name,
                "url"           => input_object.script_path,
                "configuration" => {}
              }

              if input_object.init_metadata and not input_object.init_metadata.keys.empty?
                input_hash["init"] = input_object.init_metadata
              end

              if self.configuration.has_key? input_connection.id.to_s
                # Get binary
                if self.configuration[input_connection.id.to_s].has_key? "__binaries"
                  input_hash["binary"] = self.configuration[input_connection.id.to_s]["__binaries"]
                  self.configuration[input_connection.id.to_s].delete "__binaries"
                end

                input_hash["configuration"] = self.configuration[input_connection.id.to_s]
              end

              input["input"][object.object_type][0]["input"] ||= []
              input["input"][object.object_type][0]["input"] <<
                input["input"][input_object.object_type].length
              input["input"][input_object.object_type] << input_hash
              puts "adding input to #{object.object_type}"
            end

            realized_co_inputs = []

            # Spawn jobs
            if corunnable.empty?
              puts "**! Spawn job: #{object.object_type}-#{object.name} inputs: #{input}"

              Job.create(:experiment       => self,
                         :kind             => "run",
                         :input            => input,
                         :codependant      => false,
                         :has_dependencies => false,
                         :status           => "queued")
            else
              # Mark it as corunning in the run section
              input["run"]["corunning"] = true

              ActiveRecord::Base.transaction do
                # Spawn co-dependent-jobs
                corunnable.each do |co_hash, co_inputs|
                  co_object = co_hash["object"]
                  co_connection = co_hash["connection"]

                  co_input = {
                    "type" => "run",
                    "build" => {
                      "using" => "ubuntu-base-2014-06"
                    },
                    "input" => {
                      co_object.object_type => [
                        "id"            => co_object.id.to_s,
                        "corunning"     => true,
                        "type"          => co_object.object_type,
                        "name"          => co_object.name,
                        "url"           => co_object.script_path,
                        "configuration" => self.configuration[co_connection.id.to_s]
                      ]
                    }
                  }

                  # Get binary
                  if self.configuration.has_key? co_connection.id.to_s
                    if self.configuration[co_connection.id.to_s].has_key? "__binaries"
                      co_input["input"][object.object_type].first["binary"] = self.configuration[co_connection.id.to_s]["__binaries"]
                      self.configuration[co_connection.id.to_s].delete "__binaries"
                    end
                  end

                  # We will add the co-dependant job's input to main task inputs
                  input["input"][co_object.object_type] ||= []
                  co_object_index = input["input"][co_object.object_type].length
                  input["input"][co_object.object_type] << co_input["input"][co_object.object_type].first.dup

                  co_input["run"] = {
                    "id"        => co_object.id.to_s,
                    "index"     => co_object_index,
                    "corunning" => true,
                    "type"      => co_object.object_type,
                    "name"      => co_object.name,
                    "url"       => co_object.script_path,
                  }.merge(co_object.run_metadata)

                  input["run"]["input"] ||= {}
                  input["run"]["input"][co_object.object_type] ||= []
                  input["run"]["input"][co_object.object_type] <<  co_object_index

                  input["input"][object.object_type][0]["input"] ||= {}
                  input["input"][object.object_type][0]["input"][co_object.object_type] ||= []
                  input["input"][object.object_type][0]["input"][co_object.object_type] <<
                    co_object_index

                  main_input = input["input"][co_object.object_type].last
                  main_input["input"] = {}

                  # For every input to our co-dependant task, form inputs for
                  # co-dependant task and main task
                  co_inputs.each do |input_hash|
                    input_object = input_hash["object"]
                    input_connection = input_hash["connection"]

                    co_input["input"][input_object.object_type] ||= []
                    co_input["run"]["input"] ||= {}
                    co_input["run"]["input"][input_object.object_type] ||= []
                    co_input["run"]["input"][input_object.object_type] <<
                      co_input["input"][input_object.object_type].length

                    input_hash = {
                      "id"            => input_object.id.to_s,
                      "corunning"     => false,
                      "type"          => input_object.object_type,
                      "name"          => input_object.name,
                      "url"           => input_object.script_path,
                      "configuration" => {}
                    }

                    if input_object.init_metadata and not input_object.init_metadata.keys.empty?
                      input_hash["init"] = input_object.init_metadata
                    end

                    if self.configuration.has_key? input_connection.id.to_s
                        # Get binary
                        if self.configuration[input_connection.id.to_s].has_key? "__binaries"
                          input_hash["binary"] = self.configuration[input_connection.id.to_s]["__binaries"]
                          self.configuration[input_connection.id.to_s].delete "__binaries"
                        end

                        input_hash["configuration"] = self.configuration[input_connection.id.to_s]
                    end

                    # Add this input to our main task:
                    input["input"][input_object.object_type] ||= []

                    # Also, add this input to the representation of the co-dependant
                    # task in the main task object:
                    input["input"][co_object.object_type][co_object_index]["input"] ||= {}
                    input["input"][co_object.object_type][co_object_index]["input"][input_object.object_type] ||= []
                    input["input"][co_object.object_type][co_object_index]["input"][input_object.object_type] <<
                      input["input"][input_object.object_type].length

                    # Add in the corunning hash
                    co_input["input"][co_object.object_type][0]["input"] ||= {}
                    co_input["input"][co_object.object_type][0]["input"][input_object.object_type] ||= []
                    co_input["input"][co_object.object_type][0]["input"][input_object.object_type] <<
                      co_input["input"][input_object.object_type].length

                    # Actually add the subtask to the main-task
                    input["input"][input_object.object_type] << input_hash

                    # Add the input to the co-dependant task object
                    co_input["input"][input_object.object_type] << input_hash
                  end

                  realized_co_inputs << co_input
                end

                jobs = realized_co_inputs.map do |co_input|
                  # All co-dependant jobs share the same inputs
                  co_input["input"] = input["input"]

                  puts "*****"
                  pp co_input

                  Job.create(:experiment       => self,
                             :kind             => "run",
                             :input            => co_input,
                             :codependant      => true,
                             :has_dependencies => false,
                             :status           => "queued")
                end

                puts "** Spawn co-job: #{object.object_type}-#{object.name} with #{corunnable.map(&:first).map{|hash| hash["object"].object_type}} inputs: #{input}"
                puts "*****"
                pp input

                job = Job.create(:experiment       => self,
                                 :kind             => "run",
                                 :input            => input,
                                 :codependencies   => jobs,
                                 :codependant      => false,
                                 :has_dependencies => false,
                                 :status           => "queued")
              end
            end
          end

          puts "returning #{inputs}, #{ret_runnable}, #{corunnable}"
          return [inputs, ret_runnable, corunnable]
        end

        spawn_jobs

        # Crawl the job tree and commit the dependencies and spawn the jobs:
=begin
        runnable.each do |object, inputs|
        end
=end
      end
    end

    def self.all_tags(with = "")
      self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Experiment.where('tags LIKE ?', "%;#{tag};%")
    end

    def forked_us
      Experiment.where(:forked_from_id => self.id).select([:name, :id])
    end

    def self.basic_search(term)
      Experiment.where('name LIKE ?', "%#{term}%").select([:name, :id])
    end

    def self.search_sim(name, tags, simulator, forked)
      query = Experiment.where('name LIKE ?', "%#{name}%")
      tags = tags.split ;
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%#{tag}%")
      end
      if simulator > 0
        query = query.where('simulator_id = ?', "#{simulator}")
      end
     #query = query.where('account_id LIKE ?', "%#{account}%")
      if forked == 1
        query = query.where('forked_from_id IS NOT NULL')
      elsif forked == 2
        query = query.where('forked_from_id IS NULL')
      end
      query = query.select([:name, :id])
    end

    def self.search(params)
      data = {}
      if params["simulator_id"] != "___any___"
        simulator = Object.where(:type => :simulator).find_by(:id => params["simulator_id"].to_i)

        if params["data"] && params["data"].is_a?(Array)
          data = params["data"].first

          schema = simulator.input_schema
          Occam::Configuration.type_check(data, schema, nil, true)
        end
      end

      query = Experiment
      query = query.where('name LIKE ?', "%#{params["name"]}%") if params["name"] && params["name"] != ""

      tags = params["tags"] || ""

      tags = tags.split(';')
      tags.each do |tag|
        query = query.where('tags LIKE ?', "%;#{tag};%")
      end

      query = query.where('simulator_id = ?', "#{simulator.id}") if simulator
      query = query.where('forked_from_id IS NOT NULL') if params["forked"]

      # Configuration Facet Search
      # Find configurations in the document store and add the final sql query for those ids
      unless data.empty?
        configurations = Occam::Configuration.search(data)
        if configurations.count > 0
          query = query.where('configuration_document_id IN (?)', configurations.to_a.map{|e| e.id.to_s})
        else
          return []
        end
      end

      query = query.select([:name, :id])
    end

    def job
      Job.find_by(:experiment_id => self.id)
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end
  end
end
