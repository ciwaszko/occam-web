class Occam
  class Recipe < ActiveRecord::Base
    require 'json'

    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true

    # object    - The object to configure.
    belongs_to :object, :foreign_key => :occam_object_id
    validates  :object, :presence => true

    # configuration_document_id - The simulconfiguration JSON.
    def configuration
      document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      document = Occam::Configuration.first(:id => document_id)

      if document
        document.serializable_hash
      end
    end

    def configuration=(value)
      document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      document = Occam::Configuration.first(:id => document_id)

      if document
        document.set(value)
        document.save
      else
        document = Occam::Configuration.create(value)
        self.configuration_document_id = document.id.to_s
        self.save
      end
      document
    end
  end
end
