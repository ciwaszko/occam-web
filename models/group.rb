class Occam
  class Group < ActiveRecord::Base
    # Fields

    # id        - Unique identifier.

    # name      - The name of the experiment.
    validates :name, :presence => true

    # tags      - The semicolon separated list of tags.

    # Experiment has a group_id foreign key
    has_many :experiments

    # account   - The account that created this group.
    belongs_to :account

    # workset - The Workset this group belongs to.
    belongs_to :workset

    # workflow - The Workflow this group uses.
    has_one :workflow

    # recipe - Groups can be based off of configurations.
    has_one :recipe

    # configuration - The configuration JSON.
    def has_configuration?
      self.configuration_document_id != nil
    end

    def configuration
      document_id = BSON::ObjectId.from_string(self.configuration_document_id)

      document = Occam::Configuration.first(:id => document_id)

      if document
        document.serializable_hash
      end
    end

    def configuration=(value)
      if self.configuration_document_id
        document_id = BSON::ObjectId.from_string(self.configuration_document_id)
        document = Occam::Configuration.first(:id => document_id)
      end

      if document
        document.attributes = value
        document.save
      else
        document = Occam::Configuration.create(value)
        self.configuration_document_id = document.id.to_s
        self.save
      end
      document
    end

    # forked_from - The experiment this one was based off of (if any).
    belongs_to :forked_from, :class_name => "Experiment"

    def can_view?(account)
      if self.private
      else
        true
      end
    end

    # Whether or not there are results for any experiment within this group.
    def results?
      !self.experiments.select{|e| e.results?}.empty?
    end

    # Returns some aggregate understanding of the status of this group.
    # If all jobs have completed, it will be finished.
    # The rest of the statuses override this in this order:
    # If any job is unqueued, it will be unqueued.
    # If any job is queued, it will be queued.
    # If any job is running, it will be running.
    # If any job has failed, it will have failed.
    def status
      # Assume it is finished
      status = :finished

      order = [:unqueued, :queued, :running, :failed, :finished]

      self.experiments.map do |experiment|
        job_status =
          if experiment.job
            experiment.job.status
          else
            # Some job is unqueued
            :unqueued
          end

        if order.index(job_status) < order.index(status)
          status = job_status
        end
      end

      status
    end

    def self.all_tags(with = "")
      self.all.to_a.map{|ex| ex.tags[1..-2].split(";")}.flatten.uniq.select{|tag| tag.match(Regexp.escape(with))}
    end

    def self.all_tags_json(with = "")
      self.all_tags(with).to_json
    end

    def self.with_tag(tag)
      Group.where('tags LIKE ?', "%;#{tag};%")
    end

    def forked_us
      Group.where(:forked_from_id => self.id).select([:name, :id])
    end

    def initialize(options = {}, *args)
      # Ensure a leading and ending ',' in the tag CSV
      options[:tags] ||= ""
      if options[:tags].is_a? Array
        options[:tags] = ";#{options[:tags].join(';')};"
      end

      if !options[:tags].start_with?(";")
        options[:tags] = ";#{options[:tags]}"
      end
      if !options[:tags].end_with?(";")
        options[:tags] = "#{options[:tags]};"
      end

      super options, *args
    end
  end
end
