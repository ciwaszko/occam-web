require 'sinatra'
require 'sinatra/activerecord'

class Occam < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  # Load configuration

  config_file_path = File.join(File.expand_path(File.dirname(__FILE__)), 'config.yml')
  unless File.exists?(config_file_path)
    require 'fileutils'
    FileUtils.cp "#{config_file_path}.sample", config_file_path
  end
  config = YAML.load_file(config_file_path)

  # Default to development
  if ENV['RACK_ENV'].nil?
    ENV['RACK_ENV'] = "development"
  end

  if config[ENV['RACK_ENV']]["document_store"]["adapter"] == 'mongodb'
    require 'mongo_mapper'
  end

  if config[ENV['RACK_ENV']]["database"]["adapter"] == 'postgres'
    require 'pg'
  elsif config[ENV['RACK_ENV']]["database"]["adapter"] == 'sqlite3'
    require 'sqlite3'
  else
    throw "Unknown database adapter '#{config[ENV['RACK_ENV']]["database"]["adapter"]}'"
  end

  require_relative '../config/application'

  [:test, :development, :production].each do |env|
    configure env do
      # Set up database
      database_opts = config[env.to_s]["database"]

      options = {}

      if database_opts["uri"]
        db = URI.parse(database_opts["uri"])
        options[:adapter]  = db.scheme
        options[:host]     = db.host
        options[:database] = db.path[1..-1]
      end

      options[:adapter]  ||= database_opts["adapter"]
      options[:database] ||= database_opts["database"]
      options[:timeout]  ||= database_opts["timeout"]

      # Explicitly set encoding
      options[:encoding] = 'utf8'

      # Coerse adapter types for ActiveRecord use
      options[:adapter] = "postgresql" if options[:adapter] == 'postgres'

      # Default database location for sqlite3
      if options[:adapter] == 'sqlite3'
        options[:database] ||= File.join(File.expand_path(File.dirname(__FILE__)), '..', "#{env}.db")
      end

      # Set up ActiveRecord
      ActiveRecord::Base.establish_connection(options)

      # Document store options
      dstore_opts = config[env.to_s]["document_store"]
      options = {}

      if dstore_opts["adapter"] == 'mongodb'
        MongoMapper.database = dstore_opts["database"] || "occam-#{env.to_s}"
      end
    end
  end
end
