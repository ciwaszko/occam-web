source 'https://rubygems.org'
ruby '2.0.0', :patchlevel => '353'

# Web Framework
gem 'sinatra', '~> 1.4.4'
gem 'sinatra-contrib'

# Database Abstraction
gem 'sinatra-activerecord'
gem 'activerecord', '~> 4.0.0'

# Document Access
gem 'mongo'    # MongoDB
gem 'bson_ext' # BSON library for id hashing

# Document Abstraction
gem 'mongo_mapper', '~> 0.13.0.beta'

# Memory Cache / Worker Status Access
gem 'dalli'

# Markup Rendering Engine
gem 'haml'      # Haml
gem 'redcarpet' # Markdown
gem 'nokogiri'  # XML/HTML Parsing

# Time duration markup (270s => "4 mins 30 secs")
gem 'chronic_duration'

# Git access
gem 'grit'

# URL-Safe String Processing
gem 'stringex'

# Password hashing
gem 'bcrypt-ruby'

# Runs Rakefiles
gem 'rake'

# Development only tools
group :development do
  gem 'sqlite3'      # Development Database
  gem 'ruby-prof'    # Performance monitoring
end

# Testing environment libraries
group :test do
  gem 'capybara', '~> 1.1.2',  :require => 'capybara/dsl'
  gem 'fabrication', '~> 1.2.0'
  gem 'database_cleaner', '~> 1.2.0'
  gem 'rack-test', '~> 0.6.1', :require => 'rack/test'
  gem 'minitest', '~> 4.7.0', :require => 'minitest/autorun'
  gem "ansi"              # minitest colors
  gem "turn"              # minitest output
  gem "mocha"             # stubs
end

# Production level libraries
group :production do
  gem 'pg'
end

gem 'thin'

gem 'mail'
