require_relative 'helper'

describe Occam do
  describe "Accounts Controller" do
    describe "GET /accounts" do
      it "should pass an array of accounts to the view" do
        Occam::Account.create(:username => "wilkie",
                              :password => "foo")
        Occam::Account.create(:username => "wilkie2",
                              :password => "foo")
        Occam::Account.create(:username => "wilkie3",
                              :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:accounts => ActiveRecord::Relation::ActiveRecord_Relation_Occam_Account)
        )

        get '/accounts'
      end

      it "should query and pass the accounts to the view" do
        Occam::Account.create(:username => "wilkie",
                              :password => "foo")

        account = Occam::Account.create(:username => "wilkie3",
                                        :password => "foo")

        Occam::Account.create(:username => "wilkie2",
                              :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_includes(:accounts, account)
        )

        get '/accounts'
      end

      it "should return 200" do
        get '/accounts'

        last_response.status.must_equal 200
      end

      it "should render accounts/index.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/index",
          anything
        )

        get '/accounts'
      end
    end

    describe "GET /accounts/new" do
      it "should render accounts/new.haml" do
        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/new",
          anything
        )

        get '/accounts/new'
      end

      it "should return 200" do
        get '/accounts/new'

        last_response.status.must_equal 200
      end
    end

    describe "GET /accounts/:id/edit" do
      it "should render accounts/edit.haml when authorized" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(a.username, a)

        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/edit",
          anything
        )

        get "/accounts/#{a.id}/edit"
      end

      it "should return 200 when authorized" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(a.username, a)

        get "/accounts/#{a.id}/edit"

        last_response.status.must_equal 200
      end

      it "should return 404 when account is not found" do
        Occam.any_instance.stubs(:markdown)

        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(a.username, a)

        get "/accounts/asdf/edit"

        last_response.status.must_equal 404
      end

      it "should return 406 when unauthorized" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        b = Occam::Account.create(:username => "jane",
                                  :password => "foo")

        login_as(b.username, b)

        get "/accounts/#{a.id}/edit"

        last_response.status.must_equal 406
      end
    end

    describe "GET /accounts/:id" do
      it "should render accounts/show.haml" do
        account = Occam::Account.create(:username => "wilkie",
                                        :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/show",
          anything
        )

        get "/accounts/#{account.id}"
      end

      it "should query and pass the given account" do
        account = Occam::Account.create(:username => "wilkie",
                                        :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:account, account)
        )

        get "/accounts/#{account.id}"
      end

      it "should return 200 when account is found" do
        account = Occam::Account.create(:username => "wilkie",
                                        :password => "foobar")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local(:account, account)
        )

        get "/accounts/#{account.id}"

        last_response.status.must_equal 200
      end

      it "should return 404 when account is not found" do
        Occam.any_instance.stubs(:markdown)

        get "/accounts/1234"

        last_response.status.must_equal 404
      end
    end

    describe "POST /accounts" do
      it "should create an account with the given username" do
        session = stub('session')
        session.stubs(:[]=).with(:account_id, anything)
        Occam.any_instance.stubs(:session).returns(session)

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        Occam::Account.find_by(:username => "wilkie").username.must_equal "wilkie"
      end

      it "should return 302 when account is created" do
        session = stub('session')
        session.stubs(:[]=).with(:account_id, anything)
        Occam.any_instance.stubs(:session).returns(session)

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 302
      end

      it "should redirect to account page when account is created" do
        session = stub('session')
        session.stubs(:[]=).with(:account_id, anything)
        Occam.any_instance.stubs(:session).returns(session)

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        account = Occam::Account.find_by(:username => "wilkie")

        last_response.location.must_equal(
          "http://example.org/accounts/#{account.id}")
      end

      it "should return 422 when the account cannot be created" do
        Occam::Account.create(:username => "wilkie",
                              :password => "foo")

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }

        last_response.status.must_equal 422
      end

      it "should render the account creation form when the account cannot be created" do
        Occam::Account.create(:username => "wilkie",
                              :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          :"accounts/new",
          anything
        )

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end

      it "should pass along errors to be rendered when the account cannot be created" do
        Occam::Account.create(:username => "wilkie",
                              :password => "foo")

        Occam.any_instance.expects(:render).with(
          anything,
          anything,
          has_local_of_type(:errors, ActiveModel::Errors)
        )

        post "/accounts", {
          "username" => "wilkie",
          "password" => "foobar"
        }
      end
    end

    describe "POST /accounts/:id" do
      it "should return a 404 if the account is not found" do
        Occam.any_instance.stubs(:markdown)

        post "/accounts/0", {}

        last_response.status.must_equal 404
      end

      it "should return a 406 if the account is not authorized to update" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        b = Occam::Account.create(:username => "jane",
                                  :password => "foo")

        login_as(b.username, b)

        post "/accounts/#{a.id}", {
          "username" => "foobar"
        }

        last_response.status.must_equal 406
      end

      it "should not allow changes unless the account is currently logged in" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        post "/accounts/#{a.id}", {
          "username" => "foobar"
        }

        Occam::Account.find_by(:id => a.id).username.must_equal "wilkie"
      end

      it "should allow changing of email" do
        a = Occam::Account.create(:username => "wilkie",
                                  :email    => "foo@example.org",
                                  :password => "foo")

        login_as(a.username, a)

        post "/accounts/#{a.id}", {
          "email" => "foobar@example.org"
        }

        Occam::Account.find_by(:id => a.id).email.must_equal "foobar@example.org"
      end

      it "should allow changing of organization" do
        a = Occam::Account.create(:username     => "wilkie",
                                  :organization => "foobar",
                                  :password     => "foo")

        login_as(a.username, a)

        post "/accounts/#{a.id}", {
          "organization" => "bazfoo"
        }

        Occam::Account.find_by(:id => a.id).organization.must_equal "bazfoo"
      end

      it "should allow changing of display_name" do
        a = Occam::Account.create(:username     => "wilkie",
                                  :display_name => "foobar",
                                  :password     => "foo")

        login_as(a.username, a)

        post "/accounts/#{a.id}", {
          "display_name" => "bazfoo"
        }

        Occam::Account.find_by(:id => a.id).display_name.must_equal "bazfoo"
      end

      it "should allow changing of bio" do
        a = Occam::Account.create(:username => "wilkie",
                                  :bio      => "foobar",
                                  :password => "foo")

        login_as(a.username, a)

        post "/accounts/#{a.id}", {
          "bio" => "bazfoo"
        }

        Occam::Account.find_by(:id => a.id).bio.must_equal "bazfoo"
      end

      it "should allow changing of username" do
        a = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(a.username, a)

        post "/accounts/#{a.id}", {
          "username" => "foobar"
        }

        Occam::Account.find_by(:id => a.id).username.must_equal "foobar"
      end

      it "should not allow activation if not an administrator" do
        Occam::System.first.update_attributes(:moderate_accounts => true)

        # First account *is* an administrator
        a = Occam::Account.create(:username => "admin",
                                  :password => "foo")

        b = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(b.username, b)

        post "/accounts/#{b.id}", {
          "active" => "1"
        }

        Occam::Account.find_by(:id => b.id).active.must_equal 0
      end

      it "should allow activation by an administrator" do
        Occam::System.first.update_attributes(:moderate_accounts => true)

        # First account *is* an administrator
        a = Occam::Account.create(:username => "admin",
                                  :password => "foo")

        b = Occam::Account.create(:username => "wilkie",
                                  :password => "foo")

        login_as(a.username, a)

        post "/accounts/#{b.id}", {
          "active" => "1"
        }

        Occam::Account.find_by(:id => b.id).active.must_equal 1
      end
    end
  end
end
