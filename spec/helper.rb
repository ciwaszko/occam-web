# Load the testing framework
require 'minitest/spec'
require 'turn/autorun'

# Configure the output
Turn.config do |c|
  c.natural = true
end

# Load a stubbing framework
require "mocha/setup"

# Test Environment
ENV['RACK_ENV'] = 'test'

# Load the framework
require "sinatra"

# Merge in rack testing methods
require 'rack/test'
include Rack::Test::Methods

# Load environment
require_relative "../config/environment"

# Database Clean-up between tests
require 'database_cleaner'
DatabaseCleaner.strategy = :transaction

class MiniTest::Unit::TestCase
  def setup
    DatabaseCleaner.start
  end

  def teardown
    DatabaseCleaner.clean

    MongoMapper.database.collections.each do |collection|
      collection.remove unless collection.name.match /^system\./
    end
  end
end

# Create test database
ActiveRecord::Migrator.up "db/migrate"
