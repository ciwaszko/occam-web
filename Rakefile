#!/usr/bin/env rake

if ARGV[0] && ARGV[0].start_with?('test')
  ENV['RACK_ENV'] = "test"
end

require 'rake/testtask'
require 'sinatra/activerecord/rake'
require_relative './config/environment'

Rake::TestTask.new do |t|
  t.pattern = "spec/**/*_spec.rb"
end

namespace :test do
  desc "Run all tests (rake test will do this be default)"
  task :all do
    Rake::TestTask.new("all") do |t|
      t.pattern = "spec/**/*_spec.rb"
    end
    task("all").execute
  end

  desc "Run single model"
  task :model, :file do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = "spec/models/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
      else
        t.pattern = "spec/*_test.rb"
      end
    end
    task("unittests").execute
  end

  desc "Run single controller"
  task :controller, :file do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = "spec/controllers/#{args.file}_spec.rb"
        t.pattern = file
        puts "Testing #{file}"
      else
        t.pattern = "spec/*_test.rb"
      end
    end
    task("unittests").execute
  end

  desc "Run single file"
  task :file, :file do |task, args|
    test_task = Rake::TestTask.new("unittests") do |t|
      if args.file
        file = args.file
        unless file.start_with? "spec/"
          file = "spec/#{args.file}"
        end
        t.pattern = file
        puts "Testing #{file}"
      else
        t.pattern = "spec/**/*_test.rb"
      end
    end
    task("unittests").execute
  end
end

namespace :docs do
  desc "Generate a static version of the specification docs"
  task :specs do
    require 'fileutils'
    require './lib/application'
    FileUtils.mkdir_p "docs"
    FileUtils.mkdir_p "docs/specs"

    occam = Occam.new!

    File.open("docs/specs/index.html", "w+") do |file|
      file.write occam.render_documentation_index(true)
    end

    Dir[File.join(File.dirname(__FILE__), "views", "static", "documentation", '*.md')].each do |fname|
      tag = File.basename(fname, ".md")
      next if tag == "index"
      File.open("docs/specs/#{tag}.html", "w+") do |file|
        file.write occam.render_documentation(tag, true)
      end
    end
    task("specs").execute
  end
end
