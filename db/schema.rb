# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141022150118) do

  create_table "accounts", force: true do |t|
    t.string   "username"
    t.string   "hashed_password"
    t.string   "display_name"
    t.string   "email"
    t.string   "organization"
    t.text     "bio"
    t.string   "roles"
    t.integer  "email_public"
    t.integer  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "binaries", force: true do |t|
    t.string  "name"
    t.integer "occam_object_id"
  end

  create_table "collaborations", force: true do |t|
    t.integer  "workset_id"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "connections", force: true do |t|
    t.integer "input_object_id"
    t.integer "output_object_id"
    t.integer "run_order"
    t.integer "workflow_id"
    t.integer "occam_object_id"
    t.integer "object_output_id"
    t.integer "object_input_id"
    t.string  "creates_object_of_type"
    t.integer "co_runnable"
    t.integer "connection_id"
  end

  create_table "dependencies", force: true do |t|
    t.integer  "depends_on_id"
    t.integer  "dependant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "experiments", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.string   "configuration_document_id"
    t.integer  "workset_id"
    t.integer  "group_id"
    t.integer  "workflow_id"
    t.integer  "account_id"
    t.integer  "forked_from_id"
    t.integer  "recipe_id"
    t.boolean  "private"
    t.string   "results_document_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "experiments", ["forked_from_id"], name: "index_experiments_on_forked_from_id"

  create_table "groups", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.integer  "occam_object_id"
    t.string   "configuration_document_id"
    t.integer  "workset_id"
    t.integer  "account_id"
    t.integer  "forked_from_id"
    t.integer  "recipe_id"
    t.boolean  "private"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["forked_from_id"], name: "index_groups_on_forked_from_id"

  create_table "job_codependencies", force: true do |t|
    t.integer "job_id"
    t.integer "depends_on_job_id"
  end

  add_index "job_codependencies", ["depends_on_job_id"], name: "index_job_codependencies_on_depends_on_job_id"
  add_index "job_codependencies", ["job_id"], name: "index_job_codependencies_on_job_id"

  create_table "job_dependencies", force: true do |t|
    t.integer "job_id"
    t.integer "depends_on_job_id"
  end

  add_index "job_dependencies", ["depends_on_job_id"], name: "index_job_dependencies_on_depends_on_job_id"
  add_index "job_dependencies", ["job_id"], name: "index_job_dependencies_on_job_id"

  create_table "jobs", force: true do |t|
    t.integer  "experiment_id"
    t.integer  "occam_object_id"
    t.string   "status"
    t.string   "kind"
    t.integer  "depends_on_id"
    t.text     "depends_on_ids"
    t.string   "log_file"
    t.float    "elapsed_time"
    t.text     "raw_configuration_file_paths"
    t.text     "raw_output_file_paths"
    t.string   "input_document_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dependencies"
    t.integer  "codependant"
    t.integer  "has_dependencies"
  end

  create_table "object_inputs", force: true do |t|
    t.integer "occam_object_id"
    t.string  "object_group"
    t.string  "object_type"
    t.integer "fifo"
  end

  create_table "object_outputs", force: true do |t|
    t.integer "occam_object_id"
    t.string  "object_group"
    t.string  "object_type"
    t.integer "fifo"
  end

  create_table "objects", force: true do |t|
    t.string   "name"
    t.string   "object_type"
    t.text     "tags"
    t.text     "description"
    t.text     "authors"
    t.string   "organization"
    t.string   "license"
    t.string   "website"
    t.string   "documentation"
    t.integer  "built"
    t.string   "binary_path"
    t.string   "script_path"
    t.string   "package_type"
    t.text     "self_metadata"
    t.text     "install_metadata"
    t.text     "build_metadata"
    t.text     "run_metadata"
    t.integer  "runs_binaries"
    t.string   "local_path"
    t.string   "group"
    t.integer  "active"
    t.string   "input_schema_document_id"
    t.string   "output_schema_document_id"
    t.datetime "published"
    t.datetime "updated"
  end

  create_table "recipes", force: true do |t|
    t.string  "name"
    t.string  "section"
    t.text    "description"
    t.integer "occam_object_id"
    t.string  "configuration_document_id"
  end

  create_table "systems", force: true do |t|
    t.string  "jobs_path"
    t.string  "objects_path"
    t.boolean "moderate_objects"
    t.boolean "moderate_accounts"
    t.boolean "curate_git"
    t.boolean "curate_hg"
    t.boolean "curate_svn"
    t.boolean "curate_files"
  end

  create_table "workflows", force: true do |t|
    t.string  "name"
    t.integer "group_id"
    t.integer "experiment_id"
    t.integer "workset_id"
    t.integer "account_id"
    t.integer "forked_id"
  end

  create_table "worksets", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.text     "description"
    t.integer  "occam_object_id"
    t.string   "configuration_document_id"
    t.integer  "account_id"
    t.integer  "forked_from_id"
    t.integer  "recipe_id"
    t.integer  "private"
    t.string   "results_document_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
