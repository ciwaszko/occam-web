class CreateAccounts < ActiveRecord::Migration
  def up
    create_table :accounts do |t|
      t.string  :username
      t.string  :hashed_password

      t.string  :display_name
      t.string  :email
      t.string  :organization

      t.text    :bio

      t.string  :roles

      t.integer :email_public
      t.integer :active

      t.timestamps
    end
  end

  def down
    drop_table :accounts
  end
end
