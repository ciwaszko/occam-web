class CreateJobDependencies < ActiveRecord::Migration
  def up
    create_table :job_dependencies do |t|
      t.integer :job_id
      t.integer :depends_on_job_id
    end

    add_index :job_dependencies, :job_id
    add_index :job_dependencies, :depends_on_job_id
  end

  def down
    drop_table :job_dependencies
  end
end
