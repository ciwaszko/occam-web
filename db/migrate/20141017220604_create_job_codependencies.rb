class CreateJobCodependencies < ActiveRecord::Migration
  def up
    create_table :job_codependencies do |t|
      t.integer :job_id
      t.integer :depends_on_job_id
    end

    add_index :job_codependencies, :job_id
    add_index :job_codependencies, :depends_on_job_id
  end

  def down
    drop_table :job_codependencies
  end
end
