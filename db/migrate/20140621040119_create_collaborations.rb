class CreateCollaborations < ActiveRecord::Migration
  def up
    create_table :collaborations do |t|
      t.integer :workset_id
      t.integer :account_id

      t.timestamps
    end
  end

  def down
    drop_table :collaborations
  end
end
