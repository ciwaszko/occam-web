class CreateExperiments < ActiveRecord::Migration
  def up
    create_table :experiments do |t|
      t.string  :name

      t.text    :tags

      t.string  :configuration_document_id

      t.integer :workset_id
      t.integer :group_id

      t.integer :workflow_id

      t.integer :account_id
      t.integer :forked_from_id

      t.integer :recipe_id

      t.boolean :private

      t.string  :results_document_id

      t.timestamps
    end

    add_index :experiments, :forked_from_id
  end

  def down
    drop_table :experiments
  end
end
