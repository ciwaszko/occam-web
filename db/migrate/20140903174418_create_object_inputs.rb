class CreateObjectInputs < ActiveRecord::Migration
  def up
    create_table :object_inputs do |t|
      t.integer :occam_object_id

      t.string  :object_group
      t.string  :object_type
      t.integer :fifo
    end
  end

  def down
    drop_table :object_inputs
  end
end
