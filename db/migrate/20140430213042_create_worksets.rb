class CreateWorksets < ActiveRecord::Migration
  def up
    create_table :worksets do |t|
      t.string  :name
      t.text    :tags
      t.text    :description

      t.integer :occam_object_id
      t.string  :configuration_document_id

      t.integer :account_id
      t.integer :forked_from_id

      t.integer :recipe_id

      t.integer :private

      t.string  :results_document_id

      t.timestamps
    end
  end

  def down
    drop_table :worksets
  end
end
