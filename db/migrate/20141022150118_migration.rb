class Migration < ActiveRecord::Migration
  def change
    change_table :object_inputs do |t|
      t.integer :fifo
    end

    change_table :object_outputs do |t|
      t.integer :fifo
    end

    change_table :jobs do |t|
      t.integer :codependant
      t.integer :has_dependencies
    end

    change_table :connections do |t|
      t.integer :object_output_id
      t.integer :object_input_id
      t.string  :creates_object_of_type
      t.integer :co_runnable
      t.integer :connection_id
    end
  end
end
