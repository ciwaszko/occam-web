class CreateSystems < ActiveRecord::Migration
  def up
    create_table :systems do |t|
      t.string :jobs_path
      t.string :objects_path

      t.boolean :moderate_objects
      t.boolean :moderate_accounts

      t.boolean :curate_git
      t.boolean :curate_hg
      t.boolean :curate_svn
      t.boolean :curate_files
    end
  end

  def down
    drop_table :systems
  end
end
