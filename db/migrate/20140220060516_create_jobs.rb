class CreateJobs < ActiveRecord::Migration
  def up
    create_table :jobs do |t|
      t.integer :experiment_id
      t.integer :occam_object_id

      t.string  :status
      t.string  :kind

      t.string  :log_file
      t.float   :elapsed_time

      t.text    :raw_configuration_file_paths
      t.text    :raw_output_file_paths

      t.integer :codependant
      t.integer :has_dependencies

      t.string  :input_document_id

      t.timestamps
    end
  end

  def down
    drop_table :jobs
  end
end
