class CreateObjectOutputs < ActiveRecord::Migration
  def up
    create_table :object_outputs do |t|
      t.integer :occam_object_id

      t.string  :object_group
      t.string  :object_type
      t.integer :fifo
    end
  end

  def down
    drop_table :object_outputs
  end
end
