class CreateWorkflows < ActiveRecord::Migration
  def up
    create_table :workflows do |t|
      t.string :name

      t.integer :group_id
      t.integer :experiment_id
      t.integer :workset_id

      t.integer :account_id

      t.integer :forked_id
    end
  end

  def down
    drop_table :workflows
  end
end
