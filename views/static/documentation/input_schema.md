# Input Schema

Simulator input and configuration options are described within an `input_schema.json` file.
This is simply a JSON file containing a hierarchical listing of all options.

``` javascript
{
  "memory_size": {
    "type": "int",
    "default": "2048",
    "label": "Memory Size (in MB)",
    "description": "The amount of memory to simulate in total."
  },

  "cycle_count": {
    "type": "int",
    "default": "30",
    "label": "Cycle Count",
    "description": "The number of cycles to run the simulation."
  },

  "num_chans": {
    "type": "int",
    "default": 1,
    "label": "Number of Channels",
    "validations": [
      {
        "min":    "1"
      },
      {
        "test":    "log2(x) == floor(log2(x))",
        "message": "Must be a power of two"
      }
    ],
    "description": "The number of memory channels to simulate."
  },

  "Debugging Options": {
    "debug_banks": {
      "type": "boolean",
      "default": false,
      "label": "Output Bank Debugging Statements"
    },

    "debug_power": {
      "type": "boolean",
      "default": false,
      "label": "Output Power Debugging Statements"
    }
  }
}
```

Every option is defined by a JSON key and value pair.
In the example above, `num_chans` is an option that defines an integer, has a default value of `1`, is displayed to the user as "Number of Channels" with the given description.
This option also has several validations defined.
It must be at least `1` and pass an equivalence test to ensure it is a power of two.

Groups can be created by nesting a json object. In the example above `Debugging Options` is a group with two boolean options. In the OCCAM configuration tool, this would show up as a label with "Debugging Options" that acts as a tree that can expand out to reveal the two options.

## Default

Use this tag to provide a default value.
This default *should* be valid for the given type.

## Description

This tag will be contain a description for the configuration option.
You can provide simple markdown to style the text.
You *should* be as descriptive as possible.
The configuration forms are optimized to hide the description text such that it is not overwhelming.
It is there to provide guidance.

```
{
  "num_chans": {
    "description": "The number of *logically independent* channels. That is, each with a separate memory controller.
                    Should be a power of 2.\n\n Ganged: A single command is broadcast to all ranks in all channels.
                    Effectively, this means an increase in the amount of data for a given request. For example,
                    in a ganged dual channel DDR3 setup, a single request would generate 2x64bits of data for
                    each beat. With a burst length of 8, this means 128 bytes of data is returned in 8 beats. This
                    is in contrast to a single channel of DDR3 which would produce 1x64bits of data for each beat
                    (64 byte in 8 beats). Note that dual channel transmits twice as much data in the same time
                    (8 beats) effectively doubling the bandwidth. However, this extra data is, in essence, an
                    increase in the prefetch length. It is not always the case that all of this data will in fact be
                    useful for the processor.\n\n**Unganged/Logically Independent Mode**: Effectively this breaks any
                    interdependence between channels and makes it look like there is a single memory controller
                    talking to each channel. Each channel receives separate commands. Although this can be more
                    efficient in terms of not prefetching unnecessary bits, the logic complexity of the memory
                    controller is multiplied by the number of channels.\n\nTo instantiate a memory system with N
                    unganged channels, set NUM_CHANS=N (for now, due to address mapping issues, N can only be a power
                    of 2).\n\nTo instantiate a memory system with N ganged channels, set NUM_CHANS=1 and set the
                    JEDEC_BUS_BITS to be N*64. In other words if you wanted a dual-channel ganged system, you'd set
                    JEDEC_BUS_BITS=128."
  }
}
```

## Label

This tag will be the display name for the configuration parameter.

## Type

Every option must be annotated with its type.
The following types are defined in the OCCAM system and have associated user input methods:

### int

An unbounded integer type.
This represents a whole number.
Negative values are permitted.

### float

A floating point value.
Negative values are permitted.

### string

A freeform text field.

## Validations

All input is validated for its input type.
Beyond that, many extra validations are available.
Specify them as an array in your `input_schema`.

All validations will be checked on both the client and server to ensure user input errors will be discovered *before* the simulator is queued and invoked.
Each validation may contain a message to be displayed to the user when the input is invalid.
Some validations have default messages.

```
"num_chans": {
  "type": "int",
  "default": 1,
  "label": "Number of Channels",
  "validations": [
    {
      "min":    "1"
    },
    {
      "test":    "log2(x) == floor(log2(x))",
      "message": "Must be a power of two"
    }
  ],
  "description": "The number of memory channels to simulate."
}
```

For `num_chans` above, the input will be validated against the type, `int`, to ensure it is a valid number.
It will then be checked against a `min` validation to make sure it is at least `1`.
If it is not valid, it will display a default message for a `min` validation.
It will also evaluate its value as `x` in the boolean expression given for the `test` validation.
If it is not equal, it will display the message `Must be a power of two`.

### min

Ensures that user input is greater than or equal to the given value.

### max

Ensures that user input is less than or equal to the given value.

### test

A boolean expression is given.
The user input can only pass validation if the boolean expression is true.
