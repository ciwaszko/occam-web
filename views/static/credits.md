# Acknowledgements

## Developers

* [Dave "wilkie" Wilkinson](http://wilkie.io)
* Brian Dicks
* Long Pham
* Ben Moncuso
* Jim Devine

## Artists

![left|!](/images/gear.png)
Gear designed by [Reed Enger](http://www.thenounproject.com/reed) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/file.png)
File designed by [Jamison Wieser](http://www.thenounproject.com/jamison) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/recipe.png)
Flask designed by [Renan Ferreira Santos](http://www.thenounproject.com/renanfsdesign) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/fork.png)
Fork designed by [Dmitry Baranovskiy](http://www.thenounproject.com/DmitryBaranovskiy) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/person.png)
User designed by [Andreas Bjurenborg](http://www.thenounproject.com/andreas.bjurenborg) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/add.png)
Add designed by [Kevin Kwok](http://www.thenounproject.com/Queesy) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/workset.png)
Collection based on design by [Pham Thi Dieu Linh](http://www.thenounproject.com/phdieuli) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/simulator_large.png)
Cpu based on design by [Michael Anthony](http://www.thenounproject.com/m.j.anthony) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/pillar.png)
Column based on design by [Benedikt Martens](http://www.thenounproject.com/Benedikt) from the [Noun Project](http://thenounproject.com).

<br>

![left|!](/images/benchmark_large.png)
Gear/Magnifying Glass based on design by [irene hoffman](http://www.thenounproject.com/i) from the [Noun Project](http://thenounproject.com).
