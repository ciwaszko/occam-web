class Occam
  get '/objects/import' do
    render :haml, :"objects/import", :locals => {
      :errors      => nil,
      :default_url => ""
    }
  end

  post '/objects/import' do
    if params.has_key? "url"
      url  = params["url"]
      json = Occam::Object.download_json(url)
      obj  = Occam::Object.import(url, json)
      deps = Occam::Object.all_unknown_dependencies(url, json)

      if obj.persisted?
        # Already exists
        render :haml, :"objects/import", :locals => {
          :errors => [["Object already exists."]],
          :default_url => params["url"]
        }
      else
        return render :haml, :"objects/import_confirm", :locals => {
          :errors       => obj.errors,
          :object       => obj,
          :dependencies => deps
        }
      end
    else
      status 404
    end
  end

  post '/objects' do
    object = Occam::Object.import_all(params["script_path"])

    if object.errors.any?
      status 422
      render :haml, :"objects/import", :locals => {
        :errors => object.errors,
        :default_url => params["script_path"]
      }
    else
      object.install
      object.build

      redirect "/objects/#{object.id}"
    end
  end

  # List all (with some pagination) objects
  # Parameters:
  #   "by_type" => The type of object to filter the results.
  get '/objects' do
    # We can query by type
    format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

    case format
    when 'text/html'
      # TODO: remove the simulator/benchmark bake-ins. Just give a tree view.
      simulators     = Object.all_active.where(:object_type => 'simulator').limit(3)
      simulator_tags = Object.all_tags('simulator')

      benchmarks     = Object.all_active.where(:object_type => 'benchmark').limit(3)
      benchmark_tags = Object.all_tags('benchmark')

      render :haml, :"objects/index", :locals => {
        :simulators     => simulators,
        :simulator_tags => simulator_tags,
        :benchmarks     => benchmarks,
        :benchmark_tags => benchmark_tags,
      }
    when 'application/json'
      # Return object metadata
      content_type 'application/json'
      object_list = Occam::Object.all_active
      if params["by_type"]
        object_list = object_list.where(:object_type => params["by_type"])
      end
      object_list.to_json
    end
  end

  # List all objects of a certain type
  get '/objects/types/:type' do
    objects = Occam::Object.where(:object_type => params[:type])
    tags = Occam::Object.all_tags(params[:type])

    render :haml, :"objects/type_index", :locals => {
      :objects => objects,
      :type    => params[:type],
      :tags    => tags,
      :tag     => nil
    }
  end

  get '/objects/:id/update' do
    object = Object.find_by(:id => params[:id].to_i)

    if object.nil?
      status 404
    else
      object.update
      redirect "/objects/#{object.id}"
    end
  end

  # Update a specific object
  post '/objects/:id', :auth => [:administrator] do
    object = Object.find_by(:id => params[:id].to_i)

    if object.nil?
      status 404
    else
      columns = {}

      # Administrators can set "active"
      columns = params.select { |k,_| [
          "active",
        ].include? k }

      unless columns.empty?
        object.update_columns columns
        object.save
      end

      redirect '/admin'
    end
  end

  # Explicit json content type
  get '/objects/:id.json' do
    object = Occam::Object.find_by(:id => params[:id])

    if object.nil?
      status 404
    else
      content_type 'application/json'
      object.to_json
    end
  end

  # View an object
  get '/objects/:id' do
    object = Occam::Object.find_by(:id => params[:id])

    if object.nil?
      status 404
    else
      job         = object.build_job
      install_job = object.install_job
      recipes     = object.recipes

      if job
        job_info = job.monitoring_info
      end
      if install_job
        install_job_info = install_job.monitoring_info
      end

      format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

      case format
      when 'text/html'
        render :haml, :"objects/type_show", :locals => {
          :object           => object,
          :install_job      => install_job,
          :job              => job,
          :job_info         => job_info,
          :install_job_info => install_job_info,
          :recipes          => recipes,
        }
      when 'application/json'
        # Return object metadata
        content_type 'application/json'
        object.to_json
      end
    end
  end

  get '/objects/:id/output_schema.json' do
    object = Object.find_by(:id => params[:id].to_i)

    content_type "application/json"
    object.output_schema(:json)
  end

  get '/objects/:id/input_schema.json' do
    object = Object.find_by(:id => params[:id].to_i)

    content_type "application/json"
    object.input_schema(:json)
  end

  get '/objects/:id/recipes/:recipe_id' do
    object = Object.find_by(:id => params[:id].to_i)
    if object.nil?
      status 404
    else
      recipe = Recipe.find_by(:id => params[:recipe_id].to_i)

      if recipe.nil?
        status 404
      else
        render :haml, :"objects/configuration", :locals => {
          :object => object,
          :recipe => recipe,
          :schema => object.input_schema,
        }
      end
    end
  end

  get '/objects/:id/configuration' do
    object = Object.find_by(:id => params[:id].to_i)

    render :haml, :"objects/configuration", :locals => {
      :object => object,
      :recipe => nil,
      :schema => object.input_schema,
    }
  end

  get '/objects/:id/output' do
    object      = Object.find_by(:id => params[:id].to_i)
    job         = object.build_job
    install_job = object.install_job

    job_info = {}
    install_job_info = {}

    if job
      job_info = job.monitoring_info || {}
    end

    if install_job
      install_job_info = install_job.monitoring_info || {}
    end

    job_info["job_log"] ||= ""
    install_job_info["job_log"] ||= ""

    data = {
      :build_log   => job_info["job_log"],
      :install_log => install_job_info["job_log"]
    }

    content_type "application/json"
    return data.to_json
  end

  # Return the collection of all available object types
  get '/objects/types' do
  end

  # Return the collection of all available inputs to this object
  # Parameters:
  #   "by_type" => The type of object to filter the results.
  get '/objects/:id/inputs' do
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    content_type "application/json"
    object.inputs.map do |input|
      input.serializable_hash.merge!({
        :indirect_objects => input.indirect_objects.where(:object_type => params["by_type"]).select(:name, :id),
        :objects          => input.objects.where(:object_type => params["by_type"]).select(:name, :id)
      })
    end.to_json
  end

  # Return the metadata about the specified input for this object
  get '/objects/:id/inputs/:input_id' do
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    input = Occam::ObjectInput.find_by(:occam_object_id => params[:id].to_i,
                                       :id => params[:input_id].to_i)
    status 404 and return if input.nil?

    content_type "application/json"

    input.serializable_hash.merge!({
      :indirect_objects => input.indirect_objects.select(:name, :id),
      :objects          => input.objects.select(:name, :id)
    }).to_json
  end
end
