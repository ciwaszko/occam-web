class Occam
  # Administration page (only for admin users)
  get '/admin', :auth => [:administrator] do
    # Gather system configuration
    system = System.all.first

    # Gather pending accounts
    accounts = Account.all_inactive

    # Gather pending simulators
    simulators = Occam::Object.all_inactive(:simulator)

    # Gather pending benchmarks
    benchmarks = Occam::Object.all_inactive(:benchmark)

    # Render form
    render :haml, :"admin/index", :locals => {
      :system     => system,
      :accounts   => accounts,
      :simulators => simulators,
      :benchmarks => benchmarks,
    }
  end
end
