class Occam
  # Get a list of all experiments
  get '/experiments' do
    if params["tag"]
      experiments = Experiment.with_tag params["tag"]
    else
      experiments = Experiment.all
    end

    render :haml, :"experiments/index", :locals => {
      :experiments => experiments,
      :tags        => Experiment.all_tags,
      :tag         => params["tag"]
    }
  end

  # Create a new experiment
  post '/experiments' do
    object = Object.find_by(:id => params["simulator"].to_i)

    if params["forked_from"]
      forked_from = Experiment.find_by(:id => params["forked_from"].to_i)
    end

    if params["recipe"]
      recipe = Recipe.find_by(:id => params["recipe"].to_i)

      input = recipe.configuration
      input.delete "id"
    end

    if params["workset"]
      workset = Workset.find_by(:id => params["workset"].to_i)
    end

    experiment = Experiment.create :name          => params["name"],
                                   :tags          => params["tags"],
                                   :account       => current_account,
                                   :forked_from   => forked_from,
                                   :recipe        => recipe,
                                   :workset       => workset,
                                   :simulator     => object,
                                   :configuration => input

    # TODO: check validations
    if experiment.errors.any?
      status 422
      render :haml, :"experiments/new", :locals => {
        :errors      => experiment.errors,
        :forked_from => nil,
        :simulators  => Object.names(:simulator),
        :simulator   => object,
        :recipe      => recipe,
        :workset     => workset,
        :experiment  => experiment
      }
    else
      redirect "/experiments/#{experiment.id}"
    end
  end

  # Form to create a new experiment from a recipe
  get '/experiments/from_recipe/:id' do
    recipe = Recipe.find_by(:id => params[:id])
    object = Object.find_by(:id => recipe.occam_object_id)

    if recipe.nil?
      status 404
    else
      render :haml, :"experiments/new", :locals => {
        :errors      => nil,
        :forked_from => nil,
        :experiment  => Experiment.new(:recipe_id => params[:id]),
        :recipe      => recipe,
        :workset     => nil,
        :simulator   => object
      }
    end
  end

  # Form to create a new experiment
  get '/experiments/new' do
    render :haml, :"experiments/new", :locals => {
      :errors      => nil,
      :forked_from => nil,
      :experiment  => Experiment.new,
      :recipe      => nil,
      :workset     => nil,
      :simulator   => nil,
      :simulators  => Occam::Object.names(:simulator)
    }
  end

  # Retrieve a list of tags for autocomplete
  get '/experiments/tags' do
    Experiment.all_tags_json(params["term"])
  end

  # Fork experiment
  get '/experiments/:id/fork' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      # Authorize (if private)
      if experiment.private? && experiment.account == current_account
      end

      object = experiment.object

      render :haml, :"experiments/new", :locals => {
        :errors      => nil,
        :forked_from => experiment,
        :account     => current_account,
        :simulator   => object,
        :experiment  => experiment,
        :recipe      => nil,
        :workset     => nil,
      }
    end
  end

  post '/experiments/:id/configuration' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    object = experiment.object

    schema = object.input_schema

    Configuration.decode_base64(params["data"].first)
    Configuration.type_check(params["data"].first, schema)

    experiment.configuration = params["data"].first
    experiment.save

    redirect "/experiments/#{experiment.id}"
  end

  get '/experiments/:id/configuration.json' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    content_type "application/json"
    JSON.pretty_generate(experiment.configuration)
  end

  get '/experiments/:id/configuration' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    render :haml, :"objects/configure", :locals => {
      :data       => experiment.configuration,
      :experiment => experiment,
    }
  end

  # Retrieve a specific experiment
  get '/experiments/:id' do
    experiment = Experiment.find_by(:id => params[:id].to_i)
    job = Job.find_by(:experiment_id => params[:id].to_i)
    if job
      job_info = job.monitoring_info
    end

    if experiment.recipe_id
      recipe = experiment.recipe
    end

    if experiment.workset_id
      workset = experiment.workset
    end

    if experiment.group
      group = experiment.group
    end

    if experiment.nil?
      status 404
    else
      render :haml, :"experiments/show", :locals => {
        :experiment    => experiment,
        :forked_from   => experiment.forked_from,
        :forked_us     => experiment.forked_us,
        :account       => experiment.account,
        :job           => job,
        :job_info      => job_info,
        :recipe        => recipe,
        :workset       => workset,
        :configuration => experiment.configuration,
        :workflow      => experiment.workflow,
        :group         => group
      }
    end
  end

  get '/experiments/:id/results.json' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      output = experiment.results

      content_type 'application/json'
      output.to_json
    end
  end

  # Launch graph builder for this experiment data
  get '/experiments/:id/graph/new' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      output = experiment.results

      if request.preferred_type.include? 'application/json'
        content_type 'application/json'
        output.to_json
      elsif request.preferred_type.include? 'text/html'
        object = experiment.workset.workflow.tail_connections.first.object

        # TODO: go to results browser, not graph builder
        render :haml, :"results/graph_builder", :locals => {
          :no_footer   => true,
          :experiment  => experiment,
          :experiments => [experiment],
          :workset     => experiment.workset,
          :object      => object,
          :schema      => object.output_schema,
          :data        => output["data"] || {},
          :errors      => output["errors"] || [],
          :warnings    => output["warnings"] || [],
        }
      else
        status 406
      end
    end
  end

  # Retrieve the results page for this experiment
  get '/experiments/:id/results' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      workset = experiment.workset
      if workset.can_view?(current_account)
        output = experiment.results

        if request.preferred_type.include? 'application/json'
          content_type 'application/json'
          output.to_json
        elsif request.preferred_type.include? 'text/html'
          object = workset.workflow.tail_connections.first.object

          render :haml, :"results/show", :locals => {
            :no_footer   => true,
            :experiments => [experiment],
            :object      => object,
            :schema      => object.output_schema,
            :data        => output["data"] || {},
            :errors      => output["errors"] || [],
            :warnings    => output["warnings"] || [],
          }
        else
          status 406
        end
      else
        status 406
      end
    end
  end

  # Run the experiment
  get '/experiments/:id/run' do
    experiment = Experiment.find_by(:id => params[:id].to_i)

    if experiment.nil?
      status 404
    else
      experiment.run

      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/experiments/#{experiment.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end

  # Cancel the experiment
  get '/experiments/:id/cancel' do
    job = Job.find_by(:experiment_id => params[:id].to_i)

    if job.nil?
      status 404
    else
      if request.preferred_type('text/html')
        # Redirect to the experiment page (if asking for html)
        redirect "/experiments/#{job.experiment_id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 404
      end
    end
  end

  get '/experiments/:id/output' do
    experiment  = Experiment.find_by(:id => params[:id].to_i)
    job         = experiment.job

    job_info = {}
    install_job_info = {}

    if job
      job_info = job.monitoring_info || {}
    end

    job_info["job_log"] ||= ""
    job_info["output_log"] ||= ""

    data = {
      :run_log    => job_info["job_log"],
      :output_log => job_info["output_log"]
    }

    content_type "application/json"
    return data.to_json
  end
end
