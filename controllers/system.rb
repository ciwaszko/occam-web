class Occam
  # Get the system configuration
  get '/system' do
    system = System.all.first
    render :haml, :"system/show", :locals => {
      :system => system
    }
  end

  # Update system configuration
  get '/system/new' do
    system = System.all.first
    system = System.new if system.nil?

    render :haml, :"system/new", :locals => {
      :system => system,
      :errors => system.errors
    }
  end

  # Persist system configuration
  post '/system' do
    system = System.all.first

    columns = params.select { |k,_| [
      "simulators_path", "traces_path", "benchmarks_path", "moderate_benchmarks",
      "moderate_simulators", "moderate_accounts", "jobs_path"].include? k }

    if system.nil?
      system = System.create columns
    else
      unless columns.empty?
        system.update_columns columns
        system.save
      end
    end

    if system.errors.any?
      status 422
      render :haml, :"system/new", :locals => {
        :system => system,
        :errors => system.errors
      }
    else
      redirect "/system"
    end
  end
end
