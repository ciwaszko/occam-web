class Occam < Sinatra::Base
  # Retrieve login form
  get '/login' do
    render :haml, :"sessions/login", :locals => {:errors => nil}
  end

  # Sign on
  post '/login' do
    account = Account.where("LOWER(username) like ?", params["username"].downcase).take
    if account && account.authenticated?(params["password"])
      session[:account_id] = account.id

      redirect "/accounts/#{account.id}"
    else
      status 422

      render :haml, :"sessions/login", :locals => {
        :errors => [["Username or password is invalid", nil]]
      }
    end
  end

  # Sign out
  get '/logout' do
    session[:account_id] = nil

    redirect '/'
  end
end
