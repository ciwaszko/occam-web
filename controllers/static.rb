class Occam
  get '/' do
    haml :"index"
  end

  get '/styleguide' do
    haml :"static/styleguide"
  end

  get '/about' do
    markdown :"static/about"
  end

  get '/acknowledgements' do
    markdown :"static/credits"
  end

  # 404 route
  not_found do
    markdown :"static/404"
  end

  # 500 route
  error do
    markdown :"static/500"
  end

  # Successfully get the 404 page
  get '/404' do
    markdown :"static/404"
  end

  # Successfully get the 500 page
  get '/500' do
    markdown :"static/500"
  end
end
