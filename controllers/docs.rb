# Documentation controller
#
# Displays static pages related to OCCAM system documentation.

class Occam
  def render_documentation(file, add_html=false)
    fname = "#{file.to_s}.md"

    context  = Redcarpet::Render::HTMLOutline.new(add_html)
    renderer = Redcarpet::Markdown.new(context, :fenced_code_blocks => true)

    content = ""

    File.open(File.join(File.dirname(__FILE__), "..", "views", "static", "documentation", fname)) do |f|
      content = renderer.render(f.read)
    end

    render :haml, :"static/documentation/layout", :layout => false, :locals => {
      :header  => context.title,
      :outline => context.outline.child,
    } do
      content
    end
  end

  def render_documentation_index(add_html=false)
    root = Occam::Node.new(:root)
    last = root

    Dir[File.join(File.dirname(__FILE__), "..", "views", "static", "documentation", '*.md')].each do |fname|
      next if File.basename(fname) == "index.md"

      context  = Redcarpet::Render::HTMLOutline.new(add_html)
      renderer = Redcarpet::Markdown.new(context, :fenced_code_blocks => true)

      content = ""
      File.open(fname) do |f|
        content = renderer.render(f.read)
      end

      new_node = Occam::Node.new(context.title, nil, nil, nil, "#{File.basename(fname, ".md")}#{add_html ? '.html' : ''}")
      last.sibling = new_node
      last = new_node
    end

    render :haml, :"static/documentation/layout", :layout => false, :locals => {
      :header  => :root,
      :outline => root.sibling,
    } do
      markdown :"static/documentation/index"
    end
  end

  # List all documents available in /static/documentation
  get '/docs' do
    render_documentation_index
  end

  # Documentation page
  get '/docs/:tag' do
    render_documentation params[:tag]
  end
end
