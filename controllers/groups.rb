class Occam
  # Form to generate a new group
  get '/worksets/:id/groups/new' do
    workset = Occam::Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    elsif workset.can_edit?(current_account)
      workflow = workset.workflow

      if workflow.nil?
        status 404
      else
        render :haml, :"groups/new", :locals => {
          :errors      => nil,
          :forked_from => nil,
          :experiment  => Experiment.new,
          :recipe      => nil,
          :workset     => workset,
          :workflow    => workflow
        }
      end
    else
      status 406
    end
  end

  # Launch graph builder for this collection of data
  get '/worksets/:id/groups/:group_id/graphs/new' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if group.workset_id == workset.id && workset.can_edit?(current_account)
        experiments = group.experiments.select{|e| !e.results.nil?}
        experiment = experiments.first

        object = workset.workflow.tail_connections.first.object
        output = experiment.results

        render :haml, :"results/graph_builder", :locals => {
          :no_footer   => true,
          :experiments => experiments,
          :workset     => workset,
          :object      => object,
          :schema      => object.output_schema,
          :data        => output["data"] || {},
          :errors      => output["errors"] || [],
          :warnings    => output["warnings"] || [],
        }
      else
        status 406
      end
    end
  end

  # View all results
  get '/worksets/:id/groups/:group_id/results' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_view?(current_account)
        experiments = group.experiments.select{|e| !e.results.nil?}
        experiment = experiments.first

        object = experiments.first.workset.workflow.tail_connections.first.object
        output = experiment.results

        render :haml, :"results/show", :locals => {
          :no_footer   => true,
          :experiments => experiments,
          :object      => object,
          :schema      => object.output_schema,
          :data        => output["data"] || {},
          :errors      => output["errors"] || [],
          :warnings    => output["warnings"] || [],
        }
      else
        status 406
      end
    end
  end

  get '/worksets/:id/groups/:group_id/cancel' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        experiments = group.experiments

        experiments.each do |experiment|
          experiment.cancel
        end

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          redirect "/worksets/#{workset.id}/groups/#{group.id}"
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Run all experiments in this group
  get '/worksets/:id/groups/:group_id/run' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset.nil? or group.nil?
      status 404
    else
      if workset.can_edit?(current_account)
        experiments = group.experiments

        experiments.each do |experiment|
          experiment.run
        end

        if request.preferred_type('text/html')
          # Redirect to the workset page (if asking for html)
          redirect "/worksets/#{workset.id}/groups/#{group.id}"
        elsif request.preferred_type('application/json')
          # If asking for json, return success or fail
          content_type 'application/json'
          {
            :result => "success"
          }.to_json
        else
          status 404
        end
      else
        status 406
      end
    end
  end

  # Show a group
  get '/worksets/:id/groups/:group_id' do
    group   = Occam::Group.find_by(:id => params[:group_id])
    workset = Occam::Workset.find_by(:id => params[:id])

    if workset && group
      if workset.can_view?(current_account)
        workflow    = group.workflow
        experiments = group.experiments

        render :haml, :"groups/show", :locals => {
          :workset       => workset,
          :group         => group,
          :forked_from   => group.forked_from,
          :forked_us     => group.forked_us,
          :account       => group.account,
          :collaborators => workset.accounts,
          :experiments   => experiments,
          :workflow      => workflow
        }
      else
        status 406
      end
    else
      status 404
    end
  end

  # Form to populate a group from an experiment

  # Create a new group
  post '/worksets/:id/groups' do
    workset = Workset.find_by(:id => params[:id])

    if workset.nil?
     status 404
    elsif workset.can_edit?(current_account)
      workflow = Occam::Workflow.new(:workset => workset)
      g = Group.create(:name          => params["name"],
                       :tags          => params["tags"],
                       :account       => current_account,
                       :forked_from   => nil,
                       :workset       => workset,
                       :workflow      => workflow,
                       :experiments   => [])

      if g.errors.any?
        render :haml, :"groups/new", :locals => {
          :errors      => g.errors,
          :forked_from => nil,
          :experiment  => Experiment.new,
          :recipe      => nil,
          :workset     => workset,
          :workflow    => workflow
        }
      else
        redirect "/worksets/#{workset.id}/groups/#{g.id}"
      end
    else
      status 406
    end
  end

  # Form to edit group to add configuration range
  get '/worksets/:id/groups/:group_id/add_range' do
    group   = Occam::Group.find_by(:id => params[:group_id].to_i)
    workset = Workset.find_by(:id => params[:id])

    if group.nil? or workset.nil?
     status 404
    elsif workset.can_edit?(current_account)
      workflow = workset.workflow

      render :haml, :"groups/new_range", :locals => {
        :errors      => nil,
        :forked_from => nil,
        :group       => group,
        :experiment  => Experiment.new,
        :recipe      => nil,
        :workset     => workset,
        :workflow    => workflow
      }
    else
      status 406
    end
  end

  # Edit group (add configuration, change name, etc)
  post '/worksets/:id/groups/:group_id' do
    workset  = Occam::Workset.find_by(:id => params[:id].to_i)
    group    = Occam::Group.find_by(:id => params[:group_id].to_i)
    workflow = group.workflow

    if params["data"]
      p params["data"]
      Configuration.decode_base64(params["data"])

      original_config = params["data"].dup

      p original_config

      # Generate permutations upon configuration ranges

      # Find all fields with a range specified
      find_ranges = Configuration.rake_ranges(params["data"])
      p find_ranges

      # Find all possible values for each range field
      exploded_ranges = Configuration.form_range(find_ranges)
      p exploded_ranges

      # Get every combination of values
      x, *xs = *exploded_ranges

      if x.nil? # No ranges, just add a single experiment
        Configuration.type_check(original_config)

        experiments = [
          Experiment.new(:name          => params["name"],
                         :tags          => params["tags"],
                         :account       => current_account,
                         :forked_from   => nil,
                         :recipe        => nil,
                         :workset       => workset,
                         :workflow      => workflow,
                         :configuration => original_config)]
      else
        configs = x.product(*xs)

        # Each experiment gets an identifier attached
        i = 1

        # For each configuration, merge with the one given, and then type check them
        # and create the experiment
        experiments = configs.map do |config|
          # 'config' here is an array of hashes, which only have one key: the
          # parameter that was ranged. It contains a possible value for it.

          # Each hash in the array is another ranged value:
          # [ { "num_chans" => 1 }, { "jedec_data_bus_bits" => 64 } ]

          # Now, merge this with the configuration given to us.
          name = "#{params["name"]}"
          config.each do |value|

            def deep_merge(first, second)
              merger = proc { |key, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : v2 }
              first.merge(second, &merger)
            end

            params["data"] = deep_merge(params["data"], value["hash"])
            key = value["name"].split('.').last
            name = "#{name}-#{key}-#{value["value"]}"
          end

          p params["data"]
          Configuration.type_check(params["data"])

          i += 1

          Experiment.new :name          => name,
                         :tags          => params["tags"],
                         :account       => current_account,
                         :forked_from   => nil,
                         :recipe        => nil,
                         :workset       => workset,
                         :workflow      => workflow,
                         :configuration => params["data"]
        end
      end

      group.experiments = experiments
      group.configuration = original_config
      group.save
    end

    redirect "/worksets/#{workset.id}/groups/#{group.id}"
  end
end
