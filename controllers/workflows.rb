class Occam
  # Add object to workflow
  post '/workflows/:id/connections' do
    workflow = Occam::Workflow.find_by(:id => params["id"])
    if workflow.nil?
      status 404
    else
      workset = workflow.workset
      if workset.nil?
        status 404
      else
        if workset.can_edit?(current_account)
          # Determine if connection is possible and if it is a co-runnable
          object = Occam::Object.find_by(:id => params["object"].to_i)
          if params.has_key? "connection_id"
            connection = Occam::Connection.find_by(:id => params["connection_id"].to_i)
            status 404 and return if connection.nil?
            status 404 and return if connection.workflow_id != workflow.id
          else
            connection = nil
          end

          creates_type = nil

          good = true
          co_runnable = false

          # TODO: handle input_object cases (do we allow them?)
          output_object = nil
          input_object  = nil
          if connection
            output_object = connection.object
          end

          if input_object
            # Check for direct input
            q = input_object.outputs.where(:object_type => object.object_type)
            if object.group
              q = q.where(:object_group => object.group)
            else
              q = q.where(:object_group => nil)
            end

            if !q.first.nil?
              # Connection is good
              output = q.first

              if output.fifo
                co_runnable = true
              end

              good = good && true
            end
          end

          if output_object
            found = true

            # Check for direct output
            q_base = output_object.inputs.where(:object_type => object.object_type)
            if object.group
              q = q_base.where(:object_group => object.group)
            else
              q = q_base.where(:object_group => nil)
            end

            found = q.any?

            if !found
              q = q_base.where(:object_group => "")
              found = q.any?
            end

            if found
              input = q.first
              if input.fifo
                co_runnable = true
              end
            end

            # TODO: Annotate the output_object(s?) that causes this connection!

            if !found
              # Check for indirect output (creation of an intermediate object)
              found = false
              output_object.inputs.each do |input|
                # TODO: look for fifo objects or not
                q = input.indirect_objects.where(:object_type => object.object_type)
                if q.any?
                  found = true
                  creates_type = input.object_type
                  if input.fifo
                    co_runnable = true
                  end
                  break
                end
              end
            end

            good = good && found
          end

          if good
            connection_id = nil
            if connection
              connection_id = connection.id
            end
            workflow.connections << Occam::Connection.new(:connection_id          => connection_id,
                                                          :co_runnable => (co_runnable ? 1 : 0),
                                                          :creates_object_of_type => creates_type,
                                                          :occam_object_id        => params["object"].to_i)
            workflow.save
          end
        end

        redirect request.referer
      end
    end
  end

  delete '/workflows/:id/connections/:connection_id' do
    workflow = Occam::Workflow.find_by(:id => params["id"].to_i)
    if workflow.nil?
      status 404
    else
      workset = workflow.workset
      connection = Occam::Connection.find_by(:workflow_id => workflow.id,
                                             :id          => params["connection_id"].to_i)
      # TODO: remove all orphaned connections
      if workset.nil? || connection.nil?
        status 404
      else
        if workset.can_edit?(current_account)
          connection.destroy
        end

        redirect request.referer
      end
    end
  end
end
