class Occam
  # Add collaborator to an existing workset
  post '/worksets/:id/collaborators' do
    workset = Workset.find_by(:id => params["id"])

    if workset
      account = Account.find_by(:username => params["username"])

      if account
        if workset.can_edit?(current_account)
          workset.accounts << account
          workset.save
        end

        redirect "/worksets/#{workset.id}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Remove a collaborator from this workset
  # Allowed by: creator and account owner
  get '/worksets/:id/collaborators/:account_id/remove' do
    workset = Workset.find_by(:id => params["id"])

    if workset
      account = Account.find_by(:id => params["account_id"])

      if account
        workset.accounts.delete(account)

        redirect "/worksets/#{workset.id}"
      else
        status 404
      end
    else
      status 404
    end
  end

  # Get a list of all worksets
  get '/worksets' do
    if params["tag"]
      worksets = Workset.with_tag params["tag"]
    else
      worksets = Workset.all
    end

    render :haml, :"worksets/index", :locals => {
      :worksets => worksets,
      :tags     => Workset.all_tags,
      :tag      => params["tag"]
    }
  end

  # Create a new workset
  post '/worksets' do
    status 406 and return if current_account.nil?

    if params["forked_from"]
      forked_from = Workset.find_by(:id => params["forked_from"].to_i)
    end

    workset = Workset.create :name          => params["name"],
                             :tags          => params["tags"],
                             :account       => current_account,
                             :forked_from   => forked_from,
                             :workflow      => Occam::Workflow.new

    if workset.errors.any?
      status 422
      render :haml, :"worksets/new", :locals => {
        :errors      => workset.errors,
        :forked_from => nil,
        :workset     => workset,
      }
    else
     redirect "/worksets/#{workset.id}"
    end
  end

  # Form to create a new workset
  get '/worksets/new' do
    render :haml, :"worksets/new", :locals => {
      :errors      => nil,
      :forked_from => nil,
      :workset     => Workset.new,
      :simulator   => nil,
      :simulators  => Object.names(:simulator)
    }
  end

  # Retrieve a list of tags for autocomplete
  get '/worksets/tags' do
    content_type 'application/json'
    Occam::Workset.all_tags_json(params["term"])
  end

  # Fork workset
  get '/worksets/:id/fork' do
    workset = Worksets.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    else
      object = workset.object

      render :haml, :"worksets/new", :locals => {
        :errors      => nil,
        :forked_from => workset,
        :account     => current_account,
        :simulator   => object,
        :workset     => workset,
        :recipe      => nil
      }
    end
  end

  # Edit an existing workset
  post '/worksets/:id' do
    workset = Workset.find_by(:id => params["id"])

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_account)
      fields = params.select { |k,_| ["description", "name", "tags", "private"].include? k }
      unless fields.empty?
        workset.update_columns fields
        workset.save
      end
      redirect "/worksets/#{workset.id}"
    else
      # Unauthorized
      status 406
    end
  end

  # Retrieve a specific workset
  get '/worksets/:id' do
    workset = Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_view?(current_account)
      experiments = Experiment.where(:workset_id => workset.id)
      groups = Group.where(:workset_id => workset.id)
      graphs = Graph.where(:workset_id => workset.id)

      workflow = workset.workflow

      render :haml, :"worksets/show", :locals => {
        :workset       => workset,
        :workflow      => workflow,
        :forked_from   => workset.forked_from,
        :forked_us     => workset.forked_us,
        :account       => workset.account,
        :collaborators => workset.accounts,
        :groups        => groups,
        :experiments   => experiments,
        :graphs        => graphs
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Update a specific workset
  get '/worksets/:id/edit' do
    workset = Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    # Authorize
    elsif workset.can_edit?(current_account)
      workflow = workset.workflow

      render :haml, :"worksets/new", :locals => {
        :errors        => nil,
        :workset       => workset,
        :workflow      => workflow,
        :forked_from   => workset.forked_from,
        :forked_us     => workset.forked_us,
        :account       => workset.account,
        :collaborators => workset.accounts
      }
    else
      # Unauthorized
      status 406
    end
  end

  # Add a new graph
  post '/worksets/:id/graphs' do
    workset = Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    elsif workset.can_edit? current_account
      Occam::Graph.create(:data       => JSON::parse(params["data"]),
                          :type       => params["type"],
                          :workset_id => params["id"].to_i)
    else
      status 406
    end
  end

  # Retrieve a graph
  get '/worksets/:id/graphs/:graph_id' do
    workset = Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    elsif workset.can_view? current_account
      document_id = BSON::ObjectId.from_string(params[:graph_id])
      graph = Occam::Graph.first(:id => document_id)

      if graph.nil?
        status 404
      else

      render :haml, :"results/graph", :locals => {
        :workset  => workset,
        :account  => workset.account,
        :graph    => graph,
        :data     => graph[:data] || {},
      }
      end
    else
      status 406
    end
  end

  # Retrieve the results page for this workset
  get '/worksets/:id/results' do
    workset = Workset.find_by(:id => params[:id].to_i)

    if workset.nil?
      status 404
    elsif !workset.can_view?(current_account)
      status 406
    else
      object = workset.workflow.tail

      output = workset.results || {}

      render :haml, :"results/show", :locals => {
        :workset     => workset,
        :experiments => workset.experiments,
        :schema      => object.output_schema,
        :data        => output["data"] || {},
        :errors      => output["errors"] || [],
        :warnings    => output["warnings"] || [],
      }
    end
  end

  # Run the workset
  get '/worksets/:id/run' do
    workset = Workset.find_by(:id => params[:id])

    if workset.nil?
      status 404
    elsif !workset.can_edit?(current_account)
      status 406
    else
      experiments = workset.experiments

      experiments.each do |experiment|
        experiment.run
      end

      if request.preferred_type('text/html')
        # Redirect to the workset page (if asking for html)
        redirect "/worksets/#{workset.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 406
      end
    end
  end

  # Cancel the workset
  get '/worksets/:id/cancel' do
    workset = Workset.find_by(:id => params[:id])

    if workset.nil?
      status 404
    elsif !workset.can_edit?(current_account)
      status 406
    else
      experiments = workset.experiments

      experiments.each do |experiment|
        job = Job.find_by(:experiment_id => experiment.id)

        unless job.nil?
          job.destroy
        end
      end

      if request.preferred_type('text/html')
        # Redirect to the workset page (if asking for html)
        redirect "/worksets/#{workset.id}"
      elsif request.preferred_type('application/json')
        # If asking for json, return success or fail
        content_type 'application/json'
        {
          :result => "success"
        }.to_json
      else
        status 406
      end
    end
  end

  # Form to generate a default experiment
  get '/worksets/:id/experiments/new' do
    workset = Workset.find_by(:id => params[:id])

    if workset.nil?
      status 404
    else
      object = Object.find_by(:id => workset.occam_object_id)

      if object.nil?
        status 404
      else
        render :haml, :"experiments/new", :locals => {
          :errors      => nil,
          :forked_from => nil,
          :experiment  => Experiment.new,
          :recipe      => nil,
          :workset     => workset,
          :simulator   => object
        }
      end
    end
  end

  # Form to create a new experiment from a recipe
  get '/worksets/:id/experiments/from_recipe/:recipe_id' do
    workset   = Workset.find_by(:id => params[:id])

    if workset.nil?
      status 404
    else
      recipe = Recipe.find_by(:id => params[:recipe_id])
      object = Object.find_by(:id => recipe.occam_object_id)

      if recipe.nil?
        status 404
      else
        render :haml, :"experiments/new", :locals => {
          :errors      => nil,
          :forked_from => nil,
          :experiment  => Experiment.new(:recipe_id => params[:recipe_id]),
          :recipe      => recipe,
          :workset     => workset,
          :simulator   => object
        }
      end
    end
  end
end
