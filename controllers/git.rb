class Occam
  def issue_git_service(path, service)
    system = Occam::System.first
    status 404 and return if not system.curate_git

    content_type "application/x-git-#{service}-result"

    stream do |out|
      input = request.body.read
      #path = File.join(repos_path, params[:repo])
      #status 404 and return unless path.start_with? repos_path
      command = "git #{service} --stateless-rpc #{path}"

      IO.popen(command, File::RDWR) do |pipe|
        pipe.write(input)
        while !pipe.eof?
          block = pipe.read(8192) # 8M at a time
          out << block # steam it to the client
        end
      end
    end
  end

  def publish_git_head(path)
    system = Occam::System.first
    status 404 and return if not system.curate_git

    path = File.join(path, "HEAD")

    no_cache
    content_type "text/plain"
    send_file path
  end

  def publish_git_info_refs(path)
    system = Occam::System.first
    status 404 and return if not system.curate_git

    no_cache

    if params["service"]
      if params["service"].match(/^git-/)
        service = params["service"].gsub(/^git-/, "")
        content_type "application/x-git-#{service}-advertisement"
        case service
        when "upload-pack"
          def packet_write(line)
            (line.size + 4).to_s(base=16).rjust(4, "0") + line
          end

          response.body.clear
          response.body << packet_write("# service=#{params["service"]}\n")
          response.body << "0000"
          response.body << `git upload-pack --stateless-rpc --advertise-refs #{path}`
          response.finish
        else
          status 404
        end
      else
        status 404
      end
    else
      content_type "text/plain"
      path = File.join(path, "info", "refs")
      send_file path
    end
  end

  def publish_git_objects_info_file(path, file)
    system = Occam::System.first
    status 404 and return if not system.curate_git

    if file == "packs"
      no_cache
      content_type "text/plain; charset=utf-8"
      path = File.join(path, "objects", "info", "packs")
      send_file path
    elsif file == "alternates" or file == "http-alternates"
      no_cache
      content_type "text/plain"
      path = File.join(path, "objects", "info", file)
      send_file path
    else
      status 404
    end
  end

  def publish_git_objects_pack(path, prefix, suffix)
    # Validate prefix/suffix
    if prefix.match(/^[0-9a-f]{40}$/).nil? or suffix.match(/^pack$|^idx$/).nil?
      status 404
    else
      # Send packed object
      if suffix == "idx"
        content_type "application/x-git-packed-objects-toc"
      else
        content_type "application/x-git-packed-objects"
      end

      forever_cache
      path = File.join(path, "objects", "pack", prefix)
      send_file path
    end
  end

  def publish_objects_file(path, prefix, suffix)
    # Validate prefix/suffix
    if prefix.match(/^[0-9a-f]{2}$/).nil? or suffix.match(/^[0-9a-f]{38}$/).nil?
      status 404
    else
      system = Occam::System.first
      if not system.curate_git
        status 404 and return
      end

      # Send loose object
      forever_cache
      content_type "application/x-git-loose-object"
      path = File.join(path, "objects", prefix, suffix)
      send_file path
    end
  end

  # OCCAM Object git access

  # Returns the HEAD object (get_text_file)
  get '/objects/:id/HEAD' do
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    publish_git_head(object.local_path)
  end

  # Returns the refs object (get_info_refs)
  get '/objects/:id/info/refs' do
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    publish_git_info_refs(object.local_path)
  end

  get '/objects/:id/objects/info/*' do |file|
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    publish_git_objects_info_file(object.local_path, file)
  end

  get '/objects/:id/objects/pack/pack-*.*' do |prefix, suffix|
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    publish_git_objects_pack(object.local_path, prefix, suffix)
  end

  get '/objects/:id/objects/*/*' do |prefix, suffix|
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?

    publish_git_objects_file(object.local_path, prefix, suffix)
  end

  post '/objects/:id/git-upload-pack' do
    object = Occam::Object.find_by(:id => params[:id].to_i)
    status 404 and return if object.nil?
    issue_git_service(object.local_path, 'upload-pack')
  end

  # Regular git access

  # Returns the HEAD object (get_text_file)
  get '/git/*/HEAD' do |repo|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    publish_git_head(path)
  end

  # Returns the refs object (get_info_refs)
  get '/git/*/info/refs' do |repo|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    publish_git_info_refs(path)
  end

  get '/git/*/objects/info/*' do |repo, file|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    publish_git_objects_info_file(path, file)
  end

  get '/git/*/objects/pack/pack-*.*' do |repo, prefix, suffix|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    publish_git_objects_pack(path, prefix, suffix)
  end

  get '/git/*/objects/*/*' do |repo, prefix, suffix|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    publish_git_objects_file(path, prefix, suffix)
  end

  post '/git/*/git-upload-pack' do |repo|
    system = Occam::System.first
    path = File.realpath(File.join(system.objects_path, repo))
    status 404 and return if !path.start_with?(system.objects_path)
    issue_git_service(path, 'upload-pack')
  end
end
