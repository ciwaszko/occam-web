class Occam
  # Get a list of all accounts
  get '/accounts' do
    accounts = Occam::Account.all
    render :haml, :"accounts/index", :locals => {
      :accounts => accounts
    }
  end

  # Create a new account
  post '/accounts' do
    account = Occam::Account.create :username => params["username"],
                                    :password => params["password"],
                                    :roles    => :experimentalist

    if account.errors.any?
      status 422
      render :haml, :"accounts/new", :locals => {:errors => account.errors}
    else
      # Sign in
      session[:account_id] = account.id

      redirect "/accounts/#{account.id}"
    end
  end

  # Form to create a new account
  get '/accounts/new' do
    render :haml, :"accounts/new", :locals => {:errors => nil}
  end

  # Update account/profile information or activate
  post '/accounts/:id' do
    account = Occam::Account.find_by(:id => params[:id].to_i)

    if account.nil?
      status 404
    else
      status 406 and return if current_account.nil?

      columns = {}
      if current_account.id == account.id
        # Normal accounts can change their username, etc
        columns = params.select { |k,v| [
            "username",
            "email",
            "bio",
            "display_name",
            "organization"
          ].include?(k) }
      elsif current_account.has_role? :administrator
        # Administrators can set "active"
        columns = params.select { |k,_| [
            "active",
          ].include?(k) }
      else
        status 406
      end

      unless columns.empty?
        account.update_columns columns
        account.save
      end

      if current_account == account
        redirect "/accounts/#{account.id}"
      elsif current_account.has_role? :administrator
        redirect "/admin"
      end
    end
  end

  # Retrieve a specific account page
  get '/accounts/:id' do
    account = Occam::Account.find_by(:id => params[:id].to_i)
    worksets = Occam::Workset.where(:account_id => params[:id].to_i)
    experiments = Occam::Experiment.where(:account_id => params[:id].to_i)
    jobs = Occam::Job.where(:experiment_id => experiments.map(&:id))

    if account.nil?
      status 404
    else
      collaborations = account.worksets

      render :haml, :"accounts/show", :locals => {
        :account        => account,
        :worksets       => worksets,
        :collaborations => collaborations,
        :experiments    => experiments,
        :jobs           => jobs,
      }
    end
  end

  # Form to edit an account's profile
  get '/accounts/:id/edit' do
    account = Occam::Account.find_by(:id => params[:id].to_i)

    if account.nil?
      status 404
    else
      if current_account.id != params["id"].to_i
        status 406
      else
        render :haml, :"accounts/edit", :locals => {
          :errors  => nil,
          :account => current_account
        }
      end
    end
  end
end
