$(document).ready(function(){
  var expander = $('<span>[+]</span>');
  expander.addClass('expand');

  var simulator_header = $('#advanced-simulator-search-header')
  simulator_header.append(expander)

  var experiment_header = $('#advanced-experiment-search-header')
  experiment_header.append(expander.clone())

  // Set up simulator expand
  $('#sims').on('change', function(e) {
    $('h2').text("goo");
  });

  // Set up div expands
  $('h2 span.expand').each(function() {
    var span = $(this);

    // Make the header cursor a pointer
    $(this).parent().css({
      cursor: 'pointer'
    });

    // Remove static option list
    $(this).parent().next().next().css({
      display: 'none'
    });

    $(this).parent().on('click', function(e) {
      span.toggleClass('shown');

      if ($(this).attr('id') === "advanced-simulator-search-header") {
        $(this).next().load("/search/simulators");
      }
      else if ($(this).attr('id') === "advanced-experiment-search-header") {
        $(this).next().load("/search/experiments", function(text, status) {
          $('#simulator_id').on('change', function(e) {
            var selected = $(this).children('option:selected');
            if (selected.attr('value') == "___any___") {
              $(this).parent().find('.loadable').html("");
            }
            else {
              $(this).parent().find('.loadable').css({"display": "block"}).load("/search/configurations/" + selected.attr('value'), function(text, status) {
                init_expands();
              });
            }
          });
        });
      }

      // Get associated description div
      if (span.hasClass('shown')) {
        $(this).next().css({
          display: 'block'
        });
        span.text("[ - ]");
      }
      else {
        $(this).next().css({
          display: 'none'
        });
        span.text("[+]");
      }
    });
  });
});
