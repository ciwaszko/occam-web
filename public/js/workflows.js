/* This code handles workflow editing.
 */

$(function() {
  /* Remove buttons for non-javascript version */
  $('#workflow .input select.inputs + input.button').css({
    display: 'none'
  });

  /* Add workflow boxes */
  var workflow = $('#workflow');

  var active_box = $('<div class="container embedded"><div class="container-content"></div></div>').css({
    "position": "absolute",
    "left": 0,
    "top": 0,
    "visibility": "hidden",
    "z-index": "1111"
  });
  workflow.append(active_box);

  /* Squish workflow */
  $('#workflow .container').css({
    "height": "100px",
    "width":  "100px",
  });

  $('#workflow .container .container-content').css({
    "height": "30px",
    "width":  "30px",
    "left":   "27px",
    "top":    "35px",
  });

  $('#workflow').css({
    "position": "relative"
  });

  /* Correct for edges in squished layout */
  $('#workflow .container .edge').css({
    "width": "28px",
  });

  $('#workflow .container .edge.left').css({
    "width": "30px",
  });

  $('#workflow .container .edge.top').css({
    "bottom": "48px",
  });

  $('#workflow .container .edge.right').css({
  });

  $('#workflow .container .edge.bottom').css({
    "top": "51px",
  });

  $('#workflow ul.connections > li.object:first-child > .row > .info > .container > .edge.top.right').css({
    "width": "33px",
  });

  $('#workflow .container .container-content').children()
    .css({
      "visibility": "hidden"
    });

  /* Ensure connections list is wide enough for the (+) button when
   * the list is empty. */
  $('#workflow ul.connections.input').css({
    "min-width": "90px"
  });

  var reveal_active_box = function(e) {
    /* Move active box to where this container is */
    var position = $(this).offset();
    var workflow_offset = workflow.offset();

    var x = position.left - workflow_offset.left;
    var y = position.top  - workflow_offset.top + workflow.scrollTop();

    var new_active_box = active_box.clone();

    new_active_box.empty();
    new_active_box.append($(this).clone());
    new_active_box.find('.label').remove();

    new_active_box.attr('data-left', x);
    new_active_box.attr('data-top', y);

    /* Embedded container fits its contents */
    new_active_box.children('.container-content').css({
      "left": "0px",
      "top": "0px"
    });

    new_active_box.children('.container-content').children().css({
      "visibility": "visible",
      "opacity":    0.0
    });

    new_active_box.css({
      "width": "30px",
      "height": "30px",
      "left": x-1,
      "top":  y-1,
      "visibility": "visible"
    });

    /* Reuse border color */
    new_active_box.children(".container-content").css({
      "border-color": $(this).css("border-left-color")
    });

    new_active_box.find('select').on('click keydown', function(e) {
      $(this).attr('data-clicked', !($(this).attr('data-clicked') == "true"));
    }).on('blur', function(e) {
      $(this).attr('data-clicked', false);
    });

    new_active_box.on('mouseenter', function(event) {
      if ($(this).find("select").attr('data-clicked') == "true") {
        event.stopPropagation();
        return;
      }

      var x = parseFloat(new_active_box.attr('data-left'));
      var y = parseFloat(new_active_box.attr('data-top'));

      new_active_box.stop(true).animate({
        "left": x-46,
        "top":  y-46,
      }, 200);

      new_active_box.children('.container-content').stop(true).animate({
        "width": "120px",
        "height": "120px",
      }, 200);

      new_active_box.children('.container-content').children().stop(true).animate({
        "opacity": 1.0
      }, 200);
    })
    .on('mouseleave', function(event) {
      if ($(this).find("select").attr('data-clicked') == "true") {
        event.stopPropagation();
        return;
      }

      $(this).children('select').attr('data-clicked', false);

      var x = parseFloat(new_active_box.attr('data-left'));
      var y = parseFloat(new_active_box.attr('data-top'));

      $(this).stop(true).animate({
        "left": x-1,
        "top":  y-1,
      }, 200, "swing", function() {
        new_active_box.remove();
      });

      $(this).children('.container-content').stop(true).animate({
        "width": "30px",
        "height": "30px",
      }, 200);

      $(this).children('.container-content').children().stop(true).animate({
        "opacity": 0.0
      }, 200, "swing", function() {
        new_active_box.remove();
      });
    });

    var object_type_selector = new_active_box.find('form select.inputs');

    /* Object type dropdown */
    object_type_selector.on('change', function() {
      var selected = $(this).children(':selected');

      /* Fill out the object selection dropdown with appropriate objects that
       * are of the specified type.
       */
      var object_id  = $(this).data('object-id');
      var thiz = $(this);
      var input_type = selected.attr('value');

      if (object_id === undefined) {
        // This is the global object. Grab ALL object types and inputs.
        var url = '/objects?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var object_selector = thiz.parent().children("select.objects");
          object_selector.children().remove();
          data.forEach(function(object) {
            var option = $('<option></option>');
            option.text(object["name"]);
            option.attr('value', object["id"]);
            option.data('object-id', object["id"]);
            object_selector.append(option);
          });
        });
      }
      else {
        var url = '/objects/' + object_id + '/inputs?by_type=' + input_type;

        $.getJSON(url, function(data) {
          var object_selector = thiz.parent().children("select.objects");
          object_selector.children().remove();
          data.forEach(function(input) {
            input["objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              object_selector.append(option);
            });

            input["indirect_objects"].forEach(function(object) {
              var option = $('<option></option>');
              option.text(object["name"]);
              option.attr('value', object["id"]);
              option.data('object-id', object["id"]);
              object_selector.append(option);
            });
          });
        });
      }
    });

    workflow.append(new_active_box);
  };

  $('#workflow .container:not(.embedded) .container-content').on('mouseenter',reveal_active_box);

  /* Remove input boxes */
  var input_containers = [];
  $('#workflow li.input').each(function() {
    /* Retain input container */
    $(this).find('.container').css({
      "height": "0px"
    });
    input_containers[input_containers.length] = {
      "container": $(this),
      "parent":    $(this).parent()
    };

    /* Remove input container from DOM */
    $(this).remove();
  });

  /* Create labels */
  $('#workflow .container:not(.embedded) .container-content').each(function(e, i) {
    var position = $(this).offset();
    var workflow_offset = workflow.offset();
    var x = position.left - workflow_offset.left;
    var y = position.top  - workflow_offset.top  - 5;
    x = 1;
    y = 30;

    var created = $(this).parent().hasClass("created");

    /* Add name label above the smaller node */
    var label_box = $('<div></div>');
    label_box.addClass("label name");
    label_box.text($(this).children('h3.name').text());
    $(this).parent().append(label_box);
    label_box.css({
      "width": "95px",
      "position": "absolute",
      "left": x,
      "text-align": "center",
      "font-size": "11px",
      "font-weight": "700",
      "color": "#555",
      "word-wrap": "normal",
      "white-space": "normal",
      "z-index": "888"
    });
    if (created) {
      label_box.css({
        "font-weight": "",
        "color": "#888"
      });
    }
    var label_y = y - 5 - label_box.height();
    label_box.css({
      "top": label_y,
    });

    /* Add type label above the smaller node */
    label_box = $('<div></div>');
    label_box.addClass("label type");
    label_box.text($(this).children('h3.type').text());
    $(this).parent().append(label_box);
    label_box.css({
      "width": "95px",
      "position": "absolute",
      "left": x,
      "text-align": "center",
      "font-size": "10px",
      "color": "#777",
      "word-wrap": "normal",
      "white-space": "normal",
      "z-index": "888"
    });
    label_y = label_y - 3 - label_box.height();
    label_box.css({
      "top": label_y,
    });
  });


  /* Add input button to re-add input container to DOM */
  input_containers.forEach(function(e, i) {
    var add_button = $('<div></div>');
    var last_container = e.parent.children("li:last-child").children('.row').children('.info').children('.container');
    var position = last_container.offset();
    var workflow_offset = workflow.offset();
    var appending = true;
    var x = 0;
    var y = 0;
    var bottom = 0;
    if (last_container.length == 0) {
      /* This is adding the first input for a node */
      /* In this case, we want a (+) bubble directly to the
       * left of the existing node. */
      last_container = e.parent;
      position = e.parent.offset();
      x = position.left - workflow_offset.left + 39;
      y = position.top  - workflow_offset.top;

      bottom = (e.parent.height() - 15) / 2;

      appending = false;
    }
    else {
      x = position.left - workflow_offset.left + 39;
      y = position.top  - workflow_offset.top  + 77;
      bottom = 8;
    }
    add_button.css({
      "width": "15px",
      "height": "15px",
      "color": "white",
      "overflow": "hidden",
      "position": "absolute",
      "vertical-align": "middle",
      "line-height": "15px",
      "font-size": "12px",
      "font-weight": "700",
      "background-color": "hsl(120, 50%, 80%)",
      "border-radius": "8px",
      "margin": "0",
      "text-align": "center",
      "right": 45,
      "bottom": bottom,
      "cursor": "pointer",
      "z-index": "888",
    }).on('mouseenter', function(e) {
      $(this).animate({
        "background-color": "hsl(120, 50%, 50%)"
      }, 200);
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "border-color": "hsl(120, 50%, 50%)"
      }, 200);
    }).on('mouseout', function(event) {
      $(this).stop(true).animate({
        "background-color": "hsl(120, 50%, 80%)"
      }, 200);
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "border-color": "hsl(180, 40%, 90%)"
      }, 200);
    }).one('click', function(event) {
      /* Add input container element to line DOM */
      $(this).unbind('mouseout mouseenter click');
      $(this).parent().children('.input-dashes[data-input-index=' + $(this).data("input-index") + ']').stop(true).animate({
        "opacity": "0.0"
      }, 200, "swing", function() {
        $(this).remove();
      });
      $(this).stop(true).animate({
        "opacity": "0.0"
      }, 200, "swing", function() {
        $(this).remove();
      });

      e.parent.append(e.container);

      e.container.find('.container').animate({
        "height": "100px"
      }, 200);

      e.container.find('.container-content').on('mouseenter', reveal_active_box);
    });
    var add_button_dashes = $('<div></div>').css({
      "width": "33px",
      "height": "32px",
      "color": "white",
      "position": "absolute",
      "font-weight": "700",
      "margin": "0",
      "right": 0,
      "z-index": "888",
    });
    if (!appending) {
      add_button_dashes.css({
        "border-top": "1px solid hsl(180, 40%, 90%)",
        "bottom": 16,
      });
    }
    else {
      add_button_dashes.css({
        "bottom": 14,
        "border-right": "5px solid hsl(180, 40%, 90%)",
        "border-bottom": "1px solid hsl(180, 40%, 90%)",
        "border-bottom-right-radius": "10px",
      });
    }
    add_button.addClass("input-button");
    add_button_dashes.addClass("input-dashes");
    add_button.attr("data-input-index", i);
    add_button_dashes.attr("data-input-index", i);
    add_button.text("+");
    last_container.append(add_button);
    last_container.append(add_button_dashes);
  });

  /* Allow resizing of the workflow area */
  $('#workflow').css({
    "resize": "vertical",
    "height": $('#workflow').height() + "px",
    "max-height": "none"
  });

});
