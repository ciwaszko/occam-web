/*jslint browser: true*/
/*global $, jQuery, d3, alert*/

/*
    Christopher Iwaszko
    OCCAM DRAMSIM2 Visual Configurator
    Capstone Fall 2014
*/

var ganged_channels = 1;                      // 1 if ganged channels, 0 if unganged channels
var dram_device_size = 0;                     // current size of 1 dram chip
var auto_change_memory_size = 0;              // 1 if memory size is automatically updated due to incompatible configuration
var auto_change_bus_size = 0;                 // 1 if data bus size is automatically updated due to incompatible configuration
var auto_change_number_of_channels = 0;       // 1 if number of channels is automatically updated due to incompatible configuration
var undoAction = 0;                           // 1 if the incompatible action needs to be undone
var timeoutId = 0;                            // timeout id for click and hold
var speed = 0;                                // click and hold speed
var timer = 0;                                // timer for click and hold
var colors = ['black', 'red', 'green', 'blue', 'purple', 'crimson', 'lime', 'teal', '#ff6600'];  // colors for unganged channels    
var currentTimingGraphMaxValue = 300;         // current max value for timing graph
var currentTimingGraphBarPercentage = 1;      // current timing graph bar height percentage
var currentTimingRestrictingBarGrowth = [];   // array containing timing graph parameters greater than the size needed to increase the bar height percentage
var currentPowerGraphMaxValue = 300;          // current max value for power graph
var currentPowerGraphBarPercentage = 1;       // current power graph bar height percentage
var currentPowerRestrictingBarGrowth = [];    // array containing power graph parameters greater than the size needed to increase the bar height percentage
var tabIndex = 1;                             // for setting graph form tab order

var ranges = {                                // 1 if there is a range option selected ('to' in the input field)
    data_bus_range: 0,
    transaction_queue: 0,
    command_queue: 0,
    row_accesses: 0,
    cycle_count: 0,
    epoch_length: 0,
    memory_size: 0
};

var formID = {                                // set all input form variables
        bus: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][amVkZWNfZGF0YV9idXNfYml0cw]']",
        memory_size: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][bWVtb3J5X3NpemU]']",
        device_width: "input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][ZGV2aWNlX3dpZHRo]']",
        banks: "input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX2Jhbmtz]']",
        rows: "input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX3Jvd3M]']",
        columns: "input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX2NvbHM]']",
        channels: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][bnVtX2NoYW5z]']",
        transaction_queue: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][dHJhbnNfcXVldWVfZGVwdGg]']",
        command_queue: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][Y21kX3F1ZXVlX2RlcHRo]']",
        row_accesses: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][dG90YWxfcm93X2FjY2Vzc2Vz]']",
        cycle_count: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][Y3ljbGVfY291bnQ]']",
        epoch_length: "input[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][ZXBvY2hfbGVuZ3Ro]']",
        tck: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dGNr]']",
        cl: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][Y2w]']",
        al: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][YWw]']",
        bl: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][Ymw]']",
        tras: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJhcw]']",
        trcd: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJjZA]']",
        trrd: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJyZA]']",
        trc: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJj]']",
        trp: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJw]']",
        tccd: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dGNjZA]']",
        trtp: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJ0cA]']",
        trfc: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJmYw]']",
        twtr: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHd0cg]']",
        twr: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHdy]']",
        trtrs: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHJ0cnM]']",
        tfaw: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dGZhdw]']",
        tcke: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dGNrZQ]']",
        txp: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dHhw]']",
        tcmd: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][dGNtZA]']",
        refresh_period: "input[name='data[MQ][RFJBTSBEZXZpY2UgVGltaW5nIENvbnN0cmFpbnQgUGFyYW1ldGVycw][cmVmcmVzaF9wZXJpb2Q]']",
        idd0: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkMA]']",
        idd1: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkMQ]']",
        idd2p: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkMnA]']",
        idd2q: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkMnE]']",
        idd2n: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkMm4]']",
        idd3pf:  "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkM3Bm]']",
        idd3ps: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkM3Bz]']",
        idd3n: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkM24]']",
        idd4w: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNHc]']",
        idd4r: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNHI]']",
        idd5: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNQ]']",
        idd6: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNg]']",
        idd6l: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNkw]']",
        idd7: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][aWRkNw]']",
        vdd: "input[name='data[MQ][RFJBTSBEZXZpY2UgUG93ZXIgQ29uc3VtcHRpb24gUGFyYW1ldGVycw][dmRk]']"
    };

/* set all select form  variables */
var formSelectID = {
    trace: "select[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][dHJhY2U]']",
    row_buffer: "select[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][cm93X2J1ZmZlcl9wb2xpY3k]']",
    queuing_structure: "select[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][cXVldWluZ19zdHJ1Y3R1cmU]']",
    scheduling_policy: "select[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][c2NoZWR1bGluZ19wb2xpY3k]']",
    address_mapping: "select[name='data[MQ][U3lzdGVtIENvbmZpZ3VyYXRpb24gUGFyYW1ldGVycw][YWRkcmVzc19tYXBwaW5nX3NjaGVtZQ]']"
};

/* set the given graph parameter form input */
function setGraphParameterFormInput(GRAPH_BUTTON_FORM_ID, downButton, formID, topOffset) {
    "use strict";
    $(formID).css('-webkit-appearance', 'none');
    $(formID).css('-moz-appearance', 'textfield');
    $(formID).css('width', '20px');
    $(formID).css('position', 'relative');
    $(formID).css('text_align', 'center');
    $(formID).css('font-size', '10px');
    var formInput = $(formID),
        currentOffset = downButton.offset(),
        currentValue = parseInt($(GRAPH_BUTTON_FORM_ID).val(), 10);
    formInput.attr("value", currentValue);
    formInput.offset({ top: topOffset, left: currentOffset.left - 8});           // Set position of form input
}

/* returns the upper range value of the current parameter */
function splitRange(currentElementFormID) {
    "use strict";
    var currentValue = $(currentElementFormID).val(),
        highRange = parseInt(currentValue.replace(/[\s\S]* to/g, ""), 10);       // replace everything up to and including to and convert it to an integer
    return highRange;
}

/* calculate total number of dimms needed to display accurate memory */
function calculateTotalDimms() {
    "use strict";
    var numberOfBanks = parseInt($(formID.banks).val(), 10),                                                                        // get number of banks from the form
        numberOfRows = parseInt($("input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX3Jvd3M]']").val(), 10),       // get number of rows from the form
        numberOfColumns = parseInt($("input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX2NvbHM]']").val(), 10),    // get number of columns from the form
        deviceWidth = parseInt($(formID.device_width).val(), 10),                                                                   // get device width from the form
        lowRangetotalStorage = parseInt($(formID.memory_size).val(), 10),
        highRangetotalStorage = splitRange(formID.memory_size),
        totalStorage = 0,
        memory_per_dimm = 0;
    if (highRangetotalStorage === 0) {
        highRangetotalStorage = lowRangetotalStorage;
    }
    totalStorage = highRangetotalStorage;                                                                        // get total storage
    dram_device_size = (numberOfRows * numberOfColumns * numberOfBanks * deviceWidth) / (8 * 1024 * 1024);       // set dram device size
    memory_per_dimm = dram_device_size * 16;

    return (totalStorage / memory_per_dimm);                                                                     // calculate the total number of dimms needed to display total memory
}

/* set the all graph parameter form inputs */
function setAllGraphParameterFormInputs(topOffset, type) {
    "use strict";
    if (type === 0) {
        $('.timing_parameters').each(function () {                                   // set the form input for the current graph parameter
            var textID = $(this).attr('id'),
                splitTextID = textID.split("_"),
                parameterKey = "",
                i = 0;
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            setGraphParameterFormInput(formID[parameterKey], $("#" + parameterKey + "_down"), "#" + parameterKey + "_input", topOffset);
        });
    } else {
        $('.power_parameters').each(function () {                                   // set the form input for the current graph parameter
            var textID = $(this).attr('id'),
                splitTextID = textID.split("_"),
                parameterKey = "",
                i = 0;
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            setGraphParameterFormInput(formID[parameterKey], $("#" + parameterKey + "_down"), "#" + parameterKey + "_input", topOffset);
        });
    }
}

/* set the given pop-up to visible */
function createGraphParameterFormInput(GRAPH_BUTTON_FORM_ID, formID, currentParameter, type, currentTabIndex) {
    "use strict";
    var newFormInput = document.createElement("input"),
        currentValue = parseInt($(GRAPH_BUTTON_FORM_ID).val(), 10),
        currentDiv  = document.createElement("div");
    newFormInput.setAttribute('id', formID);
    newFormInput.setAttribute('type', 'number');
    newFormInput.setAttribute('value', currentValue);
    newFormInput.setAttribute('tabindex', currentTabIndex);
    newFormInput.setAttribute('onchange', "updateGraphParameterOnChange(\"" + currentParameter + "\", " + type + ")");

    // add the newly created element and its content into the DOM 
    currentDiv = document.getElementsByClassName("pop-ups-and-inputs")[0];
    currentDiv.appendChild(newFormInput);
}

/* update the given graph parameter form input */
function updateGraphParameterFormInput(GRAPH_BUTTON_FORM_ID, formID) {
    "use strict";
    var formInput = $(formID),
        currentValue = parseInt($(GRAPH_BUTTON_FORM_ID).val(), 10);
    formInput.attr("value", currentValue);
}

/* update pop-up tip for current element on interaction */
function updatePopUpTips(popUpTable, currentInput, type) {
    "use strict";
    var currentValue = 0,
        firstRow = $(popUpTable + " td:first");
    if (type === 0) {
        currentValue = $(currentInput).val();
    } else {
        currentValue = $(currentInput + " option:selected").text();
    }
    firstRow.text(firstRow.text().replace(/\= [\s\S]*/g, " = " + currentValue));     // set the value
    firstRow.css({
        "text-align": "center",
        "color": "white"
    });
}

/* set the given pop-up to visible */
function showPopup(hoveritem, hoverpopup, shape) {
    "use strict";
    var hp = $(hoverpopup),
        offset = hoveritem.offset();
    hp.offset({ top: offset.top - shape, left: offset.left});                         // Set position of hover-over popup 
    hp.css({
        "visibility": "visible"
    });
}

/* set the given pop-up to hidden */
function hidePopup(hoverpopup) {
    "use strict";
    var hp = $(hoverpopup);
    hp.css({
        "visibility": "hidden"
    });
}

/* update the given graph parameter */
function updateGraphParameter(currentID, type, currentBar, inputID) {
    "use strict";
    var currentValue = parseInt($(inputID).val(), 10),
        oldLength = 0,
        index = 0,
        labelValue = 0,
        i = 0;
    $(currentID).val(currentValue);

    /* adjust graph scale if reach limit */
    if (type === 0) {                                                   // timing graph
        oldLength = currentTimingRestrictingBarGrowth.length;
        if (currentTimingRestrictingBarGrowth.indexOf(inputID) > -1 && currentValue <= (currentTimingGraphMaxValue / 2)) {
            index = currentTimingRestrictingBarGrowth.indexOf(inputID);
            if (index > -1) {
                currentTimingRestrictingBarGrowth.splice(index, 1);
            }
            if (currentTimingRestrictingBarGrowth.length === 0) {
                currentTimingGraphMaxValue /= 2;
                labelValue = 25;
                for (i = 0; i < 12; i += 1) {
                    $("#t_label" + i).text(labelValue);
                    $("#t_label" + i).css('font-size', "9.19728851px");
                    labelValue += 25;
                }
                $("#t_label1").attr('x', 69.300919);
                $("#t_label2").attr('x', 69.300919);
            }
            //alert ("delete "  + inputID);
        } else if (currentTimingRestrictingBarGrowth.indexOf(inputID) === -1 && currentValue >= currentTimingGraphMaxValue + 1) {
            //alert ("push "  + inputID);
            currentTimingRestrictingBarGrowth.push(inputID);
            if (currentTimingRestrictingBarGrowth.length === 1) {
                currentTimingGraphMaxValue *= 2;
                labelValue = 50;
                for (i = 0; i < 12; i += 1) {
                    $("#t_label" + i).text(labelValue);
                    $("#t_label" + i).css('font-size', "9.19728851px");
                    labelValue += 50;
                }
                $("#t_label1").attr('x', 64.300919);
                $("#t_label2").attr('x', 64.300919);
            }
        }
        if (currentTimingRestrictingBarGrowth.length === 0 && oldLength > 0) {
            $('.timing_parameters').each(function () {
                var textID = $(this).attr('id'),
                    splitTextID = textID.split("_"),
                    parameterKey = "",
                    newValue;
                for (i = 0; i < splitTextID.length - 1; i += 1) {
                    if (i > 0) {
                        parameterKey += "_";
                    }
                    parameterKey += splitTextID[i];
                }
                newValue = parseInt($("#" + parameterKey + "_input").val(), 10);
                $("#" + parameterKey + "_bar").attr("height", newValue / (currentTimingGraphMaxValue / 300));
            });
        } else if (oldLength === currentTimingRestrictingBarGrowth.length - 1) {
            $('.timing_parameters').each(function () {
                var textID = $(this).attr('id'),
                    splitTextID = textID.split("_"),
                    parameterKey = "",
                    newValue;
                for (i = 0; i < splitTextID.length - 1; i += 1) {
                    if (i > 0) {
                        parameterKey += "_";
                    }
                    parameterKey += splitTextID[i];
                }
                newValue = parseInt($("#" + parameterKey + "_input").val(), 10);
                $("#" + parameterKey + "_bar").attr("height", newValue / (currentTimingGraphMaxValue / 300));
            });
        } else {
            $(currentBar).attr("height", currentValue / (currentTimingGraphMaxValue / 300));
        }
    } else {                                                                // power graph
        oldLength = currentPowerRestrictingBarGrowth.length;
        if (currentPowerRestrictingBarGrowth.indexOf(inputID) > -1 && currentValue <= (currentPowerGraphMaxValue / 2)) {
            index = currentPowerRestrictingBarGrowth.indexOf(inputID);
            if (index > -1) {
                currentPowerRestrictingBarGrowth.splice(index, 1);
            }
            if (currentPowerRestrictingBarGrowth.length === 0) {
                currentPowerGraphMaxValue /= 2;
                labelValue = 25;
                for (i = 0; i < 12; i += 1) {
                    $("#p_label" + i).text(labelValue);
                    $("#p_label" + i).css('font-size', "9.19728851px");
                    labelValue += 25;
                }
            }
            //alert ("delete "  + inputID);
        } else if (currentPowerRestrictingBarGrowth.indexOf(inputID) === -1 && currentValue >= currentPowerGraphMaxValue + 1) {
            //alert ("push "  + inputID);
            currentPowerRestrictingBarGrowth.push(inputID);
            if (currentPowerRestrictingBarGrowth.length === 1) {
                currentPowerGraphMaxValue *= 2;
                labelValue = 50;
                for (i = 0; i < 12; i += 1) {
                    $("#p_label" + i).text(labelValue);
                    $("#p_label" + i).css('font-size', "9.19728851px");
                    labelValue += 50;
                }
                $("#p_label1").attr('x', 64.300919);
                $("#p_label2").attr('x', 64.300919);
            }
        }
        if (currentPowerRestrictingBarGrowth.length === 0 && oldLength > 0) {
            $('.power_parameters').each(function () {
                var textID = $(this).attr('id'),
                    splitTextID = textID.split("_"),
                    parameterKey = "",
                    newValue;
                for (i = 0; i < splitTextID.length - 1; i += 1) {
                    if (i > 0) {
                        parameterKey += "_";
                    }
                    parameterKey += splitTextID[i];
                }
                newValue = parseInt($("#" + parameterKey + "_input").val(), 10);
                $("#" + parameterKey + "_bar").attr("height", newValue / (currentPowerGraphMaxValue / 300));
            });
        } else if (oldLength === currentPowerRestrictingBarGrowth.length - 1) {
            $('.power_parameters').each(function () {
                var textID = $(this).attr('id'),
                    splitTextID = textID.split("_"),
                    parameterKey = "",
                    newValue;
                for (i = 0; i < splitTextID.length - 1; i += 1) {
                    if (i > 0) {
                        parameterKey += "_";
                    }
                    parameterKey += splitTextID[i];
                }
                newValue = parseInt($("#" + parameterKey + "_input").val(), 10);
                $("#" + parameterKey + "_bar").attr("height", newValue / (currentPowerGraphMaxValue / 300));
            });
        } else {
            $(currentBar).attr("height", currentValue / (currentPowerGraphMaxValue / 300));
        }
    }
}

/* increment or decrement timing or power graph bars */
function modifyGraphValue(parameterKey, type, currentID, currentBar, currentPopUp, topOffset, action) {
    "use strict";
    var currentValue = 0;

    if (action === 0) {
        currentValue = parseInt($(currentID).val(), 10) + 1;
    } else {
        if ($(currentID).val() > 0) {
            currentValue = parseInt($(currentID).val(), 10) - 1;
        }
    }

    $("#" + parameterKey + "_input").val(currentValue);
    //$(currentBar).attr("height", currentValue);
    updateGraphParameter(currentID, type, currentBar, "#" + parameterKey + "_input");

    updatePopUpTips(currentPopUp, currentID, 0);
    updateGraphParameterFormInput(formID[parameterKey], "#" + parameterKey + "_input");
    if (speed > 1) {
        speed -= 3;
    }
    if (currentValue > 0 || action === 0) {
        timeoutId = setTimeout(function () {modifyGraphValue(parameterKey, type, currentID, currentBar, currentPopUp, topOffset, action); }, speed * 0.5);
    }
}

function updateGraphParameterOnChange(currentParameter, type) {
    "use strict";
    updateGraphParameter(formID[currentParameter], type,  "#" + currentParameter + "_bar",  "#" + currentParameter + "_input");
    updatePopUpTips("#" + currentParameter + "_pop_up", "#" + currentParameter + "_input", 0);
}

/* display or hide memory controllers */
function drawMemoryControllers() {
    "use strict";
    var currentNumberOfChannels = parseInt($(formID.channels).val(), 10),
        highRange = splitRange(formID.channels),
        i = 1,
        j = 0,
        k = 1;
    if (ganged_channels === 0) {
        if (highRange === 'undefined') {
            highRange = currentNumberOfChannels;
        }
        if (highRange === 2) {
            $("#memory_controller_text").attr("y", 647);
        } else if (highRange === 4) {
            $("#memory_controller_text").attr("y", 636);
        } else {
            $("#memory_controller_text").attr("y", 606);
        }

        while (i < highRange) {
            $("#memory_controller_" + i).css({
                "visibility": "visible"
            });
            i += 1;
        }
        for (j = i; j < 8; j += 1) {
            $("#memory_controller_" + j).css({
                "visibility": "hidden"
            });
        }
        $("#memory_controller_text").text("MEMORY CONTROLLERS");
    } else {
        $("#memory_controller_text").attr("y", 647);
        for (k = 1; k < 8; k += 1) {
            $("#memory_controller_" + k).css({
                "visibility": "hidden"
            });
        }
        $("#memory_controller_text").text("MEMORY CONTROLLER");
    }
}

/* set CSS of button */
function setButtonCSS(buttonID, arrowID, type, action) {
    "use strict";
    if (type === 1) {                               // hover arrow
        $(arrowID).css('cursor', 'pointer');        // set cursor to point
        $(buttonID).css('cursor', 'pointer');       // set cursor to point
    }
    if (action === 0) {                             // mouse down
        $(buttonID).css('fill', '#b3b3b3');
        $(buttonID).css('stroke', '#333333');
    } else if (action === 1) {                      // mouse up, hover
        $(buttonID).css('fill', '#E9E9E9');
        $(buttonID).css('stroke', '#4D4D4D');
    } else if (action === 2) {                      // hover leave
        $(buttonID).css('fill', '#cccccc');
        $(buttonID).css('stroke', '#000000');
    }
}

/* hover functions for buttons */
function setButtonHoverFunctions(buttonID, arrowID) {
    "use strict";
    $(arrowID).hover(function () {
        setButtonCSS(buttonID, arrowID, 1, 1);          // hover over arrow
    },
        function () {
            setButtonCSS(buttonID, arrowID, 0, 2);      // hover leave arrow
        });
    $(buttonID).hover(function () {
        setButtonCSS(buttonID, arrowID, 1, 1);          // hover over box
    },
        function () {
            setButtonCSS(buttonID, arrowID, 0, 2);      // hover leave box
        });
}

/* set css and actions for the given graph buttons */
function setGraphButton(currentParameter, type, GRAPH_BUTTON_FORM_ID, currentInput, barID, popUpID, textID, upButtonID, upArrowID, downButtonID, downArrowID, topOffset) {
    "use strict";

    $(function () {                                     // set initial bar and input label
        var currentValue = parseInt($(GRAPH_BUTTON_FORM_ID).val(), 10);
        $(barID).attr("height", currentValue);
    });
    $(barID).hover(function () {                        // hover functions for tCK pop-up tip
        timer = setTimeout(function () {showPopup($(barID), popUpID, 32); }, 1000);
    },
        function () {
            clearTimeout(timer);
            hidePopup(popUpID);
        });

    $(textID).hover(function () {
        timer = setTimeout(function () {showPopup($(textID), popUpID, 32); }, 1000);
    },
        function () {
            clearTimeout(timer);
            hidePopup(popUpID);
        });

    $(upArrowID).mousedown(function () {               // mouse down function tCK up arrow
        setButtonCSS(upButtonID, upArrowID, 0, 0);     // mouse down on arrow
        modifyGraphValue(currentParameter, type, GRAPH_BUTTON_FORM_ID, barID, popUpID, topOffset, 0);
    }).bind('mouseup mouseleave', function () {        // mouse up function for up arrow
        clearTimeout(timeoutId);
        speed = 30;
        setButtonCSS(upButtonID, upArrowID, 0, 1);     // mouse up on arrow
    });

    $(upButtonID).mousedown(function () {              // mouse down function for up box
        setButtonCSS(upButtonID, upArrowID, 0, 0);     // mouse down on box
        modifyGraphValue(currentParameter, type, GRAPH_BUTTON_FORM_ID, barID, popUpID, topOffset, 0);
    }).bind('mouseup mouseleave', function () {        // mouse up function for up box
        clearTimeout(timeoutId);
        speed = 30;
        setButtonCSS(upButtonID, upArrowID, 0, 1);     // mouse up on box
    });

    setButtonHoverFunctions(upButtonID, upArrowID);    // hover functions for up button

    $(downArrowID).mousedown(function () {              // mouse down function for down arrow
        setButtonCSS(downButtonID, downArrowID, 0, 0);  // mouse down on arrow
        if ($(GRAPH_BUTTON_FORM_ID).val() !== 0) {
            modifyGraphValue(currentParameter, type, GRAPH_BUTTON_FORM_ID, barID, popUpID, topOffset, 1);
        }
    }).bind('mouseup mouseleave', function () {         // mouse up function for down arrow
        clearTimeout(timeoutId);
        speed = 30;
        setButtonCSS(downButtonID, downArrowID, 0, 1);  // mouse up on arrow
    });
    $(downButtonID).mousedown(function () {              // mouse down function for down box
        setButtonCSS(downButtonID, downArrowID, 0, 0);   // mouse down on box
        if ($(GRAPH_BUTTON_FORM_ID).val() !== 0) {
            modifyGraphValue(currentParameter, type, GRAPH_BUTTON_FORM_ID, barID, popUpID, topOffset, 1);
        }
    }).bind('mouseup mouseleave', function () {          // mouse up function for down box
        clearTimeout(timeoutId);
        speed = 30;
        setButtonCSS(downButtonID, downArrowID, 0, 1);   // mouse up on box
    });

    setButtonHoverFunctions(downButtonID, downArrowID);  // hover functions for down button
    updateGraphParameter(formID[currentParameter], type, barID, currentInput);
}

/* set ganged or unganged channels */
function setGangedChannels() {
    "use strict";
    var currentChannelSize = parseInt($(formID.channels).val(), 10);
    if (currentChannelSize === 1) {
        ganged_channels = 1;
    } else {
        ganged_channels = 0;
    }
}

/* determine accurate memory based on input, returns total number of ranks */
function getAccurateMemory() {
    "use strict";
    var lowRangetotalStorage = parseInt($(formID.memory_size).val(), 10),                                                          // grabs the first number from the form
        highRangetotalStorage = splitRange(formID.memory_size),                                                                    // grabs the second number from the form
        numberOfBanks = parseInt($(formID.banks).val(), 10),                                                                       // grabs number of banks from the form
        numberOfRows = parseInt($("input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX3Jvd3M]']").val(), 10),      // grabs number of rows from the form
        numberOfColumns = parseInt($("input[name='data[MQ][RFJBTSBEZXZpY2UgTWVtb3J5IFBhcmFtZXRlcnM][bnVtX2NvbHM]']").val(), 10),   // grabs number of columns from the form
        storagePerRank = (numberOfBanks * numberOfRows * numberOfColumns * 64) / (8 * 1024 * 1024),                                // set total storage per rank
        currentDataBusSize = splitRange(formID.bus),                                                                               // grab data bus bitsize from the form
        currentNumberOfChannels = splitRange(formID.channels),                                                                     // grabs the second number from the form
        channelStorage = 0,
        totalStorage = 0;

    if (highRangetotalStorage === 0) {                                // use upper range unless 0
        highRangetotalStorage = lowRangetotalStorage;
    }
    totalStorage = highRangetotalStorage;                             // set total storage
    if (ganged_channels === 1) {                                      // if ganged channels
        if (currentDataBusSize < 64) {                                // check for a form input error
            currentDataBusSize = 64;
        }
        currentNumberOfChannels = currentDataBusSize / 64;            // calculate number of channels
        channelStorage = totalStorage / currentNumberOfChannels;      // set the total channel storage
    } else {                                                          // if unganged channels
        if (currentNumberOfChannels < 1) {
            currentNumberOfChannels = 1;
        }
        channelStorage = totalStorage / currentNumberOfChannels;      // set total storage per channel
    }
    return channelStorage / storagePerRank;                           // total number of ranks
}

/* dimms have one dram device */
function oneDRAM() {
    "use strict";
    var currentDRAMposition = 402.02402;
    $(".dram0").css({
        "visibility": "hidden"
    });
    $(".dram1").css({
        "visibility": "hidden"
    });
    $(".dram2").css({
        "visibility": "hidden"
    });
    $(".dram3").css({
        "visibility": "hidden"
    });
    $(".dram4").css({
        "visibility": "visible"
    });
    $(".dram4").attr("x", currentDRAMposition);
    $(".dram5").css({
        "visibility": "hidden"
    });
    $(".dram6").css({
        "visibility": "hidden"
    });
    $(".dram7").css({
        "visibility": "hidden"
    });
}

/* dimms have two dram devices */
function twoDRAM() {
    "use strict";
    var currentDRAMposition = 289.01678,
        currentDRAMposition2 = 514.02402;
    $(".dram0").css({
        "visibility": "hidden"
    });
    $(".dram1").css({
        "visibility": "hidden"
    });
    $(".dram2").css({
        "visibility": "hidden"
    });
    $(".dram3").css({
        "visibility": "visible"
    });
    $(".dram3").attr("x", currentDRAMposition);
    $(".dram4").css({
        "visibility": "visible"
    });
    $(".dram4").attr("x", currentDRAMposition2);

    $(".dram5").css({
        "visibility": "hidden"
    });
    $(".dram6").css({
        "visibility": "hidden"
    });
    $(".dram7").css({
        "visibility": "hidden"
    });
}

/* dimms have four dram devices */
function fourDRAM() {
    "use strict";
    var currentDRAMposition = 261.77286,
        currentDRAMposition1 = 335.01678,
        currentDRAMposition2 = 468.02402,
        currentDRAMposition3 = 541.26804;
    $(".dram0").css({
        "visibility": "hidden"
    });
    $(".dram1").css({
        "visibility": "hidden"
    });
    $(".dram2").css({
        "visibility": "visible"
    });
    $(".dram2").attr("x", currentDRAMposition);
    $(".dram3").css({
        "visibility": "visible"
    });
    $(".dram3").attr("x", currentDRAMposition1);
    $(".dram4").css({
        "visibility": "visible"
    });
    $(".dram4").attr("x", currentDRAMposition2);
    $(".dram5").css({
        "visibility": "visible"
    });
    $(".dram5").attr("x", currentDRAMposition3);
    $(".dram6").css({
        "visibility": "hidden"
    });
    $(".dram7").css({
        "visibility": "hidden"
    });
}

/* dimms have eight dram devices */
function eightDRAM() {
    "use strict";
    $(".dram0").attr("x", "214.5874");
    $(".dram0").css({
        "visibility": "visible"
    });
    $(".dram1").attr("x", "257.8313");
    $(".dram1").css({
        "visibility": "visible"
    });
    $(".dram2").attr("x", "301.77286");
    $(".dram2").css({
        "visibility": "visible"
    });
    $(".dram3").attr("x", "345.01678");
    $(".dram3").css({
        "visibility": "visible"
    });
    $(".dram4").attr("x", "458.02402");

    $(".dram4").css({
        "visibility": "visible"
    });
    $(".dram5").attr("x", "501.26804");
    $(".dram5").css({
        "visibility": "visible"
    });
    $(".dram6").attr("x", "545.20953");
    $(".dram6").css({
        "visibility": "visible"
    });
    $(".dram7").attr("x", "588.45355");
    $(".dram7").css({
        "visibility": "visible"
    });
}

/* set the number of dram */
function drawDramDevices() {
    "use strict";
    $("#dram_circle").attr('transform', "matrix(0.89468412,0,0,1,51.362635,361.26384)");
    $("#dram_arrow_1").attr('d', "m 515.685,486.69607 29.89819,-67.95044");
    $("#dram_circle_2").attr('transform', "matrix(0.89468412,0,0,1,51.502087,838.24559)");
    $("#dram_arrow_2").attr('d', "M 519.75669,25.315813 532.10512,-2.7488035");
    $("#dram_arrow").attr('d', "M 495.507,1146.9536 579.07017,923.53431");

    var total_dimms = calculateTotalDimms();

    if (total_dimms === 1) {
        $("#dram_circle").css('visibility', 'visible');
        $("#dram_arrow_1").css('visibility', 'visible');
    } else {
        $("#dram_circle").css('visibility', 'hidden');
        $("#dram_arrow_1").css('visibility', 'hidden');
    }
}

/* gets data bus size or number of channels depending on whether channels are ganged or unganged, fixes it if not compatible with device width */
function getDataBusSizeOrChannels(update) {
    "use strict";
    var lowRangeDataBusSize = parseInt($(formID.bus).val(), 10),
        lowRangeNumberOfChannels = parseInt($(formID.channels).val(), 10),
        deviceWidth = parseInt($(formID.device_width).val(), 10),
        highRangeDataBusSize = 0,
        highRangeNumberOfChannels = 0;
    if (update === 1 && ganged_channels === 1) {
        if (deviceWidth === 4) {
            if (lowRangeDataBusSize < 128) {
                auto_change_bus_size = 1;
                while (lowRangeDataBusSize < 128) {
                    incrementDataBusSize(1);                             // increment total memory size until accurate dual-ranked memory representation is possible
                    lowRangeDataBusSize *= 2;
                }
            }
        }
    } else if (update === 1 && ganged_channels === 0) {
        if (deviceWidth === 4) {
            if (lowRangeNumberOfChannels < 2) {
                auto_change_number_of_channels = 1;
                while (lowRangeNumberOfChannels < 2) {
                    incrementDataBusSize(1);                             // increment total memory size until accurate dual-ranked memory representation is possible
                    lowRangeNumberOfChannels *= 2;
                }
            }
        }
    }
    highRangeDataBusSize  = splitRange(formID.bus);
    if (highRangeDataBusSize === 0) {
        highRangeDataBusSize = lowRangeDataBusSize;
    }
    if (highRangeDataBusSize < 64) {
        highRangeDataBusSize = 64;
    }
    highRangeNumberOfChannels = splitRange(formID.channels);
    if (highRangeNumberOfChannels === 0) {
        highRangeNumberOfChannels = lowRangeNumberOfChannels;
    }
    if (highRangeNumberOfChannels < 1) {
        highRangeNumberOfChannels = 1;
    }
    return highRangeDataBusSize * highRangeNumberOfChannels;
}

/* calculate number of channels  if ganged */
function numberOfChannels(update) {
    "use strict";
    var currentDataBusSize = getDataBusSizeOrChannels(update);
    return (currentDataBusSize / 64);                                     // number of channels equals (bus size/64)
}

/* display DRAM extensions */
function showExtensions() {
    "use strict";
    $(".extension_left").css({
        "visibility": "visible"
    });
    $(".extension_right").css({
        "visibility": "visible"
    });
}

/* hide DRAM extensions */
function hideExtensions() {
    "use strict";
    $(".extension_left").css({
        "visibility": "hidden"
    });
    $(".extension_right").css({
        "visibility": "hidden"
    });
}

/* set the current dimm's channel and ranks */
function setChannelsAndRanks(newNode, currentChannelNumber, currentRank, number_of_ranks_per_dimm) {
    "use strict";
    $(newNode).css({
        "display": "block"
    });
    $(newNode).find("#channel_text").text("CHANNEL " + currentChannelNumber);
    $(newNode).find("#channel_text").css({
        "font-size": "18px",
        "text-align": "center"
    });
    $(newNode).find("#rank_text0").text("RANK " + currentRank);
    $(newNode).find("#rank_text_1").text("RANK " + (currentRank + 1));

    if (currentRank > 9) {
        $(newNode).find("#rank_text0").attr('x', $(newNode).find("#rank_text0").attr('x') - 9);
    }

    if (number_of_ranks_per_dimm === 4) {
        $(newNode).find("#rank_text_2").text("RANK " + (currentRank + 2));
        $(newNode).find("#rank_text_3").text("RANK " + (currentRank + 3));
        $(newNode).find("#rank_text_2").css('font-size', '14px');
        $(newNode).find("#rank_text_3").css('font-size', '14px');
        $(newNode).find("#rank_text_2").css('visibility', 'visible');
        $(newNode).find("#rank_text_3").css('visibility', 'visible');
        $(newNode).find("#rank_arrow_2").css('visibility', 'visible');
        $(newNode).find("#rank_arrow_3").css('visibility', 'visible');
    } else {
        $(newNode).find("#rank_text_2").css('visibility', 'hidden');
        $(newNode).find("#rank_text_3").css('visibility', 'hidden');
        $(newNode).find("#rank_arrow_2").css('visibility', 'hidden');
        $(newNode).find("#rank_arrow_3").css('visibility', 'hidden');
    }
    $(newNode).find("#rank_text0").css({
        "font-size": "14px",
        "text-align": "center"
    });
    $(newNode).find("#rank_text_1").css({
        "font-size": "14px",
        "text-align": "center"
    });
    if (ganged_channels === 0) {
        $(newNode).find(".input_channel").css('stroke', colors[currentChannelNumber]);
        $(newNode).find(".output_channel").css('stroke', colors[currentChannelNumber]);
        $(newNode).find(".extension_left").css('stroke', 'black');
        $(newNode).find(".extension_right").css('stroke', 'black');
        $(newNode).find(".bus_part").css('stroke', 'black');
    }
    d3.select(".dramsim2-embed5").node().appendChild(newNode);
    showExtensions();
    drawDramDevices();
}

/* draw a ganged dimm */
function add_ganged_dimm(channelNumber, position, current_rank, deviceWidth) {
    "use strict";
    if (position === 0) {
        d3.xml("/images/occam_dramsim2_additional_channel_last.svg", "image/svg+xml", function (xml) {        // top dimm, without extensions
            var importedNode = document.importNode(xml.documentElement, true);
            setChannelsAndRanks(importedNode, channelNumber, current_rank, deviceWidth);
        });
    } else {
        d3.xml("/images/occam_dramsim2_additional_channel.svg", "image/svg+xml", function (xml) {             // middle dimms, with extensions
            var importedNode = document.importNode(xml.documentElement, true);
            setChannelsAndRanks(importedNode, channelNumber, current_rank, deviceWidth);
        });
    }
}

 /* set ranks, adds the dimm */
function setRanks(current_rank, number_of_channels, number_of_dimms_per_channel, ranks_per_dimm) {
    "use strict";
    var current_channel = 0,                                      // current channel
        current_channel_offset = 0,                               // current channel position
        old_rank = current_rank,
        temp_rank = 0,
        i = 1,
        j = 1;
    for (i = 1; i < number_of_channels + 1; i += 1) {
        if (i === number_of_channels) {
            for (j = 1; j < number_of_dimms_per_channel; j += 1) {
                current_channel = number_of_channels + 1 - i;
                current_channel_offset = (j * i - 1);            // if zero, draws last dimm without extensions
                add_ganged_dimm(current_channel, current_channel_offset, current_rank, ranks_per_dimm);  // add ganged dimm with correct channel and rank labels for first channel (one less than all other channels since first dimm is automatically displayed)
                if (ranks_per_dimm === 1) {
                    current_rank -= 2;
                } else if (ranks_per_dimm === 2) {
                    current_rank -= 2;                           // decrement current rank for next dimm below
                } else {
                    current_rank -= 4;                           // decrement current rank for next dimm below
                }
            }
        } else {
            for (j = 1; j < number_of_dimms_per_channel + 1; j += 1) {
                current_channel = number_of_channels + 1 - i;    // must draw channels from last channel to first channel
                current_channel_offset = (j * i - 1);            // if zero, draws last dimm without extensions
                add_ganged_dimm(current_channel, current_channel_offset, current_rank, ranks_per_dimm);    // add ganged dimm with correct channel and rank labels for all channels except the first channel
                if (ranks_per_dimm === 1) {
                    current_rank -= 2;
                } else if (ranks_per_dimm === 2) {
                    current_rank -= 2;                           // decrement current rank for next dimm below
                } else {
                    current_rank -= 4;                           // decrement current rank for next dimm below
                }
            }
        }
        if (ranks_per_dimm === 1) {
            temp_rank = current_rank;
            current_rank = old_rank;
            old_rank = temp_rank;
        }
    }
    if (ganged_channels === 0) {
        $("#first_input_channel").css('stroke', 'red');
        $("#first_output_channel").css('stroke', 'red');
        $(".extension_left").css('stroke', 'black');
        $(".extension_right").css('stroke', 'black');
        $(".input_bus").css('stroke', 'black');
        $(".output_bus").css('stroke', 'black');
    } else {
        $("#first_input_channel").css('stroke', '#ff6600');
        $("#first_output_channel").css('stroke', '#800080');
        $(".extension_left").css('stroke', '#ff6600');
        $(".extension_right").css('stroke', '#800080');
        $(".input_bus").css('stroke', '#ff6600');
        $(".output_bus").css('stroke', '#800080');
    }
}

/* set CSS of label */
function setLabelCSS(labelID, fontWeight, fontSize) {
    "use strict";
    $(labelID).css('font-weight', fontWeight);
    $(labelID).css('font-size', fontSize);
    $(labelID).css('text-align', 'center');
}

/* draws the dimms */
function drawDimms(noAlert, currenttotalDimms) {
    "use strict";
    setGangedChannels();
    drawMemoryControllers();
    var number_of_ranks = getAccurateMemory(),                                      // get number of ranks per channel
        total_dimms = 0,
        number_of_channels = 0,
        number_of_dimms_per_channel = 0,
        ranks_per_dimm = 0,
        current_rank = 0,
        lowRangetotalStorage = 0,
        highRangetotalStorage = 0,
        totalStorage = 0,
        len = 0;
    if (number_of_ranks < 1 && currenttotalDimms < 16) {                            // make sure there is enough total memory being simulated
        auto_change_memory_size = 1;
        while (number_of_ranks < 1) {
            incrementMemorySize(1);                                                 // increment total memory size until accurate dual-ranked memory representation is possible
            number_of_ranks = getAccurateMemory();
        }
        number_of_ranks = getAccurateMemory();                                      // get number of ranks per channel
    }

    total_dimms = calculateTotalDimms();
    number_of_channels = numberOfChannels(1);                                       // number of channels equals (bus size/64)

    number_of_dimms_per_channel = (total_dimms / number_of_channels);               // calculate the number of dimms per channel
    if (number_of_dimms_per_channel < 1 && currenttotalDimms < 16) {                // if each dimm is not dual ranked
        auto_change_memory_size = 1;
        while (number_of_dimms_per_channel < 1) {
            incrementMemorySize();                                                  // increment total memory size until accurate dual-ranked memory representation is possible
            number_of_channels = numberOfChannels(0);
            total_dimms = calculateTotalDimms();
            number_of_dimms_per_channel = (total_dimms / number_of_channels);       // calculate the number of dimms per channel
        }
        number_of_ranks = getAccurateMemory();
        number_of_channels = numberOfChannels(0);                                   // number of channels equals (bus size/64)
        total_dimms = calculateTotalDimms();
        number_of_dimms_per_channel = (total_dimms / number_of_channels);           // calculate the number of dimms per channel
    }

    ranks_per_dimm = (number_of_ranks / number_of_dimms_per_channel);
    if (total_dimms < 17) {
        $(".dramsim2-embed5").empty();                                              // empty the dimm slots
        current_rank = (2 * number_of_channels * number_of_dimms_per_channel) - 2;  // total ranks equals number of channels * number of dimms per channel * 2 ranks per dimm
        if (ranks_per_dimm === 1) {
            current_rank = (current_rank / 2) - 1;
        } else if (ranks_per_dimm === 4) {
            current_rank = (current_rank * 2);
        }
        setRanks(current_rank, number_of_channels, number_of_dimms_per_channel, ranks_per_dimm);
    } else if (noAlert !== 1) {
        undoAction = 1;
        lowRangetotalStorage = parseInt($(formID.memory_size).val(), 10);
        highRangetotalStorage = splitRange(formID.memory_size);
        if (highRangetotalStorage === 0) {
            highRangetotalStorage = lowRangetotalStorage;
        }
        totalStorage = highRangetotalStorage;
        alert(total_dimms + " DIMMs are required to represent " + totalStorage + " MB with the current DRAM device configuration. The configurator is limited to 16 DIMMs. In order to represent the desired memory size, try incrementing the number of banks, number of columns, number of rows, or device width to increase the total storage per DRAM device. Otherwise, decrement the total memory size.");
    }
    if (getDataBusSizeOrChannels(0) === 128) {
        showExtensions();
    }
    if (getDataBusSizeOrChannels(0) === 64) {
        hideExtensions();
    }
    //alert ("ranks_per_dimm  = " + ranks_per_dimm);
    //alert ("dimms per channel  = " + number_of_dimms_per_channel);
    if (ranks_per_dimm === 4) {
        $(".viz").find("#rank_text_2").css('visibility', 'visible');
        $(".viz").find("#rank_text_3").css('visibility', 'visible');
        $(".viz").find("#rank_arrow_2").css('visibility', 'visible');
        $(".viz").find("#rank_arrow_3").css('visibility', 'visible');
    } else {
        $(".viz").find("#rank_text_2").css('visibility', 'hidden');
        $(".viz").find("#rank_text_3").css('visibility', 'hidden');
        $(".viz").find("#rank_arrow_2").css('visibility', 'hidden');
        $(".viz").find("#rank_arrow_3").css('visibility', 'hidden');
    }
    if (auto_change_memory_size === 1) {
        alert("Memory size has been automatically updated to the minimum size that is compatible with the current configuration, " + $(formID.memory_size).val() + " MB.");
        auto_change_memory_size = 0;
    }
    if (auto_change_bus_size === 1) {
        alert("Data bus bit size has been automatically updated to 128 bits, the minimum size that is compatible with a dram device width of " + $(formID.device_width).val() + " bits.");
        auto_change_bus_size = 0;
        drawDimms(1, calculateTotalDimms());
    }
    if (auto_change_number_of_channels === 1) {
        alert("Number of channels has been automatically updated to 2, the minimum size that is compatible with a dram device width of " + $(formID.device_width).val() + " bits.");
        auto_change_number_of_channels = 0;
        drawDimms(1, calculateTotalDimms());
    }

    len = dram_device_size.toString().length;                                      // set initial DRAM device memory size
    $("#dram_memory_size").attr("x", (635.46552 - (len * 2)));
    $("#dram_memory_size").text(dram_device_size + " MB");
    setLabelCSS("#dram_memory_size", "bold", "20px");
    setAllGraphParameterFormInputs($(".pop-ups-and-inputs").offset().top - 600, 0);                                       // set timing graph parameters input positioning
    setAllGraphParameterFormInputs($(".pop-ups-and-inputs").offset().top - 87, 1);                                       // set power graph parameters input positioning
}


/* function to increment memory size */
function incrementMemorySize(drawDiagram) {
    "use strict";
    var currentSize = parseInt($(formID.memory_size).val(), 10),
        highRange = splitRange(formID.memory_size),
        newSize = 0,
        oldRange = 0,
        len = 0;
    if ($(formID.memory_size).val() !== 0) {
        if (ranges.memory_size === 0) {
            oldRange = $(formID.memory_size).val();
            newSize = currentSize * 2;
        } else {
            oldRange = $(formID.memory_size).val();
            if (highRange === 0) {
                highRange = currentSize * 2;
            } else {
                highRange *= 2;
            }
            newSize = currentSize + " to " + highRange;
        }
        len = newSize.toString().length;
        $(formID.memory_size).val(newSize);
        $("#memory_size").text(newSize + " MB");
        $("#memory_size").attr("x", (112 + (len * 0.05)));
        updatePopUpTips("#memory_size_pop_up", formID.memory_size, 0);
        if (drawDiagram === 0) {
            drawDimms(0, calculateTotalDimms());        // redraw dimms
            drawDramDevices();
        }
        if (undoAction === 1) {
            alert("Number of total memory size to reset to " + oldRange + "MB.");
            len = oldRange.toString().length;
            $(formID.memory_size).val(oldRange);
            $("#memory_size").text(oldRange + " MB");
            $("#memory_size").attr("x", 112 + (len * 0.05));
            updatePopUpTips("#memory_size_pop_up", formID.memory_size, 0);
            undoAction = 0;
            if (drawDiagram === 0) {
                drawDimms(1, calculateTotalDimms());    // redraw dimms with no alert
                drawDramDevices();
            }
        }
    }
}

/* increment data bus bit size */
function incrementDataBusSize(drawDiagram) {
    "use strict";
    var currentBusSize = parseInt($(formID.bus).val(), 10),
        newBusSize = 0,
        oldRange = 0,
        highRange = splitRange(formID.bus),
        len = 0;
    if (ganged_channels === 1) {
        if (highRange === 'undefined' || ranges.data_bus_range === 0) {
            highRange = currentBusSize;
        }
        if (highRange !== 512) {
            if (ranges.data_bus_range === 0) {
                oldRange = $(formID.bus).val();
                currentBusSize = parseInt($(formID.bus).val(), 10);
                newBusSize = currentBusSize * 2;
            } else {
                oldRange = $(formID.bus).val();
                currentBusSize = parseInt($(formID.bus).val(), 10);
                highRange = splitRange(formID.bus);
                if (highRange === 'undefined') {
                    highRange = currentBusSize * 2;
                } else {
                    if (drawDiagram === 0) {
                        highRange *= 2;
                    } else {
                        currentBusSize *= 2;
                    }
                }
                if (currentBusSize !== highRange) {
                    newBusSize = currentBusSize + " to " + highRange;
                } else {
                    newBusSize = currentBusSize;
                }
            }
            len = newBusSize.toString().length;
            $(formID.bus).val(newBusSize);
            currentBusSize = parseInt($(formID.bus).val(), 10);
            highRange = splitRange(formID.bus);
            if (highRange === 'undefined') {
                highRange = currentBusSize;
            }
            $("#bus").text(newBusSize + " bits");
            $("#bus").attr("x", 120 - len);
            $("#bus2").attr("x", 36 - len);
            $("#bus2").text(highRange + " bits");
            updatePopUpTips("#bus_pop_up", formID.bus, 0);
            if (drawDiagram === 0) {
                drawDimms(0, calculateTotalDimms());
            }
            if (undoAction === 1) {
                alert("Data bus bit size reset to " + oldRange + " bits.");
                len = oldRange.toString().length;
                $(formID.bus).val(oldRange);
                $("#bus").text(oldRange + " bits");
                $("#bus").attr("x", 120 - len);
                $("#bus2").attr("x", 36 - len);

                if (highRange !== 'undefined') {
                    $("#bus2").text(highRange / 2 + " bits");
                } else {
                    $("#bus2").text(oldRange + " bits");
                }
                updatePopUpTips("#bus_pop_up", formID.bus, 0);
                undoAction = 0;
                if (drawDiagram === 0) {
                    drawDimms(1, calculateTotalDimms());                            // redraw dimms with no alert, no increment memory
                }
            }
        }
    } else {
        if (ranges.data_bus_range === 0) {
            oldRange = $(formID.channels).val();
            currentBusSize = parseInt($(formID.channels).val(), 10);
            newBusSize = currentBusSize * 2;
        } else {
            oldRange = $(formID.channels).val();
            currentBusSize = parseInt($(formID.channels).val(), 10);
            highRange = splitRange(formID.channels);
            if (highRange === 'undefined') {
                highRange = currentBusSize * 2;
            } else {
                if (drawDiagram === 0) {
                    highRange *= 2;
                } else {
                    currentBusSize *= 2;
                }
            }
            if (currentBusSize !== highRange) {
                newBusSize = currentBusSize + " to " + highRange;
            } else {
                newBusSize = currentBusSize;
            }
        }
        len = newBusSize.toString().length;
        $(formID.channels).val(newBusSize);
        currentBusSize = parseInt($(formID.channels).val(), 10);
        $("#bus").text(newBusSize);
        $("#bus").attr("x", 120 - len);
        $("#bus2").attr("x", 36 - len);
        $("#bus2").text("64 bits");
        updatePopUpTips("#channels_pop_up", formID.channels, 0);
        if (drawDiagram === 0) {
            drawDimms(0, calculateTotalDimms());
        }
        if (undoAction === 1) {
            alert("Number of channels reset to " + oldRange + " bits.");
            len = oldRange.toString().length;
            $(formID.channels).val(oldRange);
            $("#bus").text(oldRange);
            $("#bus").attr("x", 120 - len);
            $("#bus2").attr("x", 36 - len);
            $("#bus2").text("64 bits");
            updatePopUpTips("#channels_pop_up", formID.channels, 0);
            undoAction = 0;
            if (drawDiagram === 0) {
                drawDimms(1, calculateTotalDimms());                              // redraw dimms with no alert, no increment memory
            }
        }
    }
}

/* set the number of arrays per bank */
function drawBankArrays() {
    "use strict";
    var deviceWidth = parseInt($(formID.device_width).val(), 10),                // get the device width
        i = 1,
        j = 0;
    while (i < (deviceWidth + 1)) {
        $("#bank_layer" + i).css({
            "visibility": "visible"
        });
        i += 1;
    }
    for (j = i; j < 65; j += 1) {
        $("#bank_layer" + j).css({
            "visibility": "hidden"
        });
    }
}

/* hide or display the given i/o pin */
function setIOPins(link_number, displayProperty, notSixtyFour) {
    "use strict";
    var currWidth = 0;
    $(link_number).attr("width", "1.332801");
    if (displayProperty === "visible" && notSixtyFour === 1) {
        currWidth = parseFloat($(link_number).attr("width")) + 3;
        $(link_number).attr("width", currWidth);
    } else if (displayProperty === "visible" && notSixtyFour === 2) {
        currWidth = parseFloat($(link_number).attr("width")) + 2;
        $(link_number).attr("width", currWidth);
    } else if (displayProperty === "visible") {
        currWidth = parseFloat($(link_number).attr("width")) + 1;
        $(link_number).attr("width", currWidth);
    }
    $(link_number).css('visibility', displayProperty);
}

/* set the number of i/o pins equal to device width */
function drawIOPins() {
    "use strict";
    var deviceWidth = parseInt($(formID.device_width).val(), 10),                   // get the device width
        i = 0;
    if (deviceWidth === 1) {                                                        // if device width is 1
        while (i < 64) {
            if (i === 31) {                                                         // set 1 pin visible 
                setIOPins("#l" + i, "visible", 1);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 1);                                   // set pin to hidden
            }
            i += 1;
        }
    } else if (deviceWidth === 2) {                                                 // if device width is 2
        while (i < 64) {                                                            // set 2 pins visible
            if (i % 32 === 16) {
                setIOPins("#l" + i, "visible", 1);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 1);                                   // set pin to hidden
            }
            i += 1;
        }
    } else if (deviceWidth === 4) {                                                 // if device width is 4
        while (i < 64) {                                                            // set 4 pins visible
            if (i % 16 === 7) {
                setIOPins("#l" + i, "visible", 1);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 1);                                   // set pin to hidden
            }
            i += 1;
        }
    } else if (deviceWidth === 8) {                                                 // if device width is 8
        while (i < 64) {                                                            // set 8 pins visible
            if (i % 8 === 3) {
                setIOPins("#l" + i, "visible", 1);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 1);                                   // set pin to hidden
            }
            i += 1;
        }
    } else if (deviceWidth === 16) {                                                 // if device width is 16
        while (i < 64) {                                                             // set 16 pins visible
            if (i % 4 === 1) {
                setIOPins("#l" + i, "visible", 1);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 1);                                   // set pin to hidden
            }
            i += 1;
        }
    } else if (deviceWidth === 32) {                                                // if device width is 32
        while (i < 64) {                                                            // set 32 pins visible
            if (i % 2 === 0) {
                setIOPins("#l" + i, "visible", 2);                                  // set pin to visible
            } else {
                setIOPins("#l" + i, "hidden", 2);                                   // set pin to hidden
            }
            i += 1;
        }
    } else {                                                                        // if device width is 64
        while (i < 64) {                                                            // set 64 pins visible
            setIOPins("#l" + i, "visible", 0);                                      // set pin to visible
            i += 1;
        }
    }
}

/* reset the given pin connecting from  MUX to bank */
function resetMUXCurrentPin(linkNumber, xAttr) {
    "use strict";
    $(linkNumber).attr("x", xAttr);
    $(linkNumber).attr("width", "0.53376454");
    $(linkNumber).attr("height", "5.5915699");
}

/* reset all pins connecting from  MUX to banks */
function resetMUXPins() {
    "use strict";
    var currentX = 467.02182,
        i = 0;
    for (i = 0; i < 1024; i += 1) {
        if (i % 256 === 0 && i !== 0) {
            currentX = 467.02182;
        } else if (i % 16 === 0 && i !== 0 && i !== 256 && i !== 512 && i !== 1028) {
            currentX += 2.88;
        }
        resetMUXCurrentPin("#m" + i, currentX);
        currentX += 0.76755;
    }
}

/* reset the given bank */
function resetCurrentBank(bankNumber, xAttr) {
    "use strict";
    $(bankNumber).attr("x", xAttr);
    $(bankNumber).attr("width", "12.033669");
    $(bankNumber).attr("height", "15.62151");
}

/* reset all banks */
function resetBanks() {
    "use strict";
    resetCurrentBank("#bank0", -479.08444);
    resetCurrentBank("#bank1", -494.15887);
    resetCurrentBank("#bank2", -509.32043);
    resetCurrentBank("#bank3", -524.56171);
    resetCurrentBank("#bank4", -539.77765);
    resetCurrentBank("#bank5", -554.85211);
    resetCurrentBank("#bank6", -570.01361);
    resetCurrentBank("#bank7", -585.25488);
}

/* hide or display the given mux pin */
function setMUXPins(pin_number, displayProperty, currWidth, xAttr) {
    "use strict";
    if (displayProperty === "visible" && currWidth !== 0 && xAttr !== 0) {
        $(pin_number).attr("width", currWidth);
        $(pin_number).attr("x", xAttr);
    }
    $(pin_number).css('visibility', displayProperty);
}

/* hide or display the given bank */
function setBanks(bankNumber, displayProperty, widthOffset, heightOffset, xAttrOffset, moveBank) {
    "use strict";
    var currWidthBank = parseFloat($(bankNumber).attr("width")) + widthOffset,
        currHeightBank = parseFloat($(bankNumber).attr("height")) + heightOffset,
        currXBank = parseFloat($(bankNumber).attr("x")) - xAttrOffset;
    if (displayProperty === "visible" && moveBank === 1) {
        $(bankNumber).attr("x", currXBank);
        $(bankNumber).attr("width", currWidthBank);
        $(bankNumber).attr("height", currHeightBank);
    }
    $(bankNumber).css('visibility', displayProperty);
}

/* display the current number of banks */
function drawBanks(num_banks) {
    "use strict";
    var i = 0;
    if (num_banks === 1) {
        $("#chip_circle").attr('transform', "matrix(1.15,0,0,1,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 221.8,185.16218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 563.92578,828.88312 338.51746,839.29949");
        setBanks("#bank0", "visible", 25, 25, 125, 1);
        i = 1;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 25, 25, 125, 1);
            i += 1;
        }
    } else if (num_banks === 2) {
        $("#chip_circle").attr('transform', "matrix(0.98,0,0,0.97,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 193,195.16218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 502.92578,828.88312 338.51746,839.29949");
        setBanks("#bank0", "visible", 20, 20, 60, 1);
        setBanks("#bank1", "visible", 20, 20, 174, 1);
        i = 2;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 20, 20, 60, 1);
            i += 1;
        }
    } else if (num_banks === 4) {
        $("#chip_circle").attr('transform', "matrix(0.82,0,0,0.81,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 205.2,235.16218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 488.92578,828.88312 338.51746,839.29949");
        setBanks("#bank0", "visible", 15, 15, 40, 1);
        setBanks("#bank1", "visible", 15, 15, 77, 1);
        setBanks("#bank2", "visible", 15, 15, 116, 1);
        setBanks("#bank3", "visible", 15, 15, 154, 1);
        i = 4;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 15, 15, 40, 1);
            i += 1;
        }
    } else if (num_banks === 8) {
        $("#chip_circle").attr('transform', "matrix(0.6563425,0,0,0.65499818,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 215,297.36218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 468.92578,828.88312 338.51746,839.29949");
        setBanks("#bank0", "visible", 8, 8, 13, 1);
        setBanks("#bank1", "visible", 8, 8, 28, 1);
        setBanks("#bank2", "visible", 8, 8, 43, 1);
        setBanks("#bank3", "visible", 8, 8, 58, 1);
        setBanks("#bank4", "visible", 8, 8, 73, 1);
        setBanks("#bank5", "visible", 8, 8, 88, 1);
        setBanks("#bank6", "visible", 8, 8, 103, 1);
        setBanks("#bank7", "visible", 8, 8, 118, 1);
        i = 8;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 8, 8, 13, 1);
            i += 1;
        }
    } else if (num_banks === 16) {
        $("#chip_circle").attr('transform', "matrix(0.43763425,0,0,0.42399818,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 290,469.36218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 462.92578,828.88312 338.51746,839.29949");
        i = 0;
        while (i < 16) {
            setBanks("#bank" + i, "visible", 0, 0, 0, 0);
            i += 1;
        }
        i = 16;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 0, 0, 0, 0);
            i += 1;
        }
    } else if (num_banks === 32) {
        $("#chip_circle").attr('transform', "matrix(0.43763425,0,0,0.42399818,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 290,469.36218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 462.92578,828.88312 338.51746,839.29949");
        i = 0;
        while (i < 32) {
            setBanks("#bank" + i, "visible", 0, 0, 0, 0);
            i += 1;
        }
        i = 32;
        while (i < 64) {
            setBanks("#bank" + i, "hidden", 0, 0, 0, 0);
            i += 1;
        }
    } else {
        $("#chip_circle").attr('transform', "matrix(0.43763425,0,0,0.42399818,355.435,627.94806)");
        $("#chip_circle").attr('d', "m 290,469.36218 c 0,16.01626 -9.84974,29 -22,29 -12.15026,0 -22,-12.98374 -22,-29 0,-16.01626 9.84974,-29 22,-29 12.15026,0 22,12.98374 22,29 z");
        $("#chip_arrow").attr('d', "M 462.92578,828.88312 338.51746,839.29949");
        i = 0;
        while (i < 64) {
            setBanks("#bank" + i, "visible", 0, 0, 0, 0);
            i += 1;
        }
    }
}

/* set the number of MUX pins equal to the device width */
function drawMUXPins() {
    "use strict";
    var deviceWidth = parseInt($(formID.device_width).val(), 10),               // get the device width
        num_banks = parseInt($(formID.banks).val(), 10),                        // get the number of banks
        i = 0,
        pos = 0,
        countBank = 1,
        currXLink = 0,
        currWidthLink = 0;
    resetMUXPins();                                                             // reset the mux pins
    resetBanks();                                                               // reset banks
    if (deviceWidth === 1) {
        while (i < (16 * num_banks) + 1) {
            pos = i - 1;
            if (i % 8 === 0 && i % 16 !== 0) {
                $("#m" + pos).attr("width", "0.53376454");                      // reset the width
                currWidthLink = parseFloat($("#m" + i).attr("width")) + 3;      // calculate the new width
                if (num_banks === 1) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + 112;
                } else if (num_banks === 2) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + ((Math.pow(countBank, 1.735)) * 49);
                    countBank += 1;
                } else if (num_banks === 4) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + ((countBank * 38) - 7);
                    countBank += 1;
                } else if (num_banks === 8) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + ((countBank * 15) - 7);
                    countBank += 1;
                } else {
                    currXLink = parseFloat($("#m" + i).attr("x")) - 2;
                }
                setMUXPins("#m" + pos, "visible", currWidthLink, currXLink);    // set the mux pin visible
            } else {
                setMUXPins("#m" + pos, "hidden", 0, 0);                         // set the mux pin hidden
            }
            i += 1;
        }
        while (i < 1025) {                                                      // hide the remaining mux pins
            pos = i - 1;
            setMUXPins("#m" + pos, "hidden", 0, 0);
            i += 1;
        }
    } else if (deviceWidth === 2) {
        while (i < (16 * num_banks) + 1) {
            pos = i - 1;
            if (i % 4 === 0 && i % 8 !== 0) {
                $("#m" + pos).attr("width", "0.53376454");
                currWidthLink = parseFloat($("#m" + i).attr("width")) + 3;
                if (num_banks === 1) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + ((Math.pow(countBank, 0.13) * 106));
                    countBank += 1;
                } else if (num_banks === 2) {
                    if (countBank < 3) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 6) + 39;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 6) + 141;
                    }
                    countBank += 1;
                } else if (num_banks === 4) {
                    if (countBank < 3) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 6) + 22;
                    } else if (countBank < 5) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 6) + 47;
                    } else if (countBank < 7) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 6) + 74;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 6) + 100;
                    }
                    countBank += 1;
                } else if (num_banks === 8) {
                    if (countBank < 3) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 1;
                    } else if (countBank < 5) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 8;
                    } else if (countBank < 7) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 15;
                    } else if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 22;
                    } else if (countBank < 11) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 29;
                    } else if (countBank < 13) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 36;
                    } else if (countBank < 14) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 43;
                    } else if (countBank < 15) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 4) + 43;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 4) + 50;
                    }
                    countBank += 1;
                } else {
                    currXLink = parseFloat($("#m" + i).attr("x")) - 2;
                }
                setMUXPins("#m" + pos, "visible", currWidthLink, currXLink);
            } else {
                setMUXPins("#m" + pos, "hidden", 0, 0);
            }
            i += 1;
        }
        while (i < 1025) {
            pos = i - 1;
            setMUXPins("#m" + pos, "hidden", 0, 0);
            i += 1;
        }
    } else if (deviceWidth === 4) {
        while (i < (16 * num_banks) + 1) {
            pos = i - 1;
            if (i % 2 === 0 && i % 4 !== 0) {
                $("#m" + pos).attr("width", "0.53376454");
                currWidthLink = parseFloat($("#m" + i).attr("width")) + 3;
                if (num_banks === 1) {
                    currXLink = parseFloat($("#m" + i).attr("x")) +  (countBank  * 6) + 96;
                    countBank += 1;
                } else if (num_banks === 2) {
                    if (countBank < 5) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 6) + 33;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 6) + 123;
                    }
                    countBank += 1;
                } else if (num_banks === 4) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 2;
                    if (countBank < 5) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 21;
                    } else if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 42;
                    } else if (countBank < 13) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 4) + 65;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 4) + 87;
                    }
                    countBank += 1;
                } else if (num_banks === 8) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.5;
                    if (countBank < 5) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 3;
                    } else if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 10;
                    } else if (countBank < 13) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 17;
                    } else if (countBank < 17) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 24;
                    } else if (countBank < 21) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 31;
                    } else if (countBank < 25) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 38;
                    } else if (countBank < 29) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2) + 45;
                    } else if (countBank < 33) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 2) + 52;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 2) + 59;
                    }
                    countBank += 1;
                } else if (num_banks === 16) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.5;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 1;
                } else if (num_banks === 32) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.5;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 1;
                } else {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.5;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 1;
                }
                setMUXPins("#m" + pos, "visible", currWidthLink, currXLink);
            } else {
                setMUXPins("#m" + pos, "hidden", 0, 0);
            }
            i += 1;
        }
        while (i < 1025) {
            pos = i - 1;
            setMUXPins("#m" + pos, "hidden", 0, 0);
            i += 1;
        }
    } else if (deviceWidth === 8) {
        while (i < (16 * num_banks) + 1) {
            pos = i - 1;
            if (i % 2 !== 0) {
                $("#m" + pos).attr("width", "0.53376454");
                currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.6;
                if (num_banks === 1) {
                    currXLink = parseFloat($("#m" + i).attr("x")) +  (countBank  * 3.1) + 97;
                    countBank += 1;
                } else if (num_banks === 2) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1.5;
                    if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 2.5) + 38;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 2.5) + 132;
                    }
                    countBank += 1;
                } else if (num_banks === 4) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 1;
                    if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 1.9) + 23;
                    } else if (countBank < 17) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 1.9) + 44.76;
                    } else if (countBank < 25) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 1.9) + 68.76;
                    } else if (countBank < 33) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 1.9) + 91.62;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x"));
                    }
                    countBank += 1;
                } else if (num_banks === 8) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 0.6;
                    if (countBank < 9) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank +  4;
                    } else if (countBank < 17) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 11;
                    } else if (countBank < 25) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank  + 18;
                    } else if (countBank < 33) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 25;
                    } else if (countBank < 41) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 32;
                    } else if (countBank < 49) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 39;
                    } else if (countBank < 57) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 46;
                    } else if (countBank < 65) {
                        currXLink = parseFloat($("#m" + i).attr("x")) + countBank + 53;
                    } else {
                        currXLink = parseFloat($("#m" + i).attr("x"));
                    }
                    countBank += 1;
                } else if (num_banks === 16) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 0.1;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 0.5;
                } else if (num_banks === 32) {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 0.1;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 0.5;
                } else {
                    currWidthLink = parseFloat($("#m" + i).attr("width")) + 0.1;
                    currXLink = parseFloat($("#m" + i).attr("x")) - 0.5;
                }
                setMUXPins("#m" + pos, "visible", currWidthLink, currXLink);
            } else {
                setMUXPins("#m" + pos, "hidden", 0, 0);
            }
            i += 1;
        }
        while (i < 1025) {
            pos = i - 1;
            setMUXPins("#m" + pos, "hidden", 0, 0);
            i += 1;
        }
    } else {
        while (i < (16 * num_banks)) {
            pos = i;
            if (num_banks === 1) {
                currXLink = parseFloat($("#m" + i).attr("x")) +  (countBank  * 1.55) + 99;
                $("#m" + pos).attr("width", "1.1");
                countBank += 1;
            } else if (num_banks === 2) {
                if (countBank < 17) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 1.22) + 39.48;
                } else {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 1.22) + 133.8;
                }
                $("#m" + pos).attr("width", "0.9");
                countBank += 1;
            } else if (num_banks === 4) {
                if (countBank < 17) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.9) + 25;
                } else if (countBank < 33) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.9) + 47.4;
                } else if (countBank < 49) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.9) + 71.95;
                } else if (countBank < 65) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.9) + 95.85;
                } else {
                    currXLink = parseFloat($("#m" + i).attr("x"));
                }
                $("#m" + pos).attr("width", "0.68");
                countBank += 1;
            } else if (num_banks === 8) {
                if (countBank < 17) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) +  5.1;
                } else if (countBank < 33) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 12.95;
                } else if (countBank < 49) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 20.88;
                } else if (countBank < 65) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 28.77;
                } else if (countBank < 81) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 36.45;
                } else if (countBank < 97) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 44.16;
                } else if (countBank < 113) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank * 0.45) + 52.08;
                } else if (countBank < 129) {
                    currXLink = parseFloat($("#m" + i).attr("x")) + (countBank  * 0.45) + 59.93;
                } else {
                    currXLink = parseFloat($("#m" + i).attr("x"));
                }
                $("#m" + pos).attr("width", "0.52");
                countBank += 1;
            } else {
                $("#m" + pos).attr("width", "0.5");
            }
            if (num_banks < 16) {
                $("#m" + pos).attr("x", currXLink);
            }
            setMUXPins("#m" + pos, "visible", 0, 0);
            i += 1;
        }
        while (i < 1025) {
            pos = i;
            setMUXPins("#m" + pos, "hidden", 0, 0);
            i += 1;
        }
    }
    drawBanks(num_banks);
}

/* set button interactions */
function setButtonInteractions(currentParameter, currentUpFunction, currentDownFunction, params) {
    "use strict";
    $("#" + currentParameter + "_up").mousedown(function () {                                        // mouse down and up function for select up arrow
        setButtonCSS("#" + currentParameter + "_up2", "#" + currentParameter + "_up", 0, 0);
        if (params === 1) {
            currentUpFunction(0);
        } else {
            currentUpFunction();
        }
    }).bind('mouseup mouseleave', function () {
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_up2", "#" + currentParameter + "_up", 0, 1);         // mouse down on arrow
    });

    $("#" + currentParameter + "_up2").mousedown(function () {                                       //mouse down and up functions for select up box
        setButtonCSS("#" + currentParameter + "_up2", "#" + currentParameter + "_up", 0, 0);         // mouse down on box
        if (params === 1) {
            currentUpFunction(0);
        } else {
            currentUpFunction();
        }
    }).bind('mouseup mouseleave', function () {
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_up2", "#" + currentParameter + "_up", 0, 1);         // mouse up on box
    });

    $("#" + currentParameter + "_down").mousedown(function () {                                      // mouse down and up function for select down arrow
        setButtonCSS("#" + currentParameter + "_down2", "#" + currentParameter + "_down", 0, 0);     // mouse down on arrow
        if (params === 1) {
            currentDownFunction(1);
        } else {
            currentDownFunction();
        }
    }).bind('mouseup mouseleave', function () {
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_down2", "#" + currentParameter + "_down", 0, 1);      // mouse up on arrow
    });

    $("#" + currentParameter + "_down2").mousedown(function () {                                      // mouse down and up on select down box
        setButtonCSS("#" + currentParameter + "_down2", "#" + currentParameter + "_down", 0, 0);      // mouse down on box
        if (params === 1) {
            currentDownFunction(1);
        } else {
            currentDownFunction();
        }
    }).bind('mouseup mouseleave', function () {
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_down2", "#" + currentParameter + "_down", 0, 1);       // mouse up on box
    });
}

/* set hover pop-ups */
function setHoverPopUp(currentParameter) {
    "use strict";
    if (currentParameter === "command_queue" || currentParameter === "transaction_queue") {
        $("#" + currentParameter + "_rect").hover(function () {
            timer = setTimeout(function () {showPopup($("#" + currentParameter + "_rect"), "#" + currentParameter + "_pop_up", 34); }, 1000);
        }, function () {
            clearTimeout(timer);
            hidePopup("#" + currentParameter + "_pop_up");
        });
    } else {
        $("#" + currentParameter + "_oval").hover(function () {
            timer = setTimeout(function () {showPopup($("#" + currentParameter + "_oval"), "#" + currentParameter + "_pop_up", 34); }, 1000);
        }, function () {
            clearTimeout(timer);
            hidePopup("#" + currentParameter + "_pop_up");
        });
    }

    $("#" + currentParameter + "_text").hover(function () {                                            // hover functions for select pop-up tip
        timer = setTimeout(function () {showPopup($("#" + currentParameter + "_text"), "#" + currentParameter + "_pop_up", 32); }, 1000);
    },
        function () {
            clearTimeout(timer);
            hidePopup("#" + currentParameter + "_pop_up");
        });
    $("#" + currentParameter).hover(function () {
        timer = setTimeout(function () {showPopup($("#" + currentParameter + "_text"), "#" + currentParameter + "_pop_up", 32); }, 1000);
    },
        function () {
            clearTimeout(timer);
            hidePopup("#" + currentParameter + "_pop_up");
        });
}

/* scale range parameter by one */
function setSelectButtons(currentParameter, fontSize) {
    "use strict";
    $(function () {                             // set initial select label
        var currentSelect = $(formSelectID[currentParameter] + " " + " option:selected").text(),
            scheme = 0;
        if (currentParameter === "trace") {
            if (currentSelect === "mase_art") {
                $("#" + currentParameter).attr("x", 23);
            } else {
                $("#" + currentParameter).attr("x", 20);
            }
        } else if (currentParameter === "row_buffer") {
            if (currentSelect === "open_page") {
                $("#" + currentParameter).attr("x", 501);
            } else {
                $("#" + currentParameter).attr("x", 500);
            }
        } else if (currentParameter === "queuing_structure") {
            if (currentSelect === "per_rank_per_bank") {
                $("#" + currentParameter).attr("x", 475);
            } else {
                $("#" + currentParameter).attr("x", 500);
            }
        } else if (currentParameter === "scheduling_policy") {
            $("#" + currentParameter).attr("x", 455);
        } else if (currentParameter === "address_mapping") {
            scheme = currentSelect.charAt(6);
        }
        currentSelect = currentSelect.toUpperCase();
        currentSelect = currentSelect.replace(/_/g, " ");
        setLabelCSS("#" + currentParameter, "bold", fontSize);
        if (currentParameter === "address_mapping") {
            $("#" + currentParameter).text("SCHEME" + scheme);
        } else {
            $("#" + currentParameter).text(currentSelect);
        }
    });

    function changeSelect() {                      // function to change the select option
        var currentSelect = $(formSelectID[currentParameter] + " " + "option:selected").text();
        if (currentParameter === "trace") {
            if (currentSelect === "mase_art") {
                currentSelect = "K6 AOE 02 SHORT";
                $("#" + currentParameter).attr("x", 23);
            } else {
                currentSelect = "MASE ART";
                $("#" + currentParameter).attr("x", 20);
            }
        } else if (currentParameter === "row_buffer") {
            if (currentSelect === "open_page") {
                currentSelect = "CLOSE PAGE";
                $("#" + currentParameter).attr("x", 500);
            } else {
                currentSelect = "OPEN PAGE";
                $("#" + currentParameter).attr("x", 501);
            }
        } else if (currentParameter === "queuing_structure") {
            if (currentSelect === "per_rank_per_bank") {
                currentSelect = "PER RANK";
                $("#queuing_structure").attr("x", 500);
            } else {
                currentSelect = "PER RANK PER BANK";
                $("#queuing_structure").attr("x", 475);
            }
        }
        if (currentParameter !== "scheduling_policy") {                                       // only one option, rank then bank round robin
            $("#" + currentParameter).text(currentSelect);
            currentSelect = currentSelect.toLowerCase();
            currentSelect = currentSelect.replace(/ /g, "_");
            $(formSelectID[currentParameter] + " " + "option:selected").text(currentSelect);
            updatePopUpTips("#" + currentParameter + "_pop_up", formSelectID[currentParameter], 1);
        }
    }

    function incrementAddressMappingScheme() {                                                 // increment address mapping scheme
        var currentScheme = $(formSelectID.address_mapping + " option:selected").text(),
            scheme = parseInt(currentScheme.charAt(6), 10);
        if (scheme === 7) {
            scheme = 1;
        } else {
            scheme += 1;
        }
        $(formSelectID.address_mapping + " option:selected").text("scheme" + scheme);
        $("#address_mapping").text("SCHEME" + scheme);
        updatePopUpTips("#address_mapping_pop_up", formSelectID.address_mapping, 1);
        if (speed > 1) {
            speed -= 0.8;
        }
        timeoutId = setTimeout(incrementAddressMappingScheme, 200 * speed);
    }

    function decrementAddressMappingScheme() {                                                 // function decrement address mapping scheme
        var currentScheme = $(formSelectID.address_mapping + " option:selected").text(),
            scheme = parseInt(currentScheme.charAt(6), 10);
        if (scheme === 1) {
            scheme = 7;
        } else {
            scheme -= 1;
        }
        $(formSelectID.address_mapping + " option:selected").text("scheme" + scheme);
        $("#address_mapping").text("SCHEME" + scheme);
        updatePopUpTips("#address_mapping_pop_up", formSelectID.address_mapping, 1);
        if (speed > 1) {
            speed -= 0.8;
        }
        timeoutId = setTimeout(decrementAddressMappingScheme, 200 * speed);
    }

    if (currentParameter === "address_mapping") {
        setButtonInteractions(currentParameter, incrementAddressMappingScheme, decrementAddressMappingScheme, 0);       // mouse down and mouse up functions for address mapping scheme
    } else {
        setButtonInteractions(currentParameter, changeSelect, changeSelect, 0);                                         // mouse down and mouse up functions for other select parameters
    }
    setHoverPopUp(currentParameter);
    setButtonHoverFunctions("#" + currentParameter + "_up2", "#" + currentParameter + "_up");                           // set hover functions for select up button
    setButtonHoverFunctions("#" + currentParameter + "_down2", "#" + currentParameter + "_down");                       // set hover functions for select down button
}

/* scale range parameter by one */
function setScaleByPowerTwoButtons(currentParameter, lowerLimit, upperLimit, offset1, offset2) {
    "use strict";
    $(function () {
        var currentValue = parseInt($(formID[currentParameter]).val(), 10),               // get current parameter value from form 
            len = currentValue.toString().length;
        setLabelCSS("#" + currentParameter, "bold", "20px");
        $("#" + currentParameter).attr("x", (offset1 + (len * offset2)));
        $("#" + currentParameter).text(currentValue);
    });

    /* modify current parameter by power of two */
    function modifyByTwo(option) {
        var currentValue = parseInt($(formID[currentParameter]).val(), 10),
            newValue = 0,
            len = 0;
        if (option === 0) {
            newValue = currentValue * 2;
        } else {
            newValue = currentValue / 2;
        }
        if ((option === 0 && newValue <  upperLimit) || (option === 1 && newValue > lowerLimit)) {
            len = newValue.toString().length;
            $(formID[currentParameter]).val(newValue);
            $("#" + currentParameter).text(newValue);
            $("#" + currentParameter).attr("x", (offset1 + (len * offset2)));
            updatePopUpTips("#" + currentParameter + "_pop_up", formID[currentParameter], 0);
            drawDramDevices();
            drawBankArrays();
            drawIOPins();
            drawMUXPins();
            drawDimms(0, calculateTotalDimms());                                           // draw dimms
            if (undoAction === 1) {
                if (currentParameter === "rows" || currentParameter === "columns") {
                    alert("Number of " + currentParameter + " reset to " + currentValue + ".");
                } else {
                    alert(currentParameter + " reset to " + currentValue + ".");
                }
                len = currentValue.toString().length;
                $(formID[currentParameter]).val(currentValue);
                $("#" + currentParameter).text(currentValue);
                $("#" + currentParameter).attr("x", (offset1 + (len * offset2)));
                updatePopUpTips("#" + currentParameter + "_pop_up", formID[currentParameter], 0);
                undoAction = 0;
                drawDramDevices();
                drawBankArrays();
                drawIOPins();
                drawMUXPins();
                drawDimms(1, calculateTotalDimms());                                       // redraw dimms with no alert
            }
        }
    }
    setButtonInteractions(currentParameter, modifyByTwo, modifyByTwo, 1);      /* mouse down and mouse up functions */
    setButtonHoverFunctions("#" + currentParameter + "_up2", "#" + currentParameter + "_up");      /* hover functions for current parameter up button */
    setButtonHoverFunctions("#" + currentParameter + "_down2", "#" + currentParameter + "_down");   /* hover functions for current parameter down button */
    setHoverPopUp(currentParameter);
}

/* scale range parameter by one */
function setInputButtonsRangeOption(currentParameter, offset1, offset2, fontSize) {
    "use strict";
    $(function () {                                                                        // set initial parameter label
        var currentValue = $(formID[currentParameter]).val(),
            len = currentValue.toString().length,
            count = (currentValue.toString().match(/to/g) || []).length;
        setLabelCSS("#" + currentParameter, "bold", fontSize);
        $("#" + currentParameter).attr("x", (offset1 - (len * offset2)));

        if (currentParameter === "transaction_queue") {
            setLabelCSS("#return_transaction_queue", "bold", "20px");                      // set return transaction queue text css
            $("#return_transaction_queue").attr("x", 504 - (len * 6));                     // set return transaction queue text positioning
            $("#return_transaction_queue").text(currentValue + " bits");
        }
        if (currentParameter === "transaction_queue" || currentParameter === "command_queue") {
            $("#" + currentParameter).text(currentValue + " bits");
        } else {
            $("#" + currentParameter).text(currentValue);
        }
        if (count > 0) {
            ranges[currentParameter] = 1;
            $("#" + currentParameter + "_range_text").css('fill', 'red');
        }
    });

    function decrementMemorySize() {                                                        // function to decrement memory size
        if ($(formID.memory_size).val() !== 1) {
            var newSize = 0,
                currentSize = 0,
                highRange = 0,
                len = 0;
            if (ranges.memory_size === 0) {
                currentSize = parseInt($(formID.memory_size).val(), 10);
                newSize = currentSize / 2;
            } else {
                currentSize = parseInt($(formID.memory_size).val(), 10);
                highRange = splitRange(formID.memory_size);
                if (highRange === 0) {
                    highRange = currentSize / 2;
                } else {
                    highRange /= 2;
                }
                if (currentSize >= highRange) {
                    newSize = currentSize;
                    ranges.memory_size = 0;
                    $("#ranges.memory_size_text").css('fill', 'black');
                } else {
                    newSize = currentSize + " to " + highRange;
                }
            }
            if (newSize < 1) {
                newSize = 1;
            }
            len = newSize.toString().length;
            $(formID.memory_size).val(newSize);
            $("#memory_size").text(newSize + " MB");
            $("#memory_size").attr("x", (112 + (len * 0.05)));
            updatePopUpTips("#memory_size_pop_up", formID.memory_size, 0);
            drawDimms(0, calculateTotalDimms());                           /* redraw dimms */
            drawDramDevices();

            if (undoAction === 1) {
                alert("Number of total memory size to reset to " + currentSize + "MB.");
                len = currentSize.toString().length;
                $(formID.memory_size).val(currentSize);
                $("#memory_size").text(currentSize + " MB");
                $("#memory_size").attr("x", (112 + (len * 0.05)));
                updatePopUpTips("#memory_size_pop_up", formID.memory_size, 0);
                undoAction = 0;
                drawDimms(1, calculateTotalDimms());                                       // redraw dimms  with no alert
                drawDramDevices();
            }
        }
    }

    function modifyValueByOneRange(option) {                                               // increment value by 1, range option
        var currentValue = parseInt($(formID[currentParameter]).val(), 10),
            newValue = 0,
            highRange = 0,
            len = 0;
        if (ranges[currentParameter] === 0) {
            currentValue = parseInt($(formID[currentParameter]).val(), 10);
            if (option === 0) {
                newValue = currentValue + 1;
            } else {
                newValue = currentValue - 1;
            }
        } else {
            currentValue = parseInt($(formID[currentParameter]).val(), 10);
            highRange = splitRange(formID[currentParameter]);
            if (highRange === 'undefined') {
                if (option === 0) {
                    highRange = currentValue + 1;
                } else {
                    highRange = currentValue - 1;
                }
            } else {
                if (option === 0) {
                    highRange += 1;
                } else {
                    highRange -= 1;
                }
            }
            if (currentValue >= highRange) {
                newValue = currentValue;
                ranges[currentParameter] = 0;
                $("#" + currentParameter + "_range_text").css('fill', 'black');
            } else {
                newValue = currentValue + " to " + highRange;
            }
        }
        if (newValue < 1) {
            newValue = 1;
        }
        len = newValue.toString().length;
        $(formID[currentParameter]).val(newValue);
        if (currentParameter === "command_queue" || currentParameter === "transaction_queue") {
            $("#" + currentParameter).text(newValue + " bits");
        } else {
            $("#" + currentParameter).text(newValue);
        }
        $("#" + currentParameter).attr("x", (offset1 - (len * offset2)));
        if (currentParameter === "transaction_queue") {
            $("#return_transaction_queue").attr("x", 504 - (len * 6));                 // set return transaction queue text positioning
            $("#return_transaction_queue").text(newValue + " bits");
        }
        updatePopUpTips("#" + currentParameter + "_pop_up", formID[currentParameter], 0);
        if (speed > 1) {
            speed -= 0.8;
        }
        if (option === 1) {
            if ($(formID[currentParameter]).val() !== 1) {
                timeoutId = setTimeout(modifyValueByOneRange, 200 * speed, option);
            }
        } else {
            timeoutId = setTimeout(modifyValueByOneRange, 200 * speed, option);
        }
    }
    if (currentParameter === "memory_size") {
        setButtonInteractions(currentParameter, incrementMemorySize, decrementMemorySize, 1);          // mouse down and mouse up functions
    } else {
        setButtonInteractions(currentParameter, modifyValueByOneRange, modifyValueByOneRange, 1);      // mouse down and mouse up functions
    }
    setButtonHoverFunctions("#" + currentParameter + "_up2", "#" + currentParameter + "_up");          // hover functions for current parameter up button
    setButtonHoverFunctions("#" + currentParameter + "_down2", "#" + currentParameter + "_down");      // hover functions for current parameter down button
    setHoverPopUp(currentParameter);

    $("#" + currentParameter + "_range").mousedown(function () {                                       // mouse down function for current parameter range box
        setButtonCSS("#" + currentParameter + "_range", "#" + currentParameter + "_range_text", 0, 0);
        if (ranges[currentParameter] === 0) {
            ranges[currentParameter] = 1;
            $("#" + currentParameter + "_range_text").css('fill', 'red');
        } else {
            ranges[currentParameter] = 0;
            $("#" + currentParameter + "_range_text").css('fill', 'black');
        }
    }).bind('mouseup mouseleave', function () {                                                         // mouse up function for current parameter range box
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_range", "#" + currentParameter + "_range_text", 0, 1);  // mouse up on box
    });

    $("#" + currentParameter + "_range_text").mousedown(function () {                                   // mouse down function for current parameter range text
        setButtonCSS("#" + currentParameter + "_range", "#" + currentParameter + "_range_text", 0, 0);  // mouse down on text
        if (ranges[currentParameter] === 0) {
            ranges[currentParameter] = 1;
            $("#" + currentParameter + "_range_text").css('fill', 'red');
        } else {
            ranges[currentParameter] = 0;
            $("#" + currentParameter + "_range_text").css('fill', 'black');
        }
    }).bind('mouseup mouseleave', function () {                                                          // mouse up function for current parameter range text
        clearTimeout(timeoutId);
        speed = 3;
        setButtonCSS("#" + currentParameter + "_range", "#" + currentParameter + "_range_text", 0, 1);   // mouse up on text
    });

    setButtonHoverFunctions("#" + currentParameter + "_range", "#" + currentParameter + "_range_text");   // hover functions for current parameter range button
}

function setInputButtonsNoRangeOption(currentParameter) {
    "use strict";

    $(function () {                                                                                        //set input label
        var currentValue = $(formID[currentParameter]).val();
        setLabelCSS("#" + currentParameter, "bold", "20px");
        $("#" + currentParameter).attr("x", 409.89615);
        $("#" + currentParameter).text(currentValue);
    });

    /* modify value by one */
    function modifyValueByOneNoRange(option) {
        var currentValue = parseInt($(formID[currentParameter]).val(), 10);
        if (option === 0) {
            currentValue += 1;
        } else if (currentValue !== 1) {
            currentValue -= 1;
        }

        $(formID[currentParameter]).val(currentValue);
        $("#" + currentParameter).text(currentValue);
        $("#" + currentParameter).attr("x", 409.89615);
        updatePopUpTips("#" + currentParameter + "_pop_up", formID[currentParameter], 0);
        if (speed > 1) {
            speed -= 0.8;
        }
        timeoutId = setTimeout(modifyValueByOneNoRange, 200 * speed, option);
    }
    setButtonInteractions(currentParameter, modifyValueByOneNoRange, modifyValueByOneNoRange, 1);          // mouse down and mouse up functions
    setButtonHoverFunctions("#" + currentParameter + "_up2", "#" + currentParameter + "_up");              // hover functions for current parameter up button
    setButtonHoverFunctions("#" + currentParameter + "_down2", "#" + currentParameter + "_down");          // hover functions for current parameter down button 
    setHoverPopUp(currentParameter);
}

/* create a pop-up tip for current element */
function createPopUpTips(parameterKey, popUpTable, currentInput, type) {
    "use strict";
    var newPopUpTable = document.createElement("table"),
        currentDiv  = document.createElement("div"),
        hp = 0,
        currentValue = 0,
        data = 0,
        rows = 0,
        i = 0;
    newPopUpTable.setAttribute('id', popUpTable);
    newPopUpTable.setAttribute('bgcolor', '#0000FF');
    newPopUpTable.style.visibility = 'hidden';
    newPopUpTable.style.position = 'absolute';
    newPopUpTable.style.top = '0';
    newPopUpTable.style.left = '0';
    // add the newly created element and its content into the DOM
    currentDiv = document.getElementsByClassName("pop-ups-and-inputs")[0];
    currentDiv.appendChild(newPopUpTable);
    hp = $("#" + popUpTable);

    if (type === 0) {
        currentValue = $(currentInput).val();
        data = $(currentInput).next().html();
    } else {
        currentValue = $(currentInput + " option:selected").text();
        data = $(currentInput).parent().next().html();
        if (data === "") {
            data = "No description available ";
        }
    }
    parameterKey = parameterKey.toUpperCase();
    parameterKey = parameterKey.replace("_", " ");
    if (parameterKey === "TRANSACTION QUEUE") {
        parameterKey = "TRANSACTION QUEUE DEPTH";
    } else if (parameterKey === "COMMAND QUEUE") {
        parameterKey = "COMMAND QUEUE DEPTH";
    } else if (parameterKey === "ADDRESS MAPPING") {
        parameterKey = "ADDRESS MAPPING SCHEME";
    }
    hp.append('<tr><td><font color="#FFFFFF">' + parameterKey + '</font></td></tr>');
    rows = data.match(/.{1,50}\s/g);
    for (i = 0; i < rows.length - 1; i += 1) {
        hp.append('<tr><td bgcolor="#8888FF"><p>' + rows[i] + '</p></td></tr>');
    }
    $("#" + popUpTable + " td").css({
        "font-size": "10px",
        "font-weight": "bold",
        "width": "285px",
        "text-align": "center"
    });
    $("#" + popUpTable + " td:first").append(' = ' + currentValue);                                              // set the value
    $("#" + popUpTable + " td:first").css({
        "text-align": "center",
        "color": "white"
    });
    $("#" + popUpTable).css({
        "padding-below": "3px"
    });

    $("#" + popUpTable + " p").css({
        "font-size": "10px",
        "font-weight": "bold"
    });
}

/* create all pop up tips */
$(function () {
    "use strict";
    $.each(formID, function (key) {
        createPopUpTips(key, key + "_pop_up", formID[key], 0);
    });
    $.each(formSelectID, function (key) {
        createPopUpTips(key, key + "_pop_up", formSelectID[key], 1);
    });
});

/* load top section svg file into page */
$(function () {
    "use strict";
    d3.xml("/images/occam_dramsim2_bus_and_channel_control.svg", "image/svg+xml", function (xml) {

        var importedNode = document.importNode(xml.documentElement, true);
        timeoutId = 0;           /* timeout id for click and hold */
        speed = 3;               /* click and hold speed */
        timer = 0;               /*  timer for click and hold */
        $(importedNode).css({
            "display": "block"
        });
        d3.select(".bus-and-channel-controls").node().appendChild(importedNode);
        drawBankArrays();       /* draw number of bank arrays equal to device width */
        drawIOPins();           /* draw number of io pins equal to device width */
        drawMUXPins();          /* draw number of mux pins equal to device width per bank */

        /* data bus bit size functions */
        $(function () {                                  /*  set initial data bus bit size label */
            var currentBusSize = $(formID.bus).val(),    /* get data bus bit size from form */
                len = currentBusSize.toString().length;  /* get number of characters */
            setLabelCSS("#bus", "bold", "20px");           /* set data bus label css */
            $("#bus").attr("x", 120 - len);         /* align label based on number of characters */
            $("#bus").text(currentBusSize + " bits");    /* set data bus label */
        });

        /* decrement data bus bit size */
        function decrementDataBusSize() {
            var currentBusSize = 0,
                newBusSize = 0,
                oldRange = 0,
                highRange = 0,
                len = 0;
            if (ganged_channels === 1) {
                if ($(formID.bus).val() !== 64 && !($(formID.bus).val() === 128 && $(formID.device_width).val() === 4)) {
                    if (ranges.data_bus_range === 0) {
                        oldRange = $(formID.bus).val();
                        currentBusSize = parseInt($(formID.bus).val(), 10);
                        newBusSize = currentBusSize / 2;
                    } else {
                        oldRange = $(formID.bus).val();
                        currentBusSize = parseInt($(formID.bus).val(), 10);
                        highRange = splitRange(formID.bus);
                        if (highRange === 'undefined') {
                            highRange = currentBusSize / 2;
                        } else {
                            highRange /= 2;
                        }
                        if (currentBusSize >= highRange) {
                            newBusSize = currentBusSize;
                            ranges.data_bus_range = 0;
                            $("#data_bus_range_text").css('fill', 'black');
                        } else {
                            newBusSize = currentBusSize + " to " + highRange;
                        }
                    }
                    if (newBusSize < 64) {
                        newBusSize = 64;
                    }
                    len = newBusSize.toString().length;
                    $(formID.bus).val(newBusSize);
                    currentBusSize = parseInt($(formID.bus).val(), 10);
                    highRange = splitRange(formID.bus);
                    if (highRange === 'undefined') {
                        highRange = currentBusSize;
                    }
                    $("#bus").text(newBusSize + " bits");
                    $("#bus").attr("x", (120 - len));
                    $("#bus2").attr("x", 36  - len);
                    $("#bus2").text(highRange + " bits");

                    updatePopUpTips("#bus_pop_up", formID.bus, 0);
                    drawDimms(0, calculateTotalDimms());

                    if (undoAction === 1) {
                        alert("Data bus bit size reset to " + oldRange + ".");
                        len = oldRange.toString().length;
                        $(formID.bus).val(oldRange);
                        $("#bus").text(oldRange + " bits");
                        $("#bus").attr("x", 120 - len);
                        $("#bus2").attr("x", 36  - len);
                        if (highRange !== 'undefined') {
                            $("#bus2").text(highRange * 2 + " bits");
                        } else {
                            $("#bus2").text(oldRange + " bits");
                        }
                        updatePopUpTips("#bus_pop_up", formID.bus, 0);
                        undoAction = 0;
                        drawDimms(1, calculateTotalDimms());                                          // redraw dimms with no alert no memory increment
                    }
                }
            } else {
                if ($(formID.channels).val() !== 1 && !($(formID.channels).val() === 2 && $(formID.device_width).val() === 4)) {
                    if (ranges.data_bus_range === 0) {
                        oldRange = $(formID.channels).val();
                        currentBusSize = parseInt($(formID.channels).val(), 10);
                        newBusSize = currentBusSize / 2;
                    } else {
                        oldRange = $(formID.channels).val();
                        currentBusSize = parseInt($(formID.channels).val(), 10);
                        highRange = splitRange(formID.channels);
                        if (highRange === 'undefined') {
                            highRange = currentBusSize / 2;
                        } else {
                            highRange /= 2;
                        }
                        if (currentBusSize >= highRange) {
                            newBusSize = currentBusSize;
                            ranges.data_bus_range = 0;
                            $("#data_bus_range_text").css('fill', 'black');
                        } else {
                            newBusSize = currentBusSize + " to " + highRange;
                        }
                    }
                    if (newBusSize < 1) {
                        newBusSize = 1;
                    }
                    len = newBusSize.toString().length;
                    $(formID.channels).val(newBusSize);
                    currentBusSize = parseInt($(formID.channels).val(), 10);
                    highRange = splitRange(formID.channels);
                    if (highRange === 'undefined') {
                        highRange = currentBusSize;
                    }
                    $("#bus").text(newBusSize);
                    $("#bus").attr("x", 120 - len);
                    $("#bus2").attr("x", 36  - len);
                    updatePopUpTips("#bus_pop_up", formID.channels, 0);
                    drawDimms(0, calculateTotalDimms());

                    if (undoAction === 1) {
                        alert("Data bus bit size reset to " + oldRange + ".");
                        len = oldRange.toString().length;
                        $(formID.channels).val(oldRange);
                        $("#bus").text(oldRange);
                        $("#bus").attr("x", 120 - len);
                        $("#bus2").attr("x", 36  - len);
                        if (highRange !== 'undefined') {
                            $("#bus2").text(highRange * 2);
                        } else {
                            $("#bus2").text(oldRange);
                        }
                        updatePopUpTips("#bus_pop_up", formID.channels, 0);
                        undoAction = 0;
                        drawDimms(1, calculateTotalDimms());                                         // redraw dimms with no alert no memory increment
                    }
                }
            }
            if (newBusSize === 1 || newBusSize === 64) {
                ganged_channels = 1;
                $("#ganged_channels_text").attr("x", 635);
                $("#bus_text").text("DATA BUS BITSIZE");
                $(formID.bus).val("64");
                $(formID.channels).val("1");
                $("#bus").text("64" + " bits");
                $("#bus2").text("64" + " bits");
                $("#ganged_channels_text").text("GANGED CHANNELS");
            }
        }

        setHoverPopUp("bus");

        $("#bus_up").mousedown(function () {                                                         // mouse down and up functions for data bus up arrow
            var currentBusSize = parseInt($(formID.bus).val(), 10),
                busHighRange = splitRange(formID.bus),
                currentChannels = parseInt($(formID.channels).val(), 10),
                channelHighRange = splitRange(formID.channels);

            if (busHighRange === 'undefined' || ranges.data_bus_range === 0) {
                busHighRange = currentBusSize;
            }

            if (channelHighRange === 'undefined' || ranges.data_bus_range === 0) {
                channelHighRange = currentChannels;
            }
            setButtonCSS("#bus_up2", "#bus_up", 0, 0);                                               // mouse down on arrow
            if (busHighRange !== 512 && channelHighRange !== 8) {
                incrementDataBusSize(0);
            }
        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#bus_up2", "#bus_up", 0, 1);                                               // mouse up on arrow
        });

        /* mouse down and up functions for data bus up box */
        $("#bus_up2").mousedown(function () {
            var currentBusSize = parseInt($(formID.bus).val(), 10),
                busHighRange = splitRange(formID.bus),
                currentChannels = parseInt($(formID.channels).val(), 10),
                channelHighRange = splitRange(formID.channels);
            if (busHighRange === 'undefined' || ranges.data_bus_range === 0) {
                busHighRange = currentBusSize;
            }
            if (channelHighRange === 'undefined' || ranges.data_bus_range === 0) {
                channelHighRange = currentChannels;
            }
            setButtonCSS("#bus_up2", "#bus_up", 0, 0);                       // mouse down on box
            if (busHighRange !== 512 && channelHighRange !== 8) {
                incrementDataBusSize(0);
            }
        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#bus_up2", "#bus_up", 0, 1);                       // mouse up on box
        });

        setButtonHoverFunctions("#bus_up2", "#bus_up");                      // hover functions for data bus up button


        $("#bus_down").mousedown(function () {                               // mouse down and up functions for data bus down arrow
            setButtonCSS("#bus_down2", "#bus_down", 0, 0);                   // mouse down on arrow 
            if (ganged_channels === 1) {
                if ($(formID.bus).val() !== 64 && !($(formID.bus).val() === 128 && $(formID.device_width).val() === 4)) {
                    decrementDataBusSize();
                }
            } else {
                if ($(formID.channels).val() !== 1 && !($(formID.channels).val() === 2 && $(formID.device_width).val() === 4)) {
                    decrementDataBusSize();
                }
            }
        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#bus_down2", "#bus_down", 0, 1);                    // mouse up on arrow
        });

        /* mouse down and up functions for data bus down box */
        $("#bus_down2").mousedown(function () {
            setButtonCSS("#bus_down2", "#bus_down", 0, 0);                     // mouse down on box
            if (ganged_channels === 1) {
                if ($(formID.bus).val() !== 64 && !($(formID.bus).val() === 128 && $(formID.device_width).val() === 4)) {
                    decrementDataBusSize();
                }
            } else {
                if ($(formID.channels).val() !== 1 && !($(formID.channels).val() === 2 && $(formID.device_width).val() === 4)) {
                    decrementDataBusSize();
                }
            }

        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#bus_down2", "#bus_down", 0, 1);                     // mouse up on box
        });

        setButtonHoverFunctions("#bus_down2", "#bus_down");                    // hover functions for data bus down button

        $("#data_bus_range").mousedown(function () {                           // mouse down and up functions for data bus range box
            setButtonCSS("#data_bus_range", "#data_bus_range_text", 0, 0);     // mouse down on box
            if (ranges.data_bus_range === 0) {
                ranges.data_bus_range = 1;
                $("#data_bus_range_text").css('fill', 'red');
            } else {
                ranges.data_bus_range = 0;
                $("#data_bus_range_text").css('fill', 'black');
            }
        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#data_bus_range", "#data_bus_range_text", 0, 1);     // mouse up on box
        });

        $("#data_bus_range_text").mousedown(function () {                      // mouse down and up functions for data bus range text
            setButtonCSS("#data_bus_range", "#data_bus_range_text", 0, 0);     // mouse down on box
            if (ranges.data_bus_range === 0) {
                ranges.data_bus_range = 1;
                $("#data_bus_range_text").css('fill', 'red');
            } else {
                ranges.data_bus_range = 0;
                $("#data_bus_range_text").css('fill', 'black');
            }
        }).bind('mouseup mouseleave', function () {
            setButtonCSS("#data_bus_range", "#data_bus_range_text", 0, 1);     // mouse up on box
        });

        setButtonHoverFunctions("#data_bus_range", "#data_bus_range_text");    // hover functions for data bus range button

        setInputButtonsRangeOption("memory_size", 112, 0.05, "20px");
        setScaleByPowerTwoButtons("rows", 1, 1000000, 75, 2);
        setScaleByPowerTwoButtons("columns", 1, 1000000, 105, 2);
        setScaleByPowerTwoButtons("banks", 2, 32, 777.5708, -4);
        setScaleByPowerTwoButtons("device_width", 2, 32, 77, 2);

        $(function () {                                                        // ganged vs. unganged channels
            var currentSize = getDataBusSizeOrChannels(0),
                currentChannels = 0;
            setGangedChannels();
            setLabelCSS("#ganged_channels_text", "normal", "14px");
            setLabelCSS("#bus_text", "normal", "14px");
            if (ganged_channels === 1) {
                $("#bus_text").text("DATA BUS BITSIZE");
                $("#bus").text(currentSize + " bits");
                $("#ganged_channels_text").attr("x", 634);
                $("#ganged_channels_text").text("GANGED CHANNELS");
            } else {
                currentChannels = $(formID.channels).val();
                $("#bus_text").text("NUMBER OF CHANNELS");
                $("#bus").text(currentChannels);
                $("#ganged_channels_text").attr("x", 635);
                $("#ganged_channels_text").text("UNGANGED CHANNELS");
            }
        });

        function modifyChannels() {                                             // modify channels - ganged or unganged
            var currentSize = 0,
                newSize = 0,
                highRange = 0;

            if ((parseInt($(formID.bus).val(), 10) * parseInt($(formID.channels).val(), 10)) === 64) {
                ganged_channels = 1;
                $("#ganged_channels_text").attr("x", 634);
                $("#bus_text").text("DATA BUS BITSIZS");
                $(formID.channels).val("1");
                $(formID.bus).val("64");
                $("#bus").text("64 bits");
                $("#bus2").text("64 bits");
                $("#ganged_channels_text").text("GANGED CHANNELS");
            } else if (ganged_channels === 1) {
                ganged_channels = 0;
                currentSize = parseInt($(formID.bus).val(), 10);
                highRange = splitRange(formID.bus);
                if (highRange === 'undefined') {
                    newSize = (currentSize / 64) + " to " + (highRange / 64);
                } else {
                    newSize = currentSize / 64;
                }
                if ((parseInt($(formID.bus).val(), 10) * parseInt($(formID.channels).val(), 10)) !== 64) {
                    $("#ganged_channels_text").attr("x", 634);
                    $("#bus_text").text("NUMBER OF CHANNELS");
                    $(formID.channels).val(newSize);
                    $(formID.bus).val("64");
                    $("#bus").text(newSize);
                    $("#bus2").text("64 bits");
                    $("#ganged_channels_text").text("UNGANGED CHANNELS");
                } else {
                    ganged_channels = 1;
                    $("#ganged_channels_text").attr("x", 634);
                    $("#bus_text").text("DATA BUS BITSIZS");
                    $(formID.channels).val("1");
                    $(formID.bus).val("64");
                    $("#bus").text("64 bits");
                    $("#bus2").text("64 bits");
                    $("#ganged_channels_text").text("GANGED CHANNELS");
                }
            } else {
                ganged_channels = 1;
                currentSize = parseInt($(formID.channels).val(), 10);
                highRange = splitRange(formID.channels);
                if (highRange === 'undefined') {
                    newSize = (currentSize * 64) + " to " + (highRange * 64);
                } else {
                    newSize = currentSize * 64;
                }
                $("#ganged_channels_text").attr("x", 635);
                $("#bus_text").text("DATA BUS BITSIZE");
                $(formID.bus).val(newSize);
                $(formID.channels).val("1");
                $("#bus").text(newSize + " bits");
                $("#bus2").text(newSize + " bits");
                $("#ganged_channels_text").text("GANGED CHANNELS");
            }
            drawDimms(0, calculateTotalDimms());
            drawMemoryControllers();

        //  updatePopUpTips("#ganged_channels_pop_up",formID['refresh_period'], 0);
        }
        setButtonInteractions("ganged_channels", modifyChannels, modifyChannels, 0);      // mouse down and mouse up functions
        setButtonHoverFunctions("#ganged_channels_up2", "#ganged_channels_up");           // hover functions for current parameter up button
        setButtonHoverFunctions("#ganged_channels_down2", "#ganged_channels_down");       // hover functions for current parameter down button
    });
});

/* load middle section svg file into page - memory controller and first dimm */
$(function () {
    "use strict";
    d3.xml("/images/occam_dramsim2_draft.svg", "image/svg+xml", function (xml) {
        var importedNode = document.importNode(xml.documentElement, true),
            lowRange = 0,
            highRange = 0,
            len = 0;
        timeoutId = 0;                                /* timeout id for click and hold */
        speed = 3;                                    /* click and hold speed */
        timer = 0;                                    /* timer for click and hold */
        $(importedNode).css({
            "display": "block"
        });
        d3.select(".viz").node().appendChild(importedNode);
        drawDramDevices();
        setInputButtonsRangeOption("transaction_queue", 231, 6, "20px");      // set transaction queue buttons
        setInputButtonsRangeOption("command_queue", 231, 6, "20px");          // set command queue buttons 

        $(function () {                                                       // function to set data bus label
            lowRange = parseInt($(formID.bus).val(), 10);
            highRange = splitRange(formID.bus);
            if (highRange === 'undefined') {
                highRange = lowRange;
            }
            len = highRange.toString().length;
            setLabelCSS("#bus2", "bold", "18px");
            $("#bus2").attr("x", 36 - len);
            $("#bus2").text(highRange + " bits");
        });

        setSelectButtons("address_mapping", "20px");                          // set address mapping scheme buttons
        setSelectButtons("scheduling_policy", "8px");                         // set scheduling policy buttons
        setSelectButtons("queuing_structure", "9px");                         // set queuing structure buttons
        setSelectButtons("row_buffer", "9px");                                // set row buffer buttons
        setInputButtonsRangeOption("row_accesses", 525, 3, "9px");            // set total row accesses buttons
        setInputButtonsRangeOption("cycle_count", 525, 3, "9px");             // set cycle count buttons
        setInputButtonsRangeOption("epoch_length", 525, 3, "9px");            // set  epoch length buttons
        drawDimms(0, calculateTotalDimms());                                  // initially draw the correct number of dimms
    });
});

/* load bottom section svg file into page - CPU */
$(function () {
    "use strict";
    d3.xml("/images/occam_dramsim2_ganged_cpu.svg", "image/svg+xml", function (xml) {
        var importedNode = document.importNode(xml.documentElement, true);
        timeoutId = 0;
        speed = 30;
        timer = 0;
        $(importedNode).css({
            "display": "block"
        });
        d3.select(".cpu").node().appendChild(importedNode);
        setSelectButtons("trace", "13px");
    });
});

/* load dram device timing constraint graph svg file into page */
$(function () {
    "use strict";
    d3.xml("/images/occam_dramsim2_ddr_timing_chart.svg", "image/svg+xml", function (xml) {
        var importedNode = document.importNode(xml.documentElement, true),
            dataMax = 250,
            data = 0,
            barW = 40,
            i = 0,
            textID = 0,
            splitTextID = 0,
            parameterKey = 0,
            randomArray = [],
            panelSVG = 0;
        timeoutId = 0;
        speed = 30;
        timer = 0;
        d3.select(".timing-chart").node().appendChild(importedNode);

        function generateData(num) {
            for (i = 0; i < num; i += 1) {
                randomArray.push(Math.random() * dataMax);
            }
            return randomArray;
        }
        data = generateData(19);

        panelSVG = d3.select("#timing_chart");

        panelSVG.selectAll("rect.timing_bar")
            .data(data)
            .enter().append("svg:rect")
            .attr("class", "timing_bar")
            .attr("width", barW)
            .attr("fill", "#5599FF")
            .attr("stroke", "black");

        $('.timing_parameters').each(function () {                               // set the form input for the current graph parameter
            textID = $(this).attr('id');
            splitTextID = textID.split("_");
            parameterKey = "";
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            if (parameterKey === "tck") {
                tabIndex = 1;
            } else if (parameterKey === "cl") {
                tabIndex = 2;
            } else if (parameterKey === "al") {
                tabIndex = 3;
            } else if (parameterKey === "cl") {
                tabIndex = 2;
            } else if (parameterKey === "bl") {
                tabIndex = 4;
            } else if (parameterKey === "trp") {
                tabIndex = 9;
            } else if (parameterKey === "tccd") {
                tabIndex = 10;
            } else if (parameterKey === "twr") {
                tabIndex = 12;
            } else if (parameterKey === "trtrs") {
                tabIndex = 13;
            } else if (parameterKey === "trfc") {
                tabIndex = 14;
            } else if (parameterKey === "tfaw") {
                tabIndex = 15;
            }
            createGraphParameterFormInput(formID[parameterKey], parameterKey + "_input", parameterKey, 0, tabIndex);
            tabIndex += 1;
        });

        setAllGraphParameterFormInputs($(".pop-ups-and-inputs").offset().top - 600, 0);

        $('.timing_parameters').each(function () {
            textID = $(this).attr('id');
            splitTextID = textID.split("_");
            parameterKey = "";
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            setGraphButton(parameterKey, 0, formID[parameterKey], "#" + parameterKey + "_input", "#" + parameterKey + "_bar", "#" + parameterKey + "_pop_up", "#" + parameterKey + "_text", "#" + parameterKey + "_up2", "#" + parameterKey + "_up", "#" + parameterKey + "_down2", "#" + parameterKey + "_down", 3196);
        });

        $('.plus_one_parameters').each(function () {                       // only loads refresh period buttons
            textID = $(this).attr('id');
            splitTextID = textID.split("_");
            parameterKey = "";
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            setInputButtonsNoRangeOption(parameterKey);
        });
    });
});

/* load dram device power consumption graph svg file into page */
$(function () {
    "use strict";
    d3.xml("/images/occam_dramsim2_ddr_power_chart.svg", "image/svg+xml", function (xml) {
        var importedNode = document.importNode(xml.documentElement, true),
            dataMax = 250,
            data = 0,
            barW = 40,
            i = 0,
            textID = 0,
            splitTextID = 0,
            parameterKey = 0,
            randomArray = [],
            panelSVG = 0;
        timeoutId = 0;
        speed = 30;
        timer = 0;
        d3.select(".power-chart").node().appendChild(importedNode);

        function generateData(num) {
            for (i = 0; i < num; i += 1) {
                randomArray.push(Math.random() * dataMax);
            }
            return randomArray;
        }
        data = generateData(15);
        panelSVG = d3.select("#power_chart");

        panelSVG.selectAll("rect.power_bar")
            .data(data)
            .enter().append("svg:rect")
            .attr("class", "timing_bar")
            .attr("width", barW)
            .attr("fill", "#5599FF")
            .attr("stroke", "black");

        $('.power_parameters').each(function () {              // set the form input for the current graph parameter
            textID = $(this).attr('id');
            splitTextID = textID.split("_");
            parameterKey = "";
            for (i = 0; i < splitTextID.length - 1; i += 1) {
                if (i > 0) {
                    parameterKey += "_";
                }
                parameterKey += splitTextID[i];
            }
            if (parameterKey === "idd0") {
                tabIndex = 19;
            } else if (parameterKey === "idd1") {
                tabIndex = 20;
            } else if (parameterKey === "idd2p") {
                tabIndex = 21;
            } else if (parameterKey === "idd2q") {
                tabIndex = 22;
            } else if (parameterKey === "idd4w") {
                tabIndex = 27;
            } else if (parameterKey === "idd4r") {
                tabIndex = 28;
            } else if (parameterKey === "idd6l") {
                tabIndex = 31;
            } else if (parameterKey === "idd7") {
                tabIndex = 32;
            }
            createGraphParameterFormInput(formID[parameterKey], parameterKey + "_input", parameterKey, 1, tabIndex);
            tabIndex += 1;
        });

        setAllGraphParameterFormInputs($(".pop-ups-and-inputs").offset().top - 87, 1);

        $('.power_parameters').each(function () {
            textID = $(this).attr('id');
            splitTextID = textID.split("_");
            parameterKey = splitTextID[0];
            setGraphButton(parameterKey, 1, formID[parameterKey], "#" + parameterKey + "_input", "#" + parameterKey + "_bar", "#" + parameterKey + "_pop_up", "#" + parameterKey + "_text", "#" + parameterKey + "_up2", "#" + parameterKey + "_up", "#" + parameterKey + "_down2", "#" + parameterKey + "_down", 3709);
        });
    });
});
