function init_expands() {
  // Displays values only when enabled
  $('.configuration-group li > .select > select').on('change', function(e) {
    var key = $(this).data("enables-key");
    var is = $(this).data("enables-is");
    var input = $(this).children("option:selected").text();
    if(is === input) { 
      $('.configuration-group ul[data-key='+key+']').parent().css({
        display:''
      });
    }
    else { 
      $('.configuration-group ul[data-key='+key+']').parent().css({
        display:'none'
      });
    }
  });

  // Set up div expands
  $('.configuration-group li > label').on('click', function(e) {
    $(this).parent().children('span.expand').trigger('click');
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group li > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated description div
    if ($(this).hasClass('shown')) {
      $(this).parent().find('.description').css({
        display: 'block'
      });
      $(this).text("[ - ]");
    }
    else {
      $(this).parent().find('.description').css({
        display: 'none'
      });
      $(this).text("[+]");
    }
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group h2').on('click', function(e) {
    var span = $(this).children('span.expand');
    span.toggleClass('shown');
    // Get associated description div
    if (span.hasClass('shown')) {
      span.parent().parent().children('ul').css({
        display: 'block'
      });
      span.text("\u25be");
    }
    else {
      span.parent().parent().children('ul').css({
        display: 'none'
      });
      span.text("\u25b8");
    }
  }).css({
    cursor: 'pointer'
  });

  $('.configuration-group h2 > span.expand').on('click', function(e) {
  }).css({
    cursor: 'pointer'
  });

  // Collapse all but the first group
  $('ul.configuration-group > li:not(:first-child) > ul.configuration-group').each(function(e) {
    $(this).parent().children('h2').children('span.expand').trigger('click');
  });

  // collapse all / expand all
  $('ul.header_bar').css({display: 'block'});
  $('ul.header_bar #collapse_all_link').on('click', function(e) {
    $('ul.configuration-group h2 > span.expand').each(function(e) {
      if ($(this).hasClass('shown')) {
        $(this).toggleClass('shown');
        $(this).parent().parent().children('ul').css({
          display: 'none'
        });
        $(this).text("\u25b8");
      }
    });
  });
  $('ul.header_bar #expand_all_link').on('click', function(e) {
    $('ul.configuration-group h2 > span.expand').each(function(e) {
      if (!($(this).hasClass('shown'))) {
        $(this).toggleClass('shown');
        $(this).parent().parent().children('ul').css({
          display: 'block'
        });
        $(this).text("\u25be");
      }
    });
  });
}

// Autocomplete for experiment tags
$(function(){
  var tag_cache = {};
  $('input.tagged').tagit({
    allowSpaces: true,
    singleField: true,
    singleFieldDelimiter: ';'
  });
  $('input.tagged.autocomplete').tagit({
    singleField: true,
    singleFieldDelimiter: ';',
    autocomplete: {
      delay: 0,
      minLength: 2,
      source: function(request, response) {
        var term = request.term;
        if (term in tag_cache) {
          response(tag_cache[term]);
          return;
        }

        $.getJSON($('input.tagged.autocomplete').data("source"), {term: term}, function(data, status, xhr) {
          tag_cache[term] = data;
          response(data);
          return;
        });
      },
    }
  });
  $('#search').searchlight('/search', {
    showIcons: false,
    align: 'left'
  });

  // Configuration expands
  init_expands();

  $('.configuration-group > li > .recipe > select').on('change', function(e) {
    var selected = $(this).children(':selected');
    var ul_group = $(this).parent().parent().children('ul.configuration-group');
    var expand = $(this).parent().parent().children('h2').children('span.expand');
    var nesting = ul_group.data('nesting');

    // Expand group
    if (!expand.hasClass('shown')) {
      expand.trigger('click');
    }

    // Ask for the config options
    jQuery.getJSON(selected.data('template'), function(data, status, xhr) {
      // Set all input fields
      $.each(data, function(key, value) {
        code = $.base64.encode(key);
        // Replace + / with - _
        code = code.replace('+', '-').replace('/', '_');
        // Remove padding
        while (code.charAt(code.length-1) == '=') {
          code = code.slice(0, code.length-1);
        }
        code = nesting + '[' + code + ']';

        $('*[name="'+code+'"]').each(function(e) {
          $(this).val(value);
        });
      });
    });
  });

  // add prettyprint class to all <pre><code></code></pre> blocks
  var prettify = false;
  $("pre code").parent().each(function() {
    $(this).addClass('prettyprint');
    prettify = true;
  });

  // if code blocks were found, bring in the prettifier ...
  if ( prettify ) {
    $.getScript("/js/prettify.js", function() { prettyPrint() });
  }
});
