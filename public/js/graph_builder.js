// TODO: UGH
var graph = $('#graph');
var type = "bars";
var occam = new OccamViz();

var options = {
  selector: '#'+graph.attr('id'),
  labels: {
  },
  data: {
    groups: []
  }
};

/* Returns urlsafe base64 encoding
 */
function base64_urlencode(key) {
  var code = $.base64.encode(key);
  // Replace + / with - _
  code = code.replace('+', '-').replace('/', '_');
  // Remove padding
  while (code.charAt(code.length-1) == '=') {
    code = code.slice(0, code.length-1);
  }

  return code;
}

/* Initializes the context menu object that displays options for each key such
 * as 'Add to X' for graph building.
 */
var menu;
function initializeContextMenu() {
  menu = $('<div id="graph-builder-menu"><ul></ul></div>').css({
    display: 'none',
    position: 'absolute',
    "z-index": '999'
  }).on('mousedown', function(event) {
    event.stopPropagation();
  });

  menu.children('ul').append("<li id='graph-builder-menu-select'>Select</li>");
  menu.children('ul').append("<li id='graph-builder-menu-select-across'>Select Across</li>");
  menu.children('ul').append("<li id='graph-builder-menu-add-x'>Add X Series</li>");
  menu.children('ul').append("<li id='graph-builder-menu-label-x'>Label X</li>");

  $('body').on('mousedown', function(event) {
    menu.css({
      display: 'none'
    });
  }).append(menu);

  $(window).on('blur', function(event) {
    menu.css({
      display: 'none'
    });
  });
}

var deselect = function(element) {
  var parent_ul = element.parent().parent();
  var grandparent_ul = parent_ul.parent().parent();

  var key = element.data('key');

  // Unhighlight all
  grandparent_ul.children('li.element')
                .children('ul')
                .children('li')
                .children('span.key').each(function() {
    if ($(this).data('key') == key) {
      $(this).removeClass("in-results");
      $(this).css("color", "");
    }
  });
  element.css("color", "");
};

var select = function(element) {
  var parent_ul = element.parent().parent();
  var grandparent_ul = parent_ul.parent().parent();

  var key = element.data('key');

  if (grandparent_ul.hasClass("array")) {
    // Highlight all
    grandparent_ul.children('li.element')
                  .children('ul')
                  .children('li')
                  .children('span.key').each(function() {
      if ($(this).data('key') == key) {
        $(this).addClass("in-results");
        $(this).css("color", "red");
      }
    });
    element.css("color", "red");
  }
  else {
    element.css("color", "blue");
  }
};

var getValues = function(element) {
  var parent_ul = element.parent().parent();
  var grandparent_ul = parent_ul.parent().parent();

  var key = element.data('key');

  var ret = [];

  if (grandparent_ul.hasClass("array")) {
    // Highlight all
    grandparent_ul.children('li.element')
                  .children('ul')
                  .children('li')
                  .children('span.key').each(function() {
      if ($(this).data('key') == key) {
        ret.push(parseFloat($(this).parent().children('span.value').text()));
      }
    });
  }

  return ret;
};

var produceGraph = function() {
  var new_graph = $("<div id='graph' class='graph'></div>");
  graph.replaceWith(new_graph);
  graph = new_graph;
  occam.graphs[type]($.extend(true, {}, options));
};

var toggleSelect = function(element) {
  // Toggle class
  element.toggleClass("in-results");

  if (!element.hasClass("in-results")) {
    deselect(element);

    // Remove from graph
    options.data.groups.forEach(function(group, i) {
      if (group.key == element.data('key')) {
        options.data.groups.splice(i,1);
      }
    });
  }
  else {
    select(element);

    // TODO: Keys as base64 values
    // TODO: Tag key here as its full path (foo.bar.baz instead of just baz)
    // TODO: Apply each of these two to the deselect as well
    options.data.groups.push({
      name: element.data('key'),
      key: element.data('key'),
      series: getValues(element)
    });

    options.labels = options.labels || {};
    options.labels.y = element.parent().children('span.value').data('units');
  }
};

/* Renders the given results data against the given schema within the given
 * element.
 */
function renderResultsData(element, data, schema) {
  // Render Warnings/Errors

  // Render data
  function renderImpl(container, hash, schema, id) {
    if (id === undefined) {
      id = "data";
    }

    if (hash instanceof Array) {
      if (schema instanceof Array) {
        schema = schema[0];
      }

      var ul = $('<ul class="array"></ul>');
      hash.map(function(element, index) {
        var li = $('<li class="element" data-id="' + id + '-' + index + '"></li>');
        li.append($('<span class="expand shown">&#9662;</span>'));
        renderImpl(li, element, schema, id + "-" + index);
        ul.append(li);
      });
      container.append(ul);
    }
    else if (typeof hash === 'string' || hash instanceof String ||
             typeof hash === 'boolean' ||
             typeof hash === 'number') {
      container.append(hash);
    }
    else if (typeof hash === 'object') {
      var keys = Object.keys(hash);
      keys_array = [];
      keys_else  = [];

      keys.forEach(function(key) {
        if (hash[key] instanceof Array) {
          keys_array.push(key);
        }
        else {
          keys_else.push(key);
        }
      });

      var ul = $('<ul class="hash"></ul>');
      keys_else.sort().forEach(function(key) {
        var base64_key = base64_urlencode(key);

        var v = hash[key];

        var inner_schema = schema;
        if (schema[key] !== undefined) {
          inner_schema = schema[key];
        }
        var units = "";
        if (inner_schema.units !== undefined) {
          units = inner_schema.units;
        }

        var li = $('<li data-id="' + id + "-" + base64_key + '"></li>');
        li.append($("<span class='key' data-key='" + key + "'>" + key + "</span>"));

        var value = $("<span class='value'></span>");
        renderImpl(value, v, inner_schema, id + "-" + base64_key);
        li.append(value);

        if (units !== "") {
          li.children('span.value').data('units', units);
          li.append(" <span class='units'>" + units + "</span>");
        }

        ul.append(li);
      });

      container.append(ul);

      keys_array.sort().forEach(function(key) {
        var base64_key = base64_urlencode(key);

        var v = hash[key];

        var inner_schema = schema;
        if (schema[key] !== undefined) {
          inner_schema = schema[key];
        }
        var units = "";
        if (inner_schema.units !== undefined) {
          units = inner_schema.units;
        }

        var li = $('<li data-id="' + id + "-" + base64_key + '"></li>');
        li.append($("<span class='expand shown'>&#9662;</span>"));
        li.append($("<span class='key' data-key='" + key + "'>" + key + "</span>"));

        var value = $("<span class='value'></span>");
        renderImpl(value, v, inner_schema, id + "-" + base64_key);
        li.append(value);

        if (units !== "") {
          li.children('span.value').data('units', units);
          li.append(" <span class='units'>" + units + "</span>");
        }
        ul.append(li);
      });

      container.append(ul);
    }
  }

  renderImpl(element, data["data"], schema);
}

/* Retrieves the results data for an experiment and renders it to the given
 * element.
 */

// TODO: get rid of this by properly encapsulating stuff
var schema;
function retrieveResultsData(element, object_id, experiment_id, func) {
  var retrieveExperiment = function() {
    $.getJSON('/experiments/' + experiment_id + '/results', function(data) {
      element.addClass('loaded');
      renderResultsData(element, data, schema);
      fitResultsData(element);
	  attachContextMenu(element);
      if (func !== undefined) {
        func();
      }
    });
  }

  var retrieveSchema = function(data) {
    schema = data.data;
    retrieveExperiment();
  }

  // Retrieve the schema if we don't already have it.
  if (schema === undefined) {
    // We have to wait for the schema...
    $.getJSON('/objects/' + object_id + '/output_schema.json', retrieveSchema);
  }
  else {
    retrieveExperiment();
  }
}

/* This will attach the context menu to each key in the given element.
 */
function attachContextMenu(element) {
  element.find('li span.key').each(function() {
    $(this).on('click', function(event) {
      // What key is this?
      // Do not open the context menu when someone is selecting text
      if (window.getSelection().rangeCount > 0 && !window.getSelection().getRangeAt(0).collapsed) {
        return;
      }

      // Open context menu
      menu.css({
        left: event.pageX,
        top:  event.pageY,
        display: 'block'
      });

      var element = $(this);

      menu.find('#graph-builder-menu-select')
          .unbind('click.toggleSelect')
          .on('click.toggleSelect', function(event) {
        toggleSelect(element);
		produceGraph();
        menu.css({
          display: 'none'
        });
      });

      var parent_array = $(this).parent().parent().parent().parent();
      var key = $(this).parent().data('id');

      menu.find('#graph-builder-menu-select-across')
          .unbind('click.toggleSelect')
          .on('click.toggleSelect', function(event) {
            var unloaded = $('#results-data .data:not(.loaded)');
            var number = unloaded.length;

            var select_across = function() {
              // Select on every tab
              $('#results-data .data')
                .find('li[data-id="' + key + '"]')
                .each(function() {
                  toggleSelect($(this).children('span.key'));
                });

			  options.labels.x = {}

              // Choose group labels (experiment dependent variable)
              // Choose x labels (look at 'id')
              id = parent_array.find('li > ul > li > span.key[data-key="id"] + span.value');
              if (id.length > 0) {
                ids = id.map(function() { return $(this).text()});
				options.labels.x.series = ids;
              }

			  // Choose global x label
			  options.labels.x.name = element.parent().parent().parent().parent().parent().parent().children('span.key').data('key');

			  // Relabel groups with experiment names
			  var experiments = $('#results-bar > ul > li');
              options.data.groups.forEach(function(group, i) {
                group.name = experiments[i].innerText;
              });

		      // At this point, we have graphed it with all of the groups
			  produceGraph();
            }

            if (number == 0) {
              select_across();
            }
            else {
              unloaded.each(function() {
                retrieveResultsData($(this), $(this).data('object-id'), $(this).data('experiment-id'), function() {
                  number -= 1;
                  if (number == 0) {
                    select_across();
                  }
                });
              });
            }

        menu.css({
          display: 'none'
        });
      });

      event.preventDefault();
      event.stopPropagation();
    });
  });
}

/* Given a results container, this will fit all of the data and style it as a
 * table. It will resize elements so they flow, but are still legible. That is,
 * aligning them together as a grid.
 */
function fitResultsData(element) {
  // Figure out the width spanning to organize the results as a table

  // This spans out the keys (left-hand side) so that the values (right-hand side) line up
  element.find('ul').each(function() {
    // Determine the widest key
    var maxKeyWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).children('span.key').width()+1;
    }).get());

    // Apply that width to all keys whose values are not themselves an array.
    if (maxKeyWidth > 0) {
      $(this).children('li').each(function() {
        $(this).children('span.key').each(function() {
          if ($(this).parent().children('span.value').children('ul.array').length == 0) {
            $(this).css({
              display: "inline-block",
              width: ""+maxKeyWidth+"px",
            });
          }
        });
      });
    }
  });

  // The above lined up keys and values
  // Since we can have an arbitrary grid of keys and values, we then make sure every
  // key/value pair is the same size. That way, these elements will line up within a
  // grid.
  element.find('ul').each(function() {
    var maxWidth = Math.max.apply( Math, $(this).children('li').map(function() {
      return $(this).width()+1;
    }).get());

    $(this).children('li').each(function() {
      $(this).css({
        width: ""+maxWidth+"px",
      });
    });
  });

  // Collapse Arrays
  element.find('ul.hash > li > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('span.value').children('ul').css({
        display: 'block'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('span.value').children('ul').css({
        display: 'none'
      });
      $(this).text("\u25b8");
    }
  });

  // Create a '...' div for collapsed data points which upon clicking
  // will expand the data once more.
  fake_li = $('<li class="fake"><span class="key">...</span></li>').css({
    display: 'none'
  }).on('click', function(e) {
    $(this).parent().parent().children('span.expand').trigger('click');
  });

  element.find('ul.array > li.element > ul').append(fake_li);

  // Collapse array element hashes
  element.find('ul.array > li.element > span.expand').on('click', function(e) {
    $(this).toggleClass('shown');
    // Get associated array element div
    if ($(this).hasClass('shown')) {
      $(this).parent().children('ul').children('li').css({
        display: 'inline-block'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'none'
      });
      $(this).text("\u25be");
    }
    else {
      $(this).parent().children('ul').children('li').css({
        display: 'none'
      });
      $(this).parent().children('ul').children('li.fake').css({
        display: 'inline-block'
      });
      $(this).text("\u25b8");
    }
  });

  // Collapse all
  element.find('ul.hash > li > span.expand').trigger('click');

  // Expand all first sections
  element.children('ul.hash').children('li').children('span.expand').trigger('click');
}

function loadGraphTypes() {
}

$(function() {
  var graphOptions = $('#graph-options');
  var graphOptionList = graphOptions.children('ul.options');
  var resultsData = $('#results-data');
  var dataSection = resultsData.children('.data.active');

  // What is the width of a scrollbar?
  var tmp_inner = $('<div style="width: 100%; height:200px;">test</div>'),
      tmp_outer = $('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append(tmp_inner),
      inner = tmp_inner[0],
      outer = tmp_outer[0];

  $('body').append(outer);
  var width1 = inner.offsetWidth;
  tmp_outer.css('overflow', 'scroll');
  var width2 = outer.clientWidth;
  tmp_outer.remove();
  var scrollWidth = width1 - width2;

  // Create a base graph

  // Attach all global graph options

  $("input#title-input").on('change', function(event) {
    options.title = $(this).val();
  });

  // Load all graph options
  var graphSelect = $("ul#graph-type-options");

  OccamViz.Graphs.getGraphs().forEach(function(graph_type) {
    var item = $("<li data-func='" + graph_type.call + "'></li>")
      .on('click', function(event) {
        graphSelect.children('li.active').removeClass('active');
        $(this).addClass('active');
        type = $(this).data('func');
        graph.html("");
        options.width = graph.width();
        occam.graphs[type]($.extend(true, {}, options));
        graphOptions.height($(window).height() - graphOptions.offset().top - (graphOptions.outerHeight() - graphOptions.height()))
          .width(graph.outerWidth() - (graphOptions.outerWidth() - graphOptions.width()) + scrollWidth);
        graphOptionList.width(graphOptions.width() - (graphOptionList.outerWidth() - graphOptionList.width()) - scrollWidth);

        // Remove existing per-graph option list
        $('#graph-options ul.options li.option:not(.global)').remove();

        // Add graph-specific options to end of the list
        if (graph_type.fields !== undefined) {
          graph_type.fields.forEach(function(field) {
            var li = $('<li></li>').addClass('option');
            var label = $('<label></label>').text(field.label);

            li.append(label);

            var div = $('<div></div>').addClass('one');

            var input_type = "text";
            if (field.type == "int") {
              input_type = "text";
            }
            else if (field.type == "string") {
              input_type = "text";
            }
            else if (field.type == "range") {
              input_type = "range";
            }
            else if (field.type == "boolean") {
              input_type = "checkbox";
            }

            var input = $('<input>')
              .attr('type', input_type)
              .on('change', function(event) {
                var element = $(this);

                var value = "";
                if (input_type == "checkbox") {
                  value = element.is(':checked');
                }
                else {
                  value = element.val();
                }

                options.fields = options.fields || {};
                options.fields[field.id] = value;
              });

            div.append(input);

            li.append(div);
            graphOptionList.append(li);
          });
        }
      });

    var item_img = $("<img/>")
      .attr('src', "/images/graph_builder/" + graph_type.call + ".png")
      .attr('alt', graph_type.name)
      .attr('title', graph_type.name)
      .css({
        'width':  '44px',
        'height': '44px',
        'margin': '10px'
      });

    item.append(item_img);

    graphSelect.append(item);
  });

  // Pick the first graph to start with
  graphSelect.children('li:first-child').addClass('active');

  fitResultsData($('#results-data .data.active'));

  // On resize, remake the graph
  $(window).on('resize', function() {
    graphSelect.children('li.active').trigger('click');
    resultsData.height($(window).height() - resultsData.offset().top - (resultsData.outerHeight() - resultsData.height()))
  });

  $(window).trigger('resize');

  // Data tabs
  $('#results-bar ul li').on('click', function(event) {
    $('#results-bar ul li.active').removeClass('active');
    $(this).addClass('active');

    // Load results (if we don't have them)
    resultsData.children('.data.active').removeClass('active');
    dataSection = resultsData.children('.data[data-experiment-id=' + $(this).data('experiment-id') + ']');
    dataSection.addClass('active');

    // Retrieve results data only if not already downloaded
    if (!dataSection.hasClass('loaded')) {
      retrieveResultsData(dataSection, $(this).data('object-id'), $(this).data('experiment-id'));
    }
  });

  $('#results-bar ul li:first-child').trigger('click');

  // Create context menu
  initializeContextMenu();

  attachContextMenu(dataSection);

  // Init command bar
  $('#command-bar ul li#save-button a').on('click', function(event) {
    // Denote something in the UI saying 'saving...'
    $.post($(this).parent().data('target'), {
      "data": JSON.stringify(options),
      "type": type
    }, function() {
      // Upon successful save (denote something in the UI saying 'saved.')
    });

    event.stopPropagation();
  });
});
