$(document).ready(function() {
  var query = 0;

  if ($('.information .installation span.status').text().trim() !== "finished") {
    query++;
  }

  if ($('.information .build-progress span.status').text().trim() !== "finished") {
    query++;
  }

  if ($('.information .run-progress span.status').text().trim() !== "finished") {
    query++;
  }

  if (query > 0) {
    var id = setInterval(function() {
      $.getJSON(document.URL + "/output", function(data) {
        $('pre#install-output').text(data["install_log"]);
        $('pre#build-output').text(data["build_log"]);
        $('pre#worker-output').text(data["run_log"]);
        $('pre#run-output').text(data["output_log"]);
      });
    }, 500);
  }
});
